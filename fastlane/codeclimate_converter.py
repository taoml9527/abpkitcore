# This file is part of Adblock Plus <https://adblockplus.org/>,
# Copyright (C) 2006-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import json
import sys
import uuid

# A script to convert SwiftLint report to CodeClimate report.
# GitLab support only CodeClimate report.

# Creates a new string with the prefix up until given substring removed.
def slicer(my_str, sub):
   index=my_str.find(sub)
   if index !=-1 :
        return my_str[index:] 
   else :
        raise Exception('Sub string not found!')

# Converts SwiftLint format issue to CodeClimate format issue
def convert(issue):
    ABPKitCorePath = slicer(issue["file"], "abpkitcore/")
    # Add just filename as location. SwiftLint will generate local file path.
    # GitLab will correctly redirect to file in blob based on the filename
    file = slicer(ABPKitCorePath, "/")
    codeClimateItem = {
        'description': issue["reason"],
        'fingerprint': str(uuid.uuid4()),
        'location': {
        'path': file,
        'lines': {
            'begin': issue["line"]
            }
        }
    }

    return codeClimateItem

def main(argv):
    print("[CodeClimate Converter] Running the script")
    with open("fastlane/swiftlint.result.json", "r") as swiftLintData:
        issues = json.load(swiftLintData)

        print("[CodeClimate Converter] Loaded fastlane/swiftlint.json file, converting....")
        convertedIssues = list(map(convert, issues))

        print("[CodeClimate Converter] Converted, writing to file fastlane/swiftlint.json")
        with open("fastlane/swiftlint.result.json", "w") as codeClimateData:
            json.dump(convertedIssues, codeClimateData, indent=4, sort_keys=True)

if __name__ == "__main__":
   main(sys.argv[1:])
