# ABPKitCore

Provides the core ABPKit functionality to be used for any content blocking technology - be it WKWebView or Safari.

## Features

For now ABPKitCore manages everything related to retrievieng a filter list for a particular filter list source:

- Load a filter list from client app bundle. 
- Construct a proper download url and load the filter list from remote host.
- Cache locally the downloaded filter list so it can be retrieved fast.
- Refresh the local cache if the filter list is older than one day.
- Store allowed domains and apply those to the loaded filter list.

Check full filter list loading flow - [Filter List Loading Flow](https://gitlab.com/eyeo/adblockplus/abpkitcore/-/wikis/Filter-list-load-flow)

## Usage

To make use of filter list loadin you need to:

1. Create a instance of FilterListLoader:

```swift
FilterListManagementComposer.makeFilterListLoader(clientMetaData: ClientMetaData, clientAppBundle: Bundle, storageContainerURL: URL)
```
You should provide the meta data about the client who uses the loader, the client app bundle to be used to load the filter lists from bundle, and a storage container url where all of the downloaded filter lists and any additional info will be stored.

2. Call the loadFilterList method on the created instance:

```swift
filterListLoader.loadFilterList(forSource: FilterListSource) { result in }
```
3. Choose a filter list source to load for based on your scenario

```swift
filterListLoader.loadFilterList(forSource: .remote(.abp(.withAA(.easyList)))) { filterList in
    ///
}
```
Here we tell the loader to load an abp predefined source with acceptable ads added from remote host. For more info about source see `FilterListSource.swift`.

## Some additional setup that might be needed

- To be able to load ABP predefined filter lists from bundle, it is needed first to bundle ABP filter lists. To do so, you have to add `bundleABPFilterList.sh` to your client app Build Phases. The script will download and bundle the choosed filter lists. This will also allow to fallback to bundled filter lists if loading from remote failed.

```bash
bash bundleABPFilterLists.sh en
```

- For your custom remote source, if it is wanted to fallback to bundled version when remote load failed, be sure that you set the name of the custom remote source to be the same as the name of the bundled filter list so the framework is able to find it.

## Documentation

Docs for public interfaces can be found at [ABPKitCore docs](https://eyeo.gitlab.io/adblockplus/abpkitcore/).

## Integration

- Swift Package Manager

ABPKitCore is available to be used through SPM, you can add it as a dependency to your own package or application.

- Manually:

You can directly download and integrate ABPKitCore as a package or as a framework.
