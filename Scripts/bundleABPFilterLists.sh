#!/bin/sh

# This script should be added as a step in your project build phases
# So the filter lists are downloaded and bundled at build time.

# Identifier, nonAARemotePAth, nonAABundledName, aaRemotePath, aaBundledName
abpFilterlists=("en
                 https://easylist-downloads.adblockplus.org/easylist_min_content_blocker.json
                 easylist_content_blocker.json
                 https://easylist-downloads.adblockplus.org/easylist_min+exceptionrules_content_blocker.json
                 easylist+exceptionrules_content_blocker.json
                 ",
                 "fr 
                 https://easylist-downloads.adblockplus.org/easylist_france_min_content_blocker.json
                 easylistfrance_content_blocker.json
                 https://easylist-downloads.adblockplus.org/easylist_france+exceptionrules_content_blocker.json
                 easylistfrance+exceptionrules_content_blocker.json
                 ",
                 "cn 
                 https://easylist-downloads.adblockplus.org/easylist_china_min_content_blocker.json
                 easylistchina_content_blocker.json
                 https://easylist-downloads.adblockplus.org/easylist_china+exceptionrules_content_blocker.json
                 easylistchina+exceptionrules_content_blocker.json
                 ",
                 "nl 
                 https://easylist-downloads.adblockplus.org/easylist_dutch_min_content_blocker.json
                 easylistdutch_content_blocker.json
                 https://easylist-downloads.adblockplus.org/easylist_dutch+exceptionrules_content_blocker.json
                 easylistdutch+exceptionrules_content_blocker.json
                 ",
                 "de 
                 https://easylist-downloads.adblockplus.org/easylist_germany_min_content_blocker.json
                 easylistgermany_content_blocker.json
                 https://easylist-downloads.adblockplus.org/easylist_germany+exceptionrules_content_blocker.json
                 easylistgermany+exceptionrules_content_blocker.json
                 ",
                 "it 
                 https://easylist-downloads.adblockplus.org/easylist_italy_min_content_blocker.json
                 easylistitaly_content_blocker.json
                 https://easylist-downloads.adblockplus.org/easylist_italy+exceptionrules_content_blocker.json
                 easylistitaly+exceptionrules_content_blocker.json
                 ",
                 "es 
                 https://easylist-downloads.adblockplus.org/easylist_spain_min_content_blocker.json
                 easylistspain_content_blocker.json
                 https://easylist-downloads.adblockplus.org/easylist_spain+exceptionrules_content_blocker.json
                 easylistspain+exceptionrules_content_blocker.json
                 ",
                 "ru-ua 
                 https://easylist-downloads.adblockplus.org/easylist_russiaukraine_min_content_blocker.json
                 easylistrussiaukraine_content_blocker.json
                 https://easylist-downloads.adblockplus.org/easylist_russiaukraine+exceptionrules_content_blocker.json
                 easylistrussiaukraine+exceptionrules_content_blocker.json
                 ")

RESOURCES="Resources"
OUTPUT_PATH=$CONFIGURATION_BUILD_DIR/$CONTENTS_FOLDER_PATH

# MAIN

# Get input arguments
[ "$#" -lt 1 ] && printf "Please give at least one argument\n" && exit 

# Update output path for macOS target
if [ $EFFECTIVE_PLATFORM_NAME == "-macosx" ]
then
    $OUTPUT_PATH=$OUTPUT_PATH/$RESOURCES
fi

# Bundle filter list for each provided code
for matchIdentifier 
do
    for filterList in "${abpFilterlists[@]}"
    do
        set -- $filterList
        identifier=$1
        nonAARemotePath=$2
        nonAABundleName=$3
        aaRemotePath=$4
        aaBundleName=$5
        if [[ "$matchIdentifier" == "$identifier" ]]
        then
          nonAABundledPath="$OUTPUT_PATH/$nonAABundleName"
          # If filter list is missing, download from remote path
          if [ ! -f $nonAABundledPath ]
          then
            echo "Downloading rules from $nonAARemotePath"
            curl -o "$nonAABundledPath" "$nonAARemotePath"
          fi

          aaBundledPath="$OUTPUT_PATH/$aaBundleName"
          # If filter list is missing, download from remote path
          if [ ! -f $aaBundledPath ]
          then
            echo "Downloading rules from $aaRemotePath"
            curl -o "$aaBundledPath" "$aaRemotePath"
          fi
        fi
    done
done
