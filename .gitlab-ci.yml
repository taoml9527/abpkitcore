stages:
  - quality_report # Gather code quality report
  - build_and_test # Run tests and export test report
  - reports # Host apps build and run

#Cache dependencies to decrease pipeline execution time
cache:
  paths:
    - Carthage # Cache carthage config to be reused between pipelines
    - vendor # Cache gems to be reused between pipelines

variables:
  LC_ALL: "en_US.UTF-8"
  LANG: "en_US.UTF-8"

include:
  - template: Code-Quality.gitlab-ci.yml # Necessary for code quality report

before_script:
  - bundle install --path vendor/bundle # Make sure correct gems are installed

#---------- Quality check ----------------#

# Code quality job
# Run swift linter to generate initial report
# Runs codeclimate converter to create GitLab compatible report
code_quality:
  stage: quality_report
  artifacts:
    paths:
      - fastlane/swiftlint.result.json
    reports:
      codequality: fastlane/swiftlint.result.json
  script:
    - bundle exec fastlane code_quality
    - python3 fastlane/codeclimate_converter.py
  tags:
    - ios, macos, abpkit
  rules:
    - if: '$CODE_QUALITY_DISABLED' # Code quality report can be disable if needed
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"' # Run code quality job in merge request pipelines
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never # If there is a MR opened for the given branch, run the pipeline only for MR
    - if: '$CI_COMMIT_BRANCH' # If no associated MR, run the pipeline on branch

#---------- Build ----------------#

# Build and test job
build_and_test:
  stage: build_and_test
  script:
    - bundle exec fastlane build_and_test
  tags:
    - ios, macos, abpkit
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

#---------- Public reports ----------------#

# Generate necessary public reports
pages:
  tags:
    - ios, macos, abpkit
  stage: reports
  script:
    - bundle exec fastlane public_docs
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
