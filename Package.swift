// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "ABPKitCore",
    platforms: [.iOS(.v11), .macOS(.v10_12)],
    products: [
        .library(name: "ABPKitCore",
                 targets: ["ABPKitCore"])
    ],
    dependencies: [],
    targets: [
        .target(name: "ABPKitCore",
                dependencies: [],
                path: "Sources"),
        .testTarget(name: "ABPKitCoreTests",
                    dependencies: ["ABPKitCore"],
                    path: "Tests")
    ]
)
