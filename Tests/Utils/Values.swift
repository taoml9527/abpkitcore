// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

// swiftlint:disable force_unwrapping

import Foundation
@testable import ABPKitCore

func anyNSError(_ code: Int = 1) -> Error {
    NSError(domain: "test", code: code)
}

extension Result where Failure == Error {
    static var anyFailure: Result {
        .failure(anyNSError())
    }

    static func anyFailureWithCode(_ code: ABPKitCoreError.ABPKitCoreErrorCode) -> Result {
        .failure(anyNSError(code.rawValue))
    }

    static func anyFailureWithCode(_ code: Int) -> Result {
        .failure(anyNSError(code))
    }
}

func anyHTTPURL() -> URL {
    URL(string: "http://anySimpleURL.com")!
}

func anyContainerURL() -> URL {
    URL(string: "storage")!
}

func anyData() -> Data {
    return Data("any data".utf8)
}

func anySuccessHTTPURLResponse() -> HTTPURLResponse {
    return HTTPURLResponse(url: anyHTTPURL(), statusCode: 200, httpVersion: nil, headerFields: nil)!
}

func anyFilterList() -> FilterList {
    .init(content: "[{trigger: {url-filter: ^https?://([^/:]*\\.)?community\\.adlandpro\\.com[/:], url-filter-is-case-sensitive: true}, action: {type: css-display-none, selector: [id=AdContent], [id=ctl00_slider]}}]")
}

func anyFilterListSource() -> FilterListSource {
    .remote(anyFilterListRemoteSource())
}

func anyFilterListRemoteSource() -> FilterListType {
    .abp(.withAA(.easyList))
}

func anyFilterListBundledSource() -> FilterListType {
    .abp(.withAA(.easyList))
}

func anyDate() -> Date {
    Date()
}

func anyDownloadedFilterListMetaInfo() -> DownloadedFilterListMetaInfo {
    DownloadedFilterListMetaInfo(source: anyFilterListRemoteSource(),
                                 lastDownloadDate: anyDate(),
                                 downloadCount: 1)
}

func anyClientMetaData(appName: String = "partnerApp") -> ClientMetaData {
    ClientMetaData(addon: .init(name: "safari", version: "1.0.0"),
                   app: .init(name: appName, version: "2.1.2"),
                   platform: .init(name: "webkit", version: "10.0.2"))
}

func anyAllowedDomain() -> AllowedDomain {
    "any.com"
}

func filterListWithAllowedDomainAdded(_ filterList: FilterList, allowedDomain: AllowedDomain) -> FilterList {
    var appendedList = filterList.content
    let formattedDomain = "\"^[^:]+:(\\/\\/)?([^\\/]+\\\\.)?\(allowedDomain)[^\\\\.][\\/:]?\""
    let domainsRule = "{\"trigger\":{\"if-top-url\":[\(formattedDomain)],\"load-type\":[\"first-party\",\"third-party\"],\"url-filter\":\".*\",\"url-filter-is-case-sensitive\":false},\"action\":{\"type\":\"ignore-previous-rules\"}}"
    _ = appendedList.popLast()
    return FilterList(content: appendedList.appending(",").appending(domainsRule).appending("]"))

}

extension Date {
    func minusFilterListExpirationTimetamp() -> Date {
        return adding(days: -filterListExpirationInDays)
    }

    private var filterListExpirationInDays: Int {
        return 1
    }

    private func adding(days: Int) -> Date {
        return Calendar(identifier: .gregorian).date(byAdding: .day, value: days, to: self)!
    }
}

extension Date {
    func adding(seconds: TimeInterval) -> Date {
        return self + seconds
    }
}
