// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation
import XCTest

func errorAlwaysEqual(_ lhs: Error, _ rhs: Error) -> Bool {
    true
}

func castedErrorIsEqual<T: Equatable>(cast: T.Type) -> (Error, Error) -> Bool {
    return { lhs, rhs in
        guard let lhs = lhs as? T, let rhs = rhs as? T else {
            return false
        }
        return lhs == rhs
    }
}

func nsErrorIsEqual(_ lhs: Error, _ rhs: Error) -> Bool {
    return castedErrorIsEqual(cast: NSError.self)(lhs, rhs)
}

func errorCodeIsEqual(_ lhs: Error, _ rhs: Error) -> Bool {
    (lhs as NSError).code == (rhs as NSError).code
}

func resultIsEqual<T>(_ successIsEqual: @escaping (T, T) -> Bool,
                      _ errorIsEqual: @escaping (Error, Error) -> Bool = errorAlwaysEqual) -> (Result<T, Error>, Result<T, Error>) -> Bool {
    return { lhs, rhs in
        switch (lhs, rhs) {
        case let (.success(lhsValue), .success(rhsValue)):
            return successIsEqual(lhsValue, rhsValue)
        case let (.failure(lhsError), .failure(rhsError)):
            return errorIsEqual(lhsError, rhsError)
        default:
            return false
        }
    }
}

extension XCTestCase {
    func expectOperation<T>(_ operation: (@escaping (T) -> Void) -> Void,
                            toCompleteWith expectedResult: T,
                            isEqual: @escaping (T, T) -> Bool,
                            when action: () -> Void = {},
                            file: StaticString = #file,
                            line: UInt = #line) {
        let loadExpectation = expectation(description: "wait for load")
        operation { result in
            XCTAssertTrue(isEqual(result, expectedResult),
                          "Expected to complete with result \(expectedResult), completed with \(result) instead",
                file: file,
                line: line)
            loadExpectation.fulfill()
        }
        action()
        wait(for: [loadExpectation], timeout: 1.0)
    }
}
