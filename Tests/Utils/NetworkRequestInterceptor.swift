// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Interceptor to be used to intercept requests for testing purpose
class NetworkRequestInterceptor: URLProtocol {
    static var performedRequests = [URLRequest]()

    // MARK: - Stubbing

    private static var _stub: Stub?
    private static var stub: Stub? {
        get { return queue.sync { _stub } }
        set { queue.sync { _stub = newValue } }
    }

    private static let queue = DispatchQueue(label: "URLProtocolStub.queue")

    struct Stub {
        let data: Data?
        let response: URLResponse?
        let error: Error?
        let requestObserver: ((URLRequest) -> Void)?
    }

    static func removeStub() {
        stub = nil
    }

    static func observeRequests(observer: @escaping (URLRequest) -> Void) {
        stub = Stub(data: nil, response: nil, error: nil, requestObserver: observer)
    }

    static func stub(stub: Stub) {
        self.stub = stub
    }

    static func stubResponse(data: Data? = nil,
                             response: URLResponse? = nil,
                             error: Error? = nil,
                             observer: @escaping (URLRequest) -> Void = { _ in }) {
        stub = Stub(data: data, response: response, error: error, requestObserver: observer)
    }

    static func stubSuccess(withData data: Data, observer: @escaping (URLRequest) -> Void = { _ in }) {
        let response = HTTPURLResponse(url: anyHTTPURL(),
                                       statusCode: 200,
                                       httpVersion: nil,
                                       headerFields: nil)
        stubResponse(data: data,
                     response: response,
                     error: nil,
                     observer: observer)
    }

    // MARK: - URLProtocol

    override class func canInit(with request: URLRequest) -> Bool {
        performedRequests.append(request)
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        request
    }

    override func startLoading() {
        guard let stub = NetworkRequestInterceptor.stub else {
            client?.urlProtocolDidFinishLoading(self)
            return
        }

        if let data = stub.data {
            self.client?.urlProtocol(self, didLoad: data)
        }

        if let response = stub.response {
            self.client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        }

        if let error = stub.error {
            self.client?.urlProtocol(self, didFailWithError: error)
        } else {
            self.client?.urlProtocolDidFinishLoading(self)
        }

        stub.requestObserver?(self.request)
    }

    override func stopLoading() {}

    // MARK: - Intercept configuration

    static func sessionConfigForIterception() -> URLSessionConfiguration {
        let session = URLSessionConfiguration.ephemeral
        session.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        session.urlCache = nil
        session.protocolClasses?.insert(NetworkRequestInterceptor.self, at: 0)
        startInterceptingRequests()
        return session
    }

    static func startInterceptingRequests() {
        URLProtocol.registerClass(NetworkRequestInterceptor.self)
    }
}
