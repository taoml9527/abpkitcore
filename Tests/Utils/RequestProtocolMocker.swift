// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation
@testable import ABPKitCore

final class RequestProtocolMocker<Param: Equatable, LoadedValue> {
    struct Requests {
        let param: Param
        let completion: (LoadedValue) -> Void
    }

    var requestedParams: [Param] {
        return requests.map { $0.param }
    }

    var requests = [Requests]()

    func completeOperation(with result: LoadedValue, at idx: Int = 0) {
        requests[idx].completion(result)
    }
}

// MARK: - FilterListLoader Mock
typealias FilterListLoaderMock = RequestProtocolMocker<FilterListSource, FilterListLoader.LoadResult>

extension RequestProtocolMocker: FilterListLoader where
          Param == FilterListSource, LoadedValue == FilterListLoader.LoadResult {
    func loadFilterList(forSource source: FilterListSource,
                        completion: @escaping FilterListTypeLoader.LoadCompletion) {
        requests.append(Requests(param: source, completion: completion))
    }
}

// MARK: - RemoteSourceFilterListLoader Mock
typealias FilterListTypeLoaderMock = RequestProtocolMocker<FilterListType, FilterListTypeLoader.LoadResult>

extension RequestProtocolMocker: FilterListTypeLoader where
          Param == FilterListType, LoadedValue == FilterListTypeLoader.LoadResult {
    func loadFilterList(forSource source: FilterListType,
                        completion: @escaping FilterListTypeLoader.LoadCompletion) {
        requests.append(Requests(param: source, completion: completion))
    }
}

// MARK: - Filter list caching
struct FilterListCacheParams: Equatable{
    let list: FilterList
    let source: FilterListType
}
typealias FilterListCacheMock = RequestProtocolMocker<FilterListCacheParams, RemoteSourceFilterListCache.CachingResult>
extension RequestProtocolMocker: RemoteSourceFilterListCache where
          Param == FilterListCacheParams, LoadedValue == RemoteSourceFilterListCache.CachingResult {
    func cacheFilterList(_ filterList: FilterList, forSource source: FilterListType, completion: @escaping CachingCompletion) {
        requests.append(Requests(param: .init(list: filterList, source: source), completion: completion))
    }
}

// MARK: - Meta Info Update

typealias FilterListMetaInfoUpdateMock = RequestProtocolMocker<FilterListType, DownloadedFilterListMetaInfoUpdater.UpdateResult>
extension RequestProtocolMocker: DownloadedFilterListMetaInfoUpdater where
          Param == FilterListType, LoadedValue == DownloadedFilterListMetaInfoUpdater.UpdateResult {
    func updateMetaInfo(forSource source: FilterListType, completion: @escaping UpdateCompletion) {
        requests.append(Requests(param: source, completion: completion))
    }
}

// MARK: - Filter list refresh status

typealias FilterListRefreshStatusLoaderMock = RequestProtocolMocker<FilterListType, FilterListRefreshStatusLoader.LoadResult>
extension RequestProtocolMocker: FilterListRefreshStatusLoader where
          Param == FilterListType, LoadedValue == FilterListRefreshStatusLoader.LoadResult {
    func loadRefreshStatus(forSource source: FilterListType,
                           completion: @escaping FilterListRefreshStatusLoader.LoadCompletion) {
        requests.append(Requests(param: source, completion: completion))
    }
}

// MARK: - HTTPClient

typealias HTTPClientMock = RequestProtocolMocker<URL, HTTPClient.GetResult>
extension RequestProtocolMocker: HTTPClient where
          Param == URL, LoadedValue == HTTPClient.GetResult {
    func get(from url: URL, completion: @escaping GetCompletion) {
        requests.append(Requests(param: url, completion: completion))
    }
}

typealias RemoteFilterListStoreMock = RequestProtocolMocker<URL, RemoteFilterListStore.DownloadResult>
extension RequestProtocolMocker: RemoteFilterListStore where
          Param == URL, LoadedValue == RemoteFilterListStore.DownloadResult {
    func download(from url: URL, completion: @escaping DownloadCompletion) {
        requests.append(Requests(param: url, completion: completion))
    }
}

// MARK: - DownloadedFilterListMetaInfoLoader

class DownloadedFilterListMetaInfoLoaderMock: DownloadedFilterListMetaInfoLoader {
    private var singleSourceLoadRequestAndCompletions = [(source: FilterListType, completion: LoadCompletion)]()

    private var allSourcesLoadCompletions = [LoadAllCompletion]()

    var singleSourceRequests: [FilterListType] {
        singleSourceLoadRequestAndCompletions.map { $0.source }
    }

    var allSourcesLoadCount: Int {
        allSourcesLoadCompletions.count
    }

    func loadMetaInfo(forSource source: FilterListType,
                      completion: @escaping LoadCompletion) {
        singleSourceLoadRequestAndCompletions.append((source, completion))
    }

    func loadAllMetaInfo(completion: @escaping LoadAllCompletion) {
        allSourcesLoadCompletions.append(completion)
    }

    func completeSingleSourceLoading(with result: LoadResult) {
        singleSourceLoadRequestAndCompletions.first?.completion(result)
    }

    func completeLoadAllMetaLoading(with result: LoadAllResult) {
        allSourcesLoadCompletions.first?(result)
    }
}

// MARK: - Filter List Meta info

class FilterListMetaInfoStoreMock: DonwloadedFilterListMetaInfoStore {
    private(set) var retrieveCommands = [RetrieveCompletion]()
    var insertCommands = [(metaInfo: [DownloadedFilterListMetaInfo], completion: InsertCompletion)]()

    var insertRequest: [[DownloadedFilterListMetaInfo]] {
        insertCommands.map { $0.metaInfo }
    }

    func retrieveMetaInfoList(completion: @escaping RetrieveCompletion) {
        retrieveCommands.append(completion)
    }

    func insertMetaInfoList(_ metaInfo: [DownloadedFilterListMetaInfo], completion: @escaping InsertCompletion) {
        insertCommands.append((metaInfo, completion))
    }

    func completeRetrieve(withResult result: RetrieveResult, at idx: Int = 0) {
        retrieveCommands[idx](result)
    }

    func completeInsert(withResult result: InsertResult, at idx: Int = 0) {
        insertCommands[idx].completion(result)
    }
}

// MARK: - Mocks
class AllowedDomainsLoaderMock: AllowedDomainsLoader {

    var commands = [LoadCompletion]()

    func loadDomains(completion: @escaping LoadCompletion) {
        commands.append(completion)
    }

    func completeLoading(forCommandAt idx: Int = 0, with result: LoadResult) {
        commands[idx](result)
    }
}

class FilterListCacheStoreMock: FilterListCacheStore {
    struct InsertRequest: Equatable {
        let filterList: RawFilterList
        let filterListName: FilterListName
    }

    private var retrieveRequests = [(name: String, completion: RetrieveCompletion)]()
    private var insertRequests = [(request: InsertRequest, completion: InsertCompletion)]()

    var requestedRetrieves: [String] {
        retrieveRequests.map { $0.name }
    }

    var requestedInserts: [InsertRequest] {
        insertRequests.map { $0.request }
    }

    func retrieveFilterList(withName name: String, completion: @escaping RetrieveCompletion) {
        retrieveRequests.append((name, completion))
    }

    func insertFilterList(_ filterList: RawFilterList, withName name: String, completion: @escaping InsertCompletion) {
        insertRequests.append((InsertRequest(filterList: filterList, filterListName: name), completion))
    }

    func completeRetrieval(with result: RetrieveResult, at idx: Int = 0) {
        retrieveRequests[idx].completion(result)
    }

    func completeInsert(with result: InsertResult, at idx: Int = 0) {
        insertRequests[idx].completion(result)
    }
}
