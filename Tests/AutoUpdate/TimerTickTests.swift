// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class TimerTickTests: XCTestCase {

    func test_timer_ticking() {
        let sut = makeSUT()
        var tickCount = 0
        let exp = expectation(description: #function)
        sut.start(tickInterval: 0.01) { [weak sut] in
            tickCount += 1

            if tickCount > 5 {
                sut?.stop()
                exp.fulfill()
            }
        }

        wait(for: [exp], timeout: 1.0)
    }

    func test_deinit_doesNotNotifyObserverAnymore() {
        var sut: BackgroundTimeTicker? = BackgroundTimeTicker()

        let exp = expectation(description: "Expect to not notify on tick")
        exp.isInverted = true
        sut?.start(tickInterval: 0.0001, tickObserver: {
            exp.fulfill()
        })
        sut = nil

        XCTAssertNil(sut)
        wait(for: [exp], timeout: 0.0002)
    }

    // MARK: - Helpers

    private func makeSUT() -> BackgroundTimeTicker {
        let sut = BackgroundTimeTicker()
        trackForMemoryLeaks(sut)
        return sut
    }
}
