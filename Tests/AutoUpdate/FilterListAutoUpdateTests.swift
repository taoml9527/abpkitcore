// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class SFERulesAutoUpdaterTests: XCTestCase {
    func test_timeTickerBehaviour() {
        let (sut, timeTicker, _) = makeSUT()
        XCTAssertTrue(timeTicker.startRequestsCount == 0,
                      "Expected time ticker to not be started at initialisation")

        sut.startAutoUpdate()
        XCTAssertTrue(timeTicker.startRequestsCount == 1,
                      "Expected timer to be started on start updating")

        sut.startAutoUpdate()
        XCTAssertTrue(timeTicker.startRequestsCount == 1,
                      "Expected timer to not be started again, once started")
    }

    func test_init_noUpdateCommands() {
        let (_, _, rulesUpdaterSpy) = makeSUT()

        XCTAssertTrue(rulesUpdaterSpy.getRequestNumber() == 0,
                      "Expected no requests on rules updater when initializing")
    }

    func test_onTimerTick_requestToPerformUpdate() {
        let (sut, timeTicker, rulesUpdaterSpy) = makeSUT()

        sut.startAutoUpdate()
        XCTAssertTrue(rulesUpdaterSpy.getRequestNumber() == 0,
                      "Expected no rules update requests before timer tick")

        timeTicker.simulateTick()
        XCTAssertTrue(rulesUpdaterSpy.getRequestNumber() == 1,
                      "Expected to make rules update request on timer tick")

        timeTicker.simulateTick()
        timeTicker.simulateTick()

        XCTAssertTrue(rulesUpdaterSpy.getRequestNumber() == 3,
                      "Expected to make rules update request on next timer ticks")
    }

    func test_updateEvent_emitsFurtherEvent() {
        let (sut, timeTicker, rulesUpdaterSpy) = makeSUT()

        var collectedEvents = [MockUpdateEvent]()
        sut.observeUpdates { event in
            collectedEvents.append(event)
        }

        sut.startAutoUpdate()
        timeTicker.simulateTick()

        rulesUpdaterSpy.emitEvent(.started)
        rulesUpdaterSpy.emitEvent(.completed)

        let expectedEvents: [MockUpdateEvent] = [.started, .completed]

        XCTAssertEqual(collectedEvents,
                       [.started, .completed],
                       "Expected to forward events \(expectedEvents), instead forwarded \(collectedEvents)")
    }

    func test_multipleObservers_notifiesAll() {
        let (sut, timeTicker, rulesUpdaterSpy) = makeSUT()

        var obs1CollectedEvents = [MockUpdateEvent]()
        sut.observeUpdates { event in
            obs1CollectedEvents.append(event)
        }

        var obs2CollectedEvents = [MockUpdateEvent]()
        sut.observeUpdates { event in
            obs2CollectedEvents.append(event)
        }

        sut.startAutoUpdate()
        timeTicker.simulateTick()

        rulesUpdaterSpy.emitEvent(.started)
        rulesUpdaterSpy.emitEvent(.completed)

        let expectedEvents: [MockUpdateEvent] = [.started, .completed]
        XCTAssertEqual(obs1CollectedEvents,
                       expectedEvents,
                       "Expected to forward events \(expectedEvents) on first observer, instead forwarded \(obs1CollectedEvents)")
        XCTAssertEqual(obs2CollectedEvents,
                       expectedEvents,
                       "Expected to forward events \(expectedEvents) on second observer, instead forwarded \(obs2CollectedEvents)")
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file, line: UInt = #line) -> (FilterListAutoUpdater<MockUpdateEvent>, TimeTickerMock, FilterListUpdaterSpy) {
        let timeTick = TimeTickerMock()
        let (mockUpdater, testSpy) = mockRulesUpdater()

        let sut = FilterListAutoUpdater(timeTicker: timeTick,
                                        filterListUpdater: mockUpdater)

        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, timeTick, testSpy)
    }

    private enum MockUpdateEvent {
        case started, completed
    }

    private struct FilterListUpdaterSpy {
        let getRequestNumber: () -> Int
        let emitEvent: (MockUpdateEvent) -> Void
    }

    private func mockRulesUpdater() -> (updater: FilterListUpdater<MockUpdateEvent>, spy: FilterListUpdaterSpy) {
        var observers = [UpdateObserver<MockUpdateEvent>]()

        func eventEmitter(event: MockUpdateEvent) {
            observers.forEach { $0(event) }
        }

        func rulesUpdater(observer: @escaping UpdateObserver<MockUpdateEvent>) {
            observers.append(observer)
        }

        return (rulesUpdater,
                .init(getRequestNumber: { observers.count },
                      emitEvent: eventEmitter))
    }

    private class TimeTickerMock: TimeTicker {
        private var observers = [TickObserver]()
        private(set) var startRequestsCount = 0

        func start(tickInterval: TimeInterval, tickObserver: @escaping TickObserver) {
            observers.append(tickObserver)
            startRequestsCount += 1
        }

        func stop() {
            ///
        }

        func simulateTick() {
            observers.forEach { $0() }
        }
    }
}
