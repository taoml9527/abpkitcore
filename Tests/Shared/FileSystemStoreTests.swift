// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class FileSystemStoreTests: XCTestCase {

    override func setUp() {
        super.setUp()

        try? FileManager.default.removeItem(at: containerURL())
    }

    override func tearDown() {
        super.tearDown()

        try? FileManager.default.removeItem(at: containerURL())
    }

    func test_emptyCacheRetrieve_throwsError() throws {
        let sut = try makeSUT()
        expectOperation(curry(sut.read)("name"),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==))
    }

    func test_emptyCacheRetrieve_noSideEffects() throws {
        let sut = try makeSUT()
        expectOperation(curry(sut.read)("name"),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==))
        expectOperation(curry(sut.read)("name"),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==))
    }

    func test_retrieveAfterInsert_returnsInsertedData() throws {
        let sut = try makeSUT()

        let data = anyData()
        let name = "filterListName"

        expectOperation(curry(sut.write)(data, name),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==))
        expectOperation(curry(sut.read)(name),
                        toCompleteWith: .success(data),
                        isEqual: resultIsEqual(==))
    }

    func test_retrieve_noSideEffects() throws {
        let sut = try makeSUT()
        let data = anyData()
        let name = "filterListName"
        expectOperation(curry(sut.write)(data, name),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==))
        expectOperation(curry(sut.read)(name),
                        toCompleteWith: .success(data),
                        isEqual: resultIsEqual(==))
        expectOperation(curry(sut.read)(name),
                        toCompleteWith: .success(data),
                        isEqual: resultIsEqual(==))
    }

    func test_insert_overridesPreviousData() throws {
        let sut = try makeSUT()

        let name = "filterListName"
        let oldData = try XCTUnwrap("old".data(using: .utf8))
        expectOperation(curry(sut.write)(oldData, name),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==))

        expectOperation(curry(sut.read)(name),
                        toCompleteWith: .success(oldData),
                        isEqual: resultIsEqual(==))

        let newData = try XCTUnwrap("new".data(using: .utf8))
        expectOperation(curry(sut.write)(newData, name),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==))
        expectOperation(curry(sut.read)(name),
                        toCompleteWith: .success(newData),
                        isEqual: resultIsEqual(==))
    }

    func test_insertFailed_throwsError() throws {
        let invalidStoreURL = try XCTUnwrap(URL(string: "Invalid://store"))
        let sut = try makeSUT(storeURL: invalidStoreURL)

        expectOperation(curry(sut.write)(anyData(), "filterListName"),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==))
    }

    func test_sideEffects_runSerially() throws {
        let sut = try makeSUT()

        var orderedExpectations = [XCTestExpectation]()

        let exp1 = expectation(description: "exp1")
        let name = "filterListName"
        let data1 = try XCTUnwrap("data1".data(using: .utf8))
        sut.write(data: data1,
                  toFileNamed: name) { _ in
            orderedExpectations.append(exp1)
            exp1.fulfill()
        }

        let exp2 = expectation(description: "exp2")
        let data2 = try XCTUnwrap("data2".data(using: .utf8))
        sut.write(data: data2, toFileNamed: name) { _ in
            orderedExpectations.append(exp2)
            exp2.fulfill()
        }

        let exp3 = expectation(description: "exp3")
        let data3 = try XCTUnwrap("data3".data(using: .utf8))
        sut.write(data: data3, toFileNamed: name) { _ in
            orderedExpectations.append(exp3)
            exp3.fulfill()
        }

        waitForExpectations(timeout: 1.0)
        XCTAssertEqual(orderedExpectations,
                       [exp1, exp2, exp3],
                       "Expected side-effects to run serially, operations finished in wrong order")
    }

    // MARK: - Filter list cache

    func test_filterListRetrieve_missingFilterList_throwsFailedToLoadFilterList() throws {
        let sut = try makeSUT()

        let filterListName = "anyName"
        expectOperation(curry(sut.retrieveFilterList)(filterListName),
                        toCompleteWith: .anyFailureWithCode(.failedToLoadCachedFilterList),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))
    }

//    func test_filterListRetrieve_malformedFilterList_throwsFailedToDecodeFilterList() {
//        let sut = makeSUT()
//
//        let filterListName = "anyName"
//        try? "invalidat filter list".data(using: .utf8)?
//              .write(to: containerURL().appendingPathComponent(filterListName))
//
//        expectOperation(curry(sut.retrieveFilterList)(filterListName),
//                        toCompleteWith: .anyFailureWithCode(.failedToDecodeFilterList),
//                        isEqual: resultIsEqual(==, errorCodeIsEqual))
//    }

    func test_filterListInsert_failed_throwsFailedToInsertFilterList() throws {
        let invalidStoreURL = try XCTUnwrap(URL(string: "Invalid://store"))
        let sut = try makeSUT(storeURL: invalidStoreURL)

        expectOperation(curry(sut.insertFilterList)(anyFilterList().content, "filterListName"),
                        toCompleteWith: .anyFailureWithCode(.failedToCacheFilterList),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))

    }

    func test_filterList_insertRetrieveIsSuccessful() throws {
        // We test just the simple insert/retrieve.
        // Any other behaviour is assured by the tests above
        let sut = try makeSUT()

        let rawFilterList = anyFilterList().content
        let filterListName = "anyName"
        expectOperation(curry(sut.insertFilterList)(rawFilterList, filterListName),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==))

        expectOperation(curry(sut.retrieveFilterList)(filterListName),
                        toCompleteWith: .success(rawFilterList),
                        isEqual: resultIsEqual(==))
    }

    // MARK: - Meta info cache

    func test_retrieveMetaInfo_malformedMetaInfo_throwsFailedToDecodeFilterListMetaInfo() throws {
        let sut = try makeSUT()
        let storeURL = try containerURL().appendingPathComponent("abpKitFilterListMetaInfo.store")
        try? "invalid".write(to: storeURL,
                             atomically: true,
                             encoding: .utf8)
        expectOperation(sut.retrieveMetaInfoList,
                        toCompleteWith: .anyFailureWithCode(.failedToDecodeFilterListMetaInfo),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))
    }

    func test_retrieveMetaInfo_failedToRetrieve_throwsFailedToLoadFilterListMetaInfo() throws {
        let sut = try makeSUT()

        expectOperation(sut.retrieveMetaInfoList,
                        toCompleteWith: .anyFailureWithCode(.failedToLoadFilterListMetaInfo),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))

    }

    func test_insertMetaInfo_failedToInsert_throwsFailedToInsertFilterListMetaInfo() throws {
        let invalidStoreURL = try XCTUnwrap(URL(string: "Invalid://store"))
        let sut = try makeSUT(storeURL: invalidStoreURL)

        expectOperation(curry(sut.insertMetaInfoList)([]),
                        toCompleteWith: .anyFailureWithCode(.failedToInsertFilterListMetaInfo),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))

    }

    func test_metaInfo_insertRetrieve_isSuccessful() throws {
        let sut = try makeSUT()

        let metaInfo = [anyDownloadedFilterListMetaInfo()]
        expectOperation(curry(sut.insertMetaInfoList)(metaInfo),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==))

        expectOperation(sut.retrieveMetaInfoList,
                        toCompleteWith: .success(metaInfo),
                        isEqual: resultIsEqual(==))
    }

    // MARK: - Allowed domains cache

    func test_allowedDomains_insertRetrieve_isSuccessful() throws {
        let sut = try makeSUT()
        let anyDomains = [anyAllowedDomain()]

        expectOperation(curry(sut.insertDomains)(anyDomains),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==))

        expectOperation(sut.retrieveDomains,
                        toCompleteWith: .success(anyDomains),
                        isEqual: resultIsEqual(==))
    }

    func test_allowedDomains_missingFile_throwsFailedToLoadAllowedDomainsError() throws {
        let sut = try makeSUT()

        expectOperation(sut.retrieveDomains,
                        toCompleteWith: .anyFailureWithCode(.failedToLoadAllowedDomains),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))
    }

    func test_allowedDomains_malformedStoredData_throwsFailedToDecodeError() throws {
        let sut = try makeSUT()

        try "invalid data".data(using: .utf8)?
                          .write(to: containerURL()
                          .appendingPathComponent("abpKitAllowedDomains.store"))

        expectOperation(sut.retrieveDomains,
                        toCompleteWith: .anyFailureWithCode(.failedToDecodeAllowedDomains),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))
    }

    // MARK: - Helpers

    private func makeSUT(storeURL: URL? = nil,
                         file: StaticString = #file,
                         line: UInt = #line) throws -> FileSystemStore {
        let sut = try FileSystemStore(containerURL: storeURL ?? containerURL())

        trackForMemoryLeaks(sut, file: file, line: line)

        return sut
    }

    private func containerURL() throws -> URL {
        let url = try FileManager.default
                                 .url(for: .applicationSupportDirectory,
                                      in: .userDomainMask,
                                      appropriateFor: nil,
                                      create: true)
                                 .appendingPathComponent("FileSystemTest", isDirectory: true)

        try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true)
        return url
    }
}
