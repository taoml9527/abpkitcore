// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class RemoteSourceLoadWithBundleFallbackTests: XCTestCase {

    func test_load_loadsRulesForCorrectSource() {
        let (sut, _, remoteSourceLoader) = makeSUT()

        let source = anyFilterListRemoteSource()
        sut.loadFilterList(forSource: source, completion: { _ in })
        XCTAssertEqual(remoteSourceLoader.requestedParams,
                       [source],
                       "Expected to request load for \(source), instead requested for \(remoteSourceLoader.requestedParams)")
    }

    func test_load_rulesLoadFromRemoteSourceSucceeded_completesWithRules() {
        let (sut, bundleLoader, remoteSourceLoader) = makeSUT()

        let filterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            remoteSourceLoader.completeOperation(with: .success(filterList))
        })
        XCTAssertTrue(bundleLoader.requestedParams.isEmpty,
                      "Expected no bundle requests when remote load succeeded")
    }

    func test_load_rulesLoadFromRemoteSourceFailed_requestsLoadForBundleSource() {
        // Custom source
        let path = "https://easylist-downloads.adblockplus.org/fanboy-annoyance.json"
        expectFallbackToCorrectBundledSource(for: .custom(path),
                                             expectedBundledSource: .custom("fanboy-annoyance.json"))

        // AA sources
        expectFallbackToCorrectBundledSource(for: .abp(.withAA(.easyList)),
                                             expectedBundledSource: .abp(.withAA(.easyList)))
        expectFallbackToCorrectBundledSource(for: .abp(.withAA(.easyListRussiaUkraine)),
                                             expectedBundledSource: .abp(.withAA(.easyListRussiaUkraine)))
        expectFallbackToCorrectBundledSource(for: .abp(.withAA(.easyListSpain)),
                                             expectedBundledSource: .abp(.withAA(.easyListSpain)))
        expectFallbackToCorrectBundledSource(for: .abp(.withAA(.easyListItaly)),
                                             expectedBundledSource: .abp(.withAA(.easyListItaly)))
        expectFallbackToCorrectBundledSource(for: .abp(.withAA(.easyListGermany)),
                                             expectedBundledSource: .abp(.withAA(.easyListGermany)))
        expectFallbackToCorrectBundledSource(for: .abp(.withAA(.easyListFrance)),
                                             expectedBundledSource: .abp(.withAA(.easyListFrance)))
        expectFallbackToCorrectBundledSource(for: .abp(.withAA(.easyListDutch)),
                                             expectedBundledSource: .abp(.withAA(.easyListDutch)))
        expectFallbackToCorrectBundledSource(for: .abp(.withAA(.easyListChina)),
                                             expectedBundledSource: .abp(.withAA(.easyListChina)))

        // Non AA sources
        expectFallbackToCorrectBundledSource(for: .abp(.withoutAA(.easyList)),
                                             expectedBundledSource: .abp(.withoutAA(.easyList)))
        expectFallbackToCorrectBundledSource(for: .abp(.withoutAA(.easyListRussiaUkraine)),
                                             expectedBundledSource: .abp(.withoutAA(.easyListRussiaUkraine)))
        expectFallbackToCorrectBundledSource(for: .abp(.withoutAA(.easyListSpain)),
                                             expectedBundledSource: .abp(.withoutAA(.easyListSpain)))
        expectFallbackToCorrectBundledSource(for: .abp(.withoutAA(.easyListItaly)),
                                             expectedBundledSource: .abp(.withoutAA(.easyListItaly)))
        expectFallbackToCorrectBundledSource(for: .abp(.withoutAA(.easyListGermany)),
                                             expectedBundledSource: .abp(.withoutAA(.easyListGermany)))
        expectFallbackToCorrectBundledSource(for: .abp(.withoutAA(.easyListFrance)),
                                             expectedBundledSource: .abp(.withoutAA(.easyListFrance)))
        expectFallbackToCorrectBundledSource(for: .abp(.withoutAA(.easyListDutch)),
                                             expectedBundledSource: .abp(.withoutAA(.easyListDutch)))
        expectFallbackToCorrectBundledSource(for: .abp(.withoutAA(.easyListChina)),
                                             expectedBundledSource: .abp(.withoutAA(.easyListChina)))
    }

    func test_load_loadingFromBundleCompletesWithCorrectResult() {
        let (sut, bundleLoader, remoteSourceLoader) = makeSUT()

        let filterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            remoteSourceLoader.completeOperation(with: .anyFailure, at: 0)
                            bundleLoader.completeOperation(with: .success(filterList), at: 0)
        })

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            remoteSourceLoader.completeOperation(with: .anyFailure, at: 1)
                            bundleLoader.completeOperation(with: .anyFailure, at: 1)
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (FilterListTypeLoader, FilterListTypeLoaderMock, FilterListTypeLoaderMock) {
        let bundleLoader = FilterListTypeLoaderMock()
        let remoteLoader = FilterListTypeLoaderMock()

        let sut = RemoteSourceFilterListLoaderWithBundleFallback(decoratee: remoteLoader,
                                                                 bundleSourceLoader: bundleLoader)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, bundleLoader, remoteLoader)
    }

    private func expectFallbackToCorrectBundledSource(for remoteSource: FilterListType,
                                                      expectedBundledSource: FilterListType,
                                                      file: StaticString = #file,
                                                      line: UInt = #line) {
        let (sut, bundleLoader, remoteSourceLoader) = makeSUT()

        sut.loadFilterList(forSource: remoteSource, completion: { _ in })
        remoteSourceLoader.completeOperation(with: .anyFailure)

        XCTAssertEqual(bundleLoader.requestedParams,
                       [expectedBundledSource],
                       "Expected  to load for bundled source \(expectedBundledSource), instead loaded for \(bundleLoader.requestedParams) ")
    }
}
