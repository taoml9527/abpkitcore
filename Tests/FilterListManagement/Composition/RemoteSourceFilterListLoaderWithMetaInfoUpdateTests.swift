// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class RemoteSourceFilterListLoaderWithMetaInfoUpdateTests: XCTestCase {

    func test_load_loadForCorrectSource() {
        let (sut, decoratee, _) = makeSUT()

        let source = anyFilterListRemoteSource()

        sut.loadFilterList(forSource: source, completion: { _ in })

        XCTAssertEqual(decoratee.requestedParams,
                       [source],
                       "Expected to request load for \([source]), instead requested for \(decoratee.requestedParams)")
    }

    func test_load_decorateeFailed_throwsError() {
        let (sut, decoratee, _) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            decoratee.completeOperation(with: .anyFailure)
        })
    }

    func test_load_decorateeSuccess_completesWithFilterList() {
        let (sut, decoratee, metaInfoStore) = makeSUT()

        let filterList = anyFilterList()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success(filterList))
                            metaInfoStore.completeOperation(with: .anyFailure)
        })
    }

    func test_load_decorateeSuccess_reuqestsUpdateForCorrectSource() {
        let (sut, decoratee, metaInfoUpdater) = makeSUT()

        let source = anyFilterListRemoteSource()
        sut.loadFilterList(forSource: source, completion: { _ in })

        decoratee.completeOperation(with: .success(anyFilterList()))

        XCTAssertEqual(metaInfoUpdater.requestedParams,
                       [source],
                       "Expected to request met info update for \(source), requested instead for \(metaInfoUpdater.requestedParams)")
    }

    // MARK: - Helpers

    func makeSUT(file: StaticString = #file,
                 line: UInt = #line) -> (FilterListTypeLoader, FilterListTypeLoaderMock,
        FilterListMetaInfoUpdateMock) {
            let decoratee = FilterListTypeLoaderMock()
            let metaInfoUpdater = FilterListMetaInfoUpdateMock()
            let sut = FilterListTypeLoaderWithMetaInfoUpdate(decoratee: decoratee,
                                                                     metaInfoUpdater: metaInfoUpdater)

            trackForMemoryLeaks(sut, file: file, line: line)

            return (sut, decoratee, metaInfoUpdater)
    }
}
