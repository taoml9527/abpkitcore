// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

final class RemoteSourceFilterListLoaderWithCachingTests: XCTestCase {

    func test_load_requestsLoadOnDecoratee() {
        let (sut, decoratee, _) = makeSUT()

        let source = anyFilterListRemoteSource()
        sut.loadFilterList(forSource: source, completion: { _ in })
        XCTAssertEqual(decoratee.requestedParams,
                       [source],
                       "Expected to load for source \(source), instead loaded for \(decoratee.requestedParams)")
    }

    func test_load_decorateeLoadFailed_throwsErrorAndDoesNotSaveAnything() {
        let (sut, decoratee, cache) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            decoratee.completeOperation(with: .anyFailure)
        })

        XCTAssertTrue(cache.requestedParams.isEmpty,
                      "Expected to not save anything on decoratee failed to load filter list")
    }

    func test_load_decorateeLoadSucceeded_savesTheRulesForCorrectSource() {
        let (sut, decoratee, cache) = makeSUT()

        let filterList = anyFilterList()
        let source = anyFilterListRemoteSource()

        expectOperation(curry(sut.loadFilterList)(source),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            decoratee.completeOperation(with: .success(filterList))
                            cache.completeOperation(with: .success)
        })

        let expectedCacheParams = FilterListCacheParams(list: filterList, source: source)
        XCTAssertEqual(cache.requestedParams,
                       [expectedCacheParams],
                       "Expected to call cache with params \(expectedCacheParams), instead called with \(cache.requestedParams)")
    }

    func test_load_cacheFailed_returnsLoadedList() {
        let (sut, decoratee, cache) = makeSUT()

        let filterList = anyFilterList()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success(filterList))
                            cache.completeOperation(with: .anyFailure)
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (FilterListTypeLoaderWithCaching, FilterListTypeLoaderMock, FilterListCacheMock) {
        let decoratee = FilterListTypeLoaderMock()
        let filterListCache = FilterListCacheMock()
        let sut = FilterListTypeLoaderWithCaching(decoratee: decoratee,
                                                          filterListCache: filterListCache)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, decoratee, filterListCache)
    }
}
