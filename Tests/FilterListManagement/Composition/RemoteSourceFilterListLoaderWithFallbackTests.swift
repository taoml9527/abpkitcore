// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class RemoteSourceRulesLoaderWithFallbackTests: XCTestCase {

    func test_getList_primarySucceeds_returnsData() {
        let (sut, primaryLoader, fallbackLoader) = makeSUT()

        let filterList = anyFilterList()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            primaryLoader.completeOperation(with: .success(filterList))
        })

        XCTAssertTrue(fallbackLoader.requests.isEmpty,
                      "Expected to not make requests on secondary loader when primary succeeded")
    }

    func test_getList_primaryFails_loadsFromSecondary() {
        let (sut, primaryLoader, fallbackLoader) = makeSUT()

        let filterList = anyFilterList()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            primaryLoader.completeOperation(with: .anyFailure)
                            fallbackLoader.completeOperation(with: .success(filterList))
        })
    }

    func test_getList_bothStoresFail_returnsError() {
        let (sut, primaryLoader, fallbackLoader) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            primaryLoader.completeOperation(with: .anyFailure)
                            fallbackLoader.completeOperation(with: .anyFailure)
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (FilterListTypeLoader, FilterListTypeLoaderMock, FilterListTypeLoaderMock) {
        let primarySourceLoader = FilterListTypeLoaderMock()
        let fallbackSourceLoader = FilterListTypeLoaderMock()
        let sut = FilterListTypeLoaderWithFallback(primarySourceLoader: primarySourceLoader,
                                                           secondarySourceLoader: fallbackSourceLoader)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, primarySourceLoader, fallbackSourceLoader)
    }
}
