// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import XCTest
@testable import ABPKitCore

class FilterListLoaderIntegrationTests: XCTestCase {
    func test_load_bundleSource_loadsFromBundleLoader() {
        let (sut, bundledSourceLoader, remoteSourceLoader, _) = makeSUT()

        // Bundled source, loads using bundle
        let bundledSource = anyFilterListBundledSource()
        sut.loadFilterList(forSource: .bundled(bundledSource), completion: { _ in })
        XCTAssertEqual(bundledSourceLoader.requestedParams, [bundledSource])
        XCTAssertTrue(remoteSourceLoader.requestedParams.isEmpty, "Expected no remote load request when loading a bundled source")
    }

    func test_load_bundledSource_failedToLoadFromBundled_throwsError() {
        let (sut, bundledSourceLoader, _, _) = makeSUT()
        // No rules for bundled source, throws error
        expectOperation(curry(sut.loadFilterList)(.bundled(anyFilterListBundledSource())),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            bundledSourceLoader.completeOperation(with: .anyFailure)
        })
    }

    func test_load_bundledSource_successLoadingFromBundled_returnsFilterList() {
        let (sut, bundledSourceLoader, _, allowedDomainsLoader) = makeSUT()
        // Existing bundled source, returns rules
        let bundledFilterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(.bundled(anyFilterListBundledSource())),
                        toCompleteWith: .success(bundledFilterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            bundledSourceLoader.completeOperation(with: .success(bundledFilterList))
                            allowedDomainsLoader.completeLoading(with: .anyFailure)
        })
    }

    func test_load_remoteSource_loadsFromRemoteLoader() {
        let (sut, bundledSourceLoader, remoteSourceLoader, _) = makeSUT()

        // Bundled source, loads using bundle
        let remoteSource = anyFilterListRemoteSource()
        sut.loadFilterList(forSource: .remote(remoteSource), completion: { _ in })
        XCTAssertEqual(remoteSourceLoader.requestedParams, [remoteSource])
        XCTAssertTrue(bundledSourceLoader.requestedParams.isEmpty, "Expected no remote load request when loading a bundled source")
    }

    func test_load_remoteSource_successLoadingFromRemoteLoader_returnsFilterList() {
        let (sut, _, remoteSourceLoader, allowedDomainsLoader) = makeSUT()
        // Existing bundled source, returns rules
        let remoteFilterList = anyFilterList()

        expectOperation(curry(sut.loadFilterList)(.remote(anyFilterListRemoteSource())),
                        toCompleteWith: .success(remoteFilterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            remoteSourceLoader.completeOperation(with: .success(remoteFilterList))
                            allowedDomainsLoader.completeLoading(with: .anyFailure)
        })
    }

    func test_load_remotSource_failedToLoadFromRemote_fallbacksToBundleLoader() {
        let (sut, bundledSourceLoader, remoteSourceLoader, allowedDomainsLoader) = makeSUT()

        let bundledFilterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(.remote(anyFilterListRemoteSource())),
                        toCompleteWith: .success(bundledFilterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            remoteSourceLoader.completeOperation(with: .anyFailure)
                            bundledSourceLoader.completeOperation(with: .success(bundledFilterList))
                            allowedDomainsLoader.completeLoading(with: .anyFailure)
        })
    }

    func test_load_remotSource_failedToLoadFromRemoteAndBundle_throwsError() {
        let (sut, bundledSourceLoader, remoteSourceLoader, _) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(.remote(anyFilterListRemoteSource())),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            remoteSourceLoader.completeOperation(with: .anyFailure)
                            bundledSourceLoader.completeOperation(with: .anyFailure)
        })
    }

    func test_load_addsAllowedDomains() {
        let (sut, _, remoteSourceLoader, allowedDomainsLoader) = makeSUT()

        let filterList = anyFilterList()
        let allowedDomain = anyAllowedDomain()
        let filterListWithAllowedDomain = filterListWithAllowedDomainAdded(filterList,
                                                                           allowedDomain: allowedDomain)

        expectOperation(curry(sut.loadFilterList)(.remote(anyFilterListRemoteSource())),
                        toCompleteWith: .success(filterListWithAllowedDomain),
                        isEqual: resultIsEqual(==),
                        when: {
                            remoteSourceLoader.completeOperation(with: .success(filterList))
                            allowedDomainsLoader.completeLoading(with: .success([allowedDomain]))
        })
    }

    func test_load_failedToLoadAllowedDomains_completesWithLoadedFilterList() {
        let (sut, _, remoteSourceLoader, allowedDomainsLoader) = makeSUT()

        let filterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(.remote(anyFilterListRemoteSource())),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            remoteSourceLoader.completeOperation(with: .success(filterList))
                            allowedDomainsLoader.completeLoading(with: .anyFailure)
        })
    }

    // MARK: - Helpers

    private func makeSUT() -> (FilterListLoader, FilterListTypeLoaderMock, FilterListTypeLoaderMock, AllowedDomainsLoaderMock) {
        let bundleSourceLoader = FilterListTypeLoaderMock()
        let remoteSourceLoader = FilterListTypeLoaderMock()
        let alloweDomainsLoader = AllowedDomainsLoaderMock()

        let composer = FilterListManagementComposer(clientMetaData: anyClientMetaData(),
                                                    clientAppBundle: Bundle.main,
                                                    storageContainerURL: anyContainerURL(),
                                                    systemTime: Date.init)
        composer.bundleSourceFilterListLoader = bundleSourceLoader
        composer.remoteSourceFilterListLoader = remoteSourceLoader
        composer.allowedDomainsLoader = alloweDomainsLoader

        let sut = composer.filterListLoader

        return (sut, bundleSourceLoader, remoteSourceLoader, alloweDomainsLoader)
    }
}
