// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class RefreshingRemoteSourceFilterListLoaderTests: XCTestCase {

    func test_load_reuqestsRefreshCheckForCorrectSource() {
        let (sut, refreshStatusLoader, _, _) = makeSUT()

        let source = anyFilterListRemoteSource()
        sut.loadFilterList(forSource: source, completion: { _ in })

        XCTAssertEqual(refreshStatusLoader.requestedParams, [source])
    }

    func test_load_noRefreshNeeded_loadsUsingOldFilterListLoader() {
        let (sut, refreshStatusLoader, oldFilterListLoader, freshFilterListLoader) = makeSUT()

        let source = anyFilterListRemoteSource()
        sut.loadFilterList(forSource: source, completion: { _ in })
        refreshStatusLoader.completeOperation(with: .noRefreshNeeded)

        XCTAssertEqual(oldFilterListLoader.requestedParams, [source])
        XCTAssertTrue(freshFilterListLoader.requestedParams.isEmpty)
    }

    func test_load_oldFilterListLoading_completesWithCorrectResult() {
        let (sut, refreshStatusLoader, oldFilterListLoader, _) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            refreshStatusLoader.completeOperation(with: .noRefreshNeeded, at: 0)
                            oldFilterListLoader.completeOperation(with: .anyFailure, at: 0)
        })

        let filterList = anyFilterList()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            refreshStatusLoader.completeOperation(with: .noRefreshNeeded, at: 1)
                            oldFilterListLoader.completeOperation(with: .success(filterList), at: 1)
        })
    }

    func test_load_needsRefresh_loadsUsingFreshFilterListLoader() {
        let (sut, refreshStatusLoader, oldFilterListLoader, freshFilterListLoader) = makeSUT()

        let source = anyFilterListRemoteSource()
        sut.loadFilterList(forSource: source, completion: { _ in })
        refreshStatusLoader.completeOperation(with: .needsRefresh)

        XCTAssertEqual(freshFilterListLoader.requestedParams, [source])
        XCTAssertTrue(oldFilterListLoader.requestedParams.isEmpty)
    }

    func test_load_freshFilterListLoading_completesWithCorrectResult() {
        let (sut, refreshStatusLoader, _, freshFilterListLoader) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            refreshStatusLoader.completeOperation(with: .needsRefresh, at: 0)
                            freshFilterListLoader.completeOperation(with: .anyFailure, at: 0)
        })

        let filterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            refreshStatusLoader.completeOperation(with: .needsRefresh, at: 1)
                            freshFilterListLoader.completeOperation(with: .success(filterList), at: 1)
        })
    }

    // MARK: - Helpers

    func makeSUT(file: StaticString = #file,
                 line: UInt = #line) -> (FilterListTypeLoader, FilterListRefreshStatusLoaderMock,
                                         FilterListTypeLoaderMock, FilterListTypeLoaderMock) {
            let refreshStatusLoader = FilterListRefreshStatusLoaderMock()
            let oldFilterListLoader = FilterListTypeLoaderMock()
            let freshFilterListLoader = FilterListTypeLoaderMock()
            let sut = RefreshingFilterListTypeLoader(refreshStatusLoader: refreshStatusLoader,
                                                             oldFilterListLoader: oldFilterListLoader,
                                                             freshFilterListLoader: freshFilterListLoader)

            trackForMemoryLeaks(sut, file: file, line: line)

            return (sut, refreshStatusLoader, oldFilterListLoader, freshFilterListLoader)
    }
}
