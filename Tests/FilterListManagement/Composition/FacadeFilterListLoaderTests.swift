// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import XCTest
@testable import ABPKitCore

class FacadeFilterListLoaderTests: XCTestCase {

    func test_load_bundledSource_forwardsToBundledLoader() {
        let (sut, bundleLoader, remoteLoader) = makeSUT()

        let bundledSource = anyFilterListBundledSource()
        sut.loadFilterList(forSource: .bundled(bundledSource), completion: { _ in })

        XCTAssertEqual(bundleLoader.requestedParams,
                       [bundledSource],
                       "Expected to request bundled source \(bundledSource), instead requested \(bundleLoader.requestedParams)")
        XCTAssertTrue(remoteLoader.requestedParams.isEmpty,
                      "Expected to not make any remote loader requests for bundled source")
    }

    func test_load_bundledSource_loadFailed_completesWithFailure() {
        let (sut, bundleLoader, _) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(.bundled(anyFilterListBundledSource())),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            bundleLoader.completeOperation(with: .anyFailure)
        })
    }

    func test_load_bundledSource_loadSucceeded_completesWithFilterList() {
        let (sut, bundleLoader, _) = makeSUT()

        let filterList = anyFilterList()

        expectOperation(curry(sut.loadFilterList)(.bundled(anyFilterListBundledSource())),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            bundleLoader.completeOperation(with: .success(filterList))
        })
    }

    func test_load_remoteSource_forwardsToRemoteLoader() {
        let (sut, bundleLoader, remoteLoader) = makeSUT()

        let remoteSource = anyFilterListRemoteSource()
        sut.loadFilterList(forSource: .remote(remoteSource), completion: { _ in })

        XCTAssertEqual(remoteLoader.requestedParams,
                       [remoteSource],
                       "Expected to request remote source \(remoteSource), instead requested \(remoteLoader.requestedParams)")
        XCTAssertTrue(bundleLoader.requestedParams.isEmpty,
                      "Expected to not make any bundle loader requests for remote source")
    }

    func test_load_remoteSource_loadFailed_completesWithFailure() {
        let (sut, _, remoteLoader) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(.remote(anyFilterListRemoteSource())),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            remoteLoader.completeOperation(with: .anyFailure)
        })
    }

    func test_load_remoteSource_loadSucceeded_completesWithRules() {
        let (sut, _, remoteLoader) = makeSUT()

        let filterList = anyFilterList()

        expectOperation(curry(sut.loadFilterList)(.remote(anyFilterListRemoteSource())),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            remoteLoader.completeOperation(with: .success(filterList))
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (FilterListLoaderStrategy, FilterListTypeLoaderMock, FilterListTypeLoaderMock) {
        let bundledLoader = FilterListTypeLoaderMock()
        let remoteLoader = FilterListTypeLoaderMock()
        let sut = FilterListLoaderStrategy(bundledSourceLoader: bundledLoader, remoteSourceLoader: remoteLoader)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, bundledLoader, remoteLoader)
    }
}
