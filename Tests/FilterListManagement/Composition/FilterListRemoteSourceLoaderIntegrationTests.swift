// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import XCTest
@testable import ABPKitCore

// This tests are meant to test only high level acceptance requirements
// All of the other scenarios are tested at unit level
class FilterListRemoteSourceLoaderIntegrationTests: XCTestCase {

    func test_load_remoteLoadPath() throws {
        // This path tests that if the filter list is missing in cache then
        // it is downloaded from remote source, then the meta info for the list
        // is updated accordingly, as well the filter list is inserted to the cache

        // - Setup

        let currentDate = Date()
        let clientMetaData = anyClientMetaData()
        let (sut, metaInfoStore, filterListCacheStore, httpClient) = makeSUT(systemDate: { currentDate })

        // - Act/Assert

        let source = FilterListType.abp(.withAA(.easyList))
        let remoteFilterList = remoteEasyListRules()

        let filterListData = try XCTUnwrap(remoteFilterList.content.data(using: .utf8))
        let filterListGetResult = FilterListGetResult(data: filterListData,
                                                      httpResponse: anySuccessHTTPURLResponse())

        // Assert completes with remote filter list
        expectOperation(curry(sut.loadFilterList)(.remote(source)),
                        toCompleteWith: .success(remoteFilterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            // Complete retrieve for refresh status check
                            metaInfoStore.completeRetrieve(withResult: .success([]))
                            // Complete retrieve for url build. Meta info is used to build the correct url params
                            metaInfoStore.completeRetrieve(withResult: .success([]), at: 1)

                            // Complete remote loading with success
                            httpClient.completeOperation(with: .success(filterListGetResult))

                            // Complete retrieve for meta info update. After a successful download,
                            // the old meta info should be updated
                            metaInfoStore.completeRetrieve(withResult: .success([]), at: 2)

                            // Complete new filter list insert with success
                            filterListCacheStore.completeInsert(with: .success(()))
        })

        let expectedLoadURL = try makeEasyListURLFor(clientMetaData: clientMetaData,
                                                     downloadCount: 0,
                                                     lastVersion: 0)

        // Assert side effects
        XCTAssertEqual(filterListCacheStore.requestedRetrieves,
                       [],
                       "Expected no filter list cache retrieval if no meta exists for source")
        XCTAssertEqual(httpClient.requestedParams,
                       [expectedLoadURL],
                       "Expected to request remote load with url \(expectedLoadURL), requested instead for \(httpClient.requestedParams)")
        XCTAssertEqual(filterListCacheStore.requestedInserts,
                       [.init(filterList: remoteEasyListRules().content,
                              filterListName: "easyList+exceptionRules.json")],
                       "Expected to insert to cache the downloaded filter list")
        XCTAssertEqual(metaInfoStore.insertRequest,
                       [[.init(source: source,
                               lastDownloadDate: currentDate,
                               downloadCount: 1)]],
                       "Expected to insert meta info to cache for downloaded filter list")
    }

    func test_load_cacheLoadPath() {
        // This path tests that if the filter list is cached locally and it is not expired
        // then it should be loaded from cache, and no remote calls, as well meta info update should be made.

        // - Setup

        // Freeze time to current date
        let currentDate = Date()
        let (sut, metaInfoStore, filterListCacheStore, httpClient) = makeSUT(systemDate: { currentDate })

        // - Act/Assert

        let source = FilterListType.abp(.withAA(.easyList))
        let notExpiredDownloadDate = currentDate.adding(seconds: -10)
        let metaInfo = DownloadedFilterListMetaInfo(source: source,
                                                    lastDownloadDate: notExpiredDownloadDate,
                                                    downloadCount: 2)

        let cachedFilterList = cachedEasyListRules().content
        expectOperation(curry(sut.loadFilterList)(.remote(source)),
                        toCompleteWith: .success(FilterList(content: cachedFilterList)),
                        isEqual: resultIsEqual(==),
                        when: {
                            // Complete retrieval for refresh status check
                            metaInfoStore.completeRetrieve(withResult: .success([metaInfo]))
                            // Complete filter list load from cache
                            filterListCacheStore.completeRetrieval(with: .success(cachedEasyListRules().content))
        })

        XCTAssertEqual(httpClient.requestedParams,
                       [],
                       "Expected no remote requests if load from cache succeeded")
    }

    func test_load_expiredFilterListPath() throws {
        // This path tests that if the filter list cached locally but it is expired then it should be
        // downloaded from remote source, then cached locally, as well the meta info should be updated accordingly

        // - Setup

        let currentDate = Date()
        let clientMetaData = anyClientMetaData()
        let (sut, metaInfoStore, filterListCacheStore, httpClient) = makeSUT(systemDate: { currentDate })

        // - Act

        let source = FilterListType.abp(.withAA(.easyList))
        let expiredDownloadDate = currentDate.minusFilterListExpirationTimetamp()
        let expiredMetaInfo = DownloadedFilterListMetaInfo(source: source,
                                                           lastDownloadDate: expiredDownloadDate,
                                                           downloadCount: 2)
        let remoteFilterList = remoteEasyListRules()

        let filterListData = try XCTUnwrap(remoteFilterList.content.data(using: .utf8))
        let filterListGetResult = FilterListGetResult(data: filterListData,
                                                      httpResponse: anySuccessHTTPURLResponse())
        expectOperation(curry(sut.loadFilterList)(.remote(source)),
                        toCompleteWith: .success(remoteFilterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            // Complete retrieve for refresh status check
                            metaInfoStore.completeRetrieve(withResult: .success([expiredMetaInfo]))

                            // Complete retrieve for url build. Meta info is used to build the correct url params
                            metaInfoStore.completeRetrieve(withResult: .success([expiredMetaInfo]), at: 1)

                            // Complete remote loading with success
                            httpClient.completeOperation(with: .success(filterListGetResult))

                            // Complete retrieve for meta info update. After a successful download,
                            // the old meta info should be updated
                            metaInfoStore.completeRetrieve(withResult: .success([expiredMetaInfo]), at: 2)

                            // Complete new filter list insert with success
                            filterListCacheStore.completeInsert(with: .success(()))
        })

        let lastVersion = try XCTUnwrap(Int(expiredMetaInfo.lastDownloadDate.asTimestamp()))
        let expectedURL = try makeEasyListURLFor(clientMetaData: clientMetaData,
                                                 downloadCount: expiredMetaInfo.downloadCount,
                                                 lastVersion: lastVersion)

        // - Assert

        XCTAssertEqual(filterListCacheStore.requestedRetrieves,
                       [],
                       "Expected no filter list cache retrieval if filter list is expired")
        XCTAssertEqual(httpClient.requestedParams,
                       [expectedURL],
                       "Expected to request remote load with url \(expectedURL), requested instead for \(httpClient.requestedParams)")
        XCTAssertEqual(filterListCacheStore.requestedInserts,
                       [.init(filterList: remoteFilterList.content,
                              filterListName: "easyList+exceptionRules.json")],
                       "Expected to insert to cache the downloaded filter list")
        XCTAssertEqual(metaInfoStore.insertRequest,
                       [[.init(source: source,
                               lastDownloadDate: currentDate,
                               downloadCount: expiredMetaInfo.downloadCount + 1)]],
                       "Expected to update filter list meta data on filter list refresh")
    }

    // MARK: - Helpers

    private func makeSUT(clientMetaData: ClientMetaData = anyClientMetaData(),
                         systemDate: @escaping () -> Date) -> (FilterListLoader,
        FilterListMetaInfoStoreMock, FilterListCacheStoreMock, HTTPClientMock) {
            let metaInfoStore = FilterListMetaInfoStoreMock()
            let filterListCacheStore = FilterListCacheStoreMock()
            let httpClient = HTTPClientMock()

            let composer = FilterListManagementComposer(clientMetaData: clientMetaData,
                                                        clientAppBundle: Bundle.main,
                                                        storageContainerURL: anyContainerURL(),
                                                        systemTime: systemDate)
            composer.httpClient = httpClient
            composer.metaInfoStore = metaInfoStore
            composer.filterListCacheStore = filterListCacheStore

            let sut = composer.filterListLoader

            return (sut, metaInfoStore, filterListCacheStore, httpClient)
    }

    private func makeEasyListURLFor(clientMetaData: ClientMetaData,
                                    downloadCount: Int = 0,
                                    lastVersion: Int = 0) throws -> URL {
        return try XCTUnwrap(URL(string: "https://easylist-downloads.adblockplus.org/easylist_min+exceptionrules_content_blocker.json?addonName=\(clientMetaData.addon.name)&addonVersion=\(clientMetaData.addon.version)&application=\(clientMetaData.app.name)&applicationVersion=\(clientMetaData.app.version)&platform=\(clientMetaData.platform.name)&platformVersion=\(clientMetaData.platform.version)&lastVersion=\(lastVersion)&downloadCount=\(downloadCount)"))
    }

    private func remoteEasyListRules() -> FilterList {
        .init(content: "[RemoteRules]")
    }

    private func cachedEasyListRules() -> FilterList {
        .init(content: "[CachedRules]")
    }
}
