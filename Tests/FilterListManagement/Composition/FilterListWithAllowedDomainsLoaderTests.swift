// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class FilterListWithAllowedDomainsLoaderTests: XCTestCase {

    func test_load_requestsLoadForCorrectSource() {
        let (sut, decoratee, _) = makeSUT()
        let loadSource = anyFilterListSource()
        sut.loadFilterList(forSource: loadSource, completion: { _ in })
        XCTAssertEqual(decoratee.requestedParams,
                       [loadSource],
                       "Expected to request rules load for \(loadSource), instead requested for \(decoratee.requestedParams)")
    }

    func test_load_decorateeFailed_throwsError() {
        let (sut, decoratee, allowedDomainsLoader) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(anyFilterListSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            decoratee.completeOperation(with: .anyFailure)
        })

        XCTAssertTrue(allowedDomainsLoader.commands.isEmpty, "Expected no whitelisted domains request if failed to load rules")
    }

    func test_load_allowedDomainsLoadFailed_returnsLoadedFilterList() {
        let (sut, decoratee, allowedDomainsLoader) = makeSUT()

        let filterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(anyFilterListSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success(filterList))
                            allowedDomainsLoader.completeLoading(with: .anyFailure)
        })
    }

    func test_load_wlLoadSuccess_emptyDomains_returnsOriginalRules() {
        let (sut, decoratee, allowedDomainsLoader) = makeSUT()

        let filterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(anyFilterListSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success(filterList))
                            allowedDomainsLoader.completeLoading(with: .success([]))
        })
    }

    func test_load_wlLoadSuccess_appendAllowedDomainsToFilterList() {
        let (sut, decoratee, allowedDomainsLoader) = makeSUT()

        let loadedFilterList = anyFilterList()
        let allowedDomain = anyAllowedDomain()
        let filterListWithAllowedDomains = filterListWithAllowedDomainAdded(loadedFilterList,
                                                                            allowedDomain: allowedDomain)

        expectOperation(curry(sut.loadFilterList)(anyFilterListSource()),
                        toCompleteWith: .success(filterListWithAllowedDomains),
                        isEqual: resultIsEqual(==),
                        when: {
                            decoratee.completeOperation(with: .success(loadedFilterList))
                            allowedDomainsLoader.completeLoading(with: .success([allowedDomain]))
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (FilterListLoader, FilterListLoaderMock, AllowedDomainsLoaderMock) {
        let decoratee = FilterListLoaderMock()
        let allowedDomainsLoader = AllowedDomainsLoaderMock()
        let sut = FilterListWithAllowedDomainsLoader(decoratee: decoratee, allowedDomainsLoader: allowedDomainsLoader)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, decoratee, allowedDomainsLoader)
    }
}
