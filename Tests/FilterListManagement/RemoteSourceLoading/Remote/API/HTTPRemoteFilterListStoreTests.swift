// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class HTTPRemoteFilterListStoreTests: XCTestCase {

    func test_download_requestsWithCorrectURL() {
        let (sut, client) = makeSUT()

        let url = anyHTTPURL()
        sut.download(from: url, completion: { _ in })

        XCTAssertEqual(client.requestedParams,
                       [url],
                       "Expected to request rules for \(url), instead requests for \(client.requestedParams)")
    }

    func test_download_failsOnClientError() {
        let (sut, client) = makeSUT()

        expectOperation(curry(sut.download)(anyHTTPURL()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            client.completeOperation(with: .anyFailure)
        })
    }

    func test_download_failsOnInvalidHTTPStatusCode() throws {
        let (sut, client) = makeSUT()

        let codes = [199, 301, 400, 404, 500]

        try codes.enumerated().forEach { index, code in
            let completionResult = try makeResult(withStatusCode: code)
            expectOperation(curry(sut.download)(anyHTTPURL()),
                            toCompleteWith: .failure(ABPKitCoreError.filterListBadDownloadErrorCode(code)),
                            isEqual: resultIsEqual(==, nsErrorIsEqual),
                            when: {
                                client.completeOperation(with: .success(completionResult),
                                                         at: index)
                            })
        }
    }

    func test_download_validadStatusCode_getsData() throws {
        let (sut, downloader) = makeSUT()

        let rawRules = "[{trigger: {}, action: {}}]"
        let data = Data(rawRules.utf8)

        let completionResult = try makeResult(data: data)
        expectOperation(curry(sut.download)(anyHTTPURL()),
                        toCompleteWith: .success(FilterList(content: rawRules)),
                        isEqual: resultIsEqual(==),
                        when: {
                            downloader.completeOperation(with: .success(completionResult))
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file, line: UInt = #line) -> (HTTPRemoteFilterListStore, HTTPClientMock) {
        let client = HTTPClientMock()
        let sut = HTTPRemoteFilterListStore(httpClient: client)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, client)
    }

    private func makeResult(withStatusCode code: Int = 200, data: Data = anyData()) throws -> FilterListGetResult {
        let response = try XCTUnwrap(HTTPURLResponse(url: anyHTTPURL(),
                                                     statusCode: code,
                                                     httpVersion: nil,
                                                     headerFields: nil),
                                     "Failed to crete URL response")

        return .init(data: data, httpResponse: response)
    }
}
