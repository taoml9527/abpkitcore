// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class URLSessionHTTPDownloadClientTests: XCTestCase {

    override func tearDown() {
        super.tearDown()
        NetworkRequestInterceptor.removeStub()
    }

    func test_download_downloadsFromURL() {
        let url = anyHTTPURL()
        let exp = expectation(description: #function)

        NetworkRequestInterceptor.observeRequests { request in
            XCTAssertEqual(request.url,
                           url,
                           "Expected to request from \(url), requested from \(String(describing: request.url))")
            XCTAssertEqual(request.httpMethod,
                           "GET",
                           "Expected httpMethod to be GET, instead it is \(String(describing: request.httpMethod))")
            exp.fulfill()
        }
        let sut = makeSUT()

        sut.get(from: anyHTTPURL(), completion: { _ in })
        wait(for: [exp], timeout: 1.0)
    }

    func test_downloadFromURL_failsOnRequestError() {
        expectGet(toCompleteWith: .anyFailure,
                       stubbedWith: (nil, nil, anyNSError()))
    }

    func test_downloadFromURL_failsOnAllInvalidRepresentationCases() {
        func expectGetToFail(stubbedWith stub: (Data?, URLResponse?, Error?),
                             file: StaticString = #file,
                             line: UInt = #line) {
            expectGet(toCompleteWith: .anyFailure,
                      stubbedWith: stub,
                      file: file,
                      line: line)
        }
        expectGetToFail(stubbedWith: (nil, nil, nil))
        expectGetToFail(stubbedWith: (nil, nonHTTPURLResponse(), nil))
        expectGetToFail(stubbedWith: (anyData(), nil, nil))
        expectGetToFail(stubbedWith: (anyData(), nil, anyNSError()))
        expectGetToFail(stubbedWith: (nil, nonHTTPURLResponse(), anyNSError()))
        expectGetToFail(stubbedWith: (nil, anySuccessHTTPURLResponse(), anyNSError()))
        expectGetToFail(stubbedWith: (anyData(), nonHTTPURLResponse(), anyNSError()))
        expectGetToFail(stubbedWith: (anyData(), anySuccessHTTPURLResponse(), anyNSError()))
        expectGetToFail(stubbedWith: (anyData(), nonHTTPURLResponse(), nil))
    }

    func test_downloadFromURL_succeedsOnHTTPURLResponseWithData() {
        let data = anyData()
        let response = anySuccessHTTPURLResponse()

        expectGet(toCompleteWith: .success(.init(data: data,
                                                 httpResponse: response)),
                  stubbedWith: (data, response, nil))
    }

    // MARK: - Private methods

    private func makeSUT(file: StaticString = #file, line: UInt = #line) -> HTTPClient {
        let session = URLSession(configuration: NetworkRequestInterceptor.sessionConfigForIterception())
        let sut = URLSessionHTTPClient(session: session)

        trackForMemoryLeaks(sut, file: file, line: line)

        return sut
    }

    private func expectGet(toCompleteWith expectedResult: HTTPClient.GetResult,
                           stubbedWith stub: (Data?, URLResponse?, Error?),
                           file: StaticString = #file,
                           line: UInt = #line) {
        NetworkRequestInterceptor.stubResponse(data: stub.0, response: stub.1, error: stub.2)

        func compareGetResult(lhs: FilterListGetResult, rhs: FilterListGetResult) -> Bool {
            return lhs.data == rhs.data &&
                   lhs.httpResponse.url == rhs.httpResponse.url &&
                   lhs.httpResponse.statusCode == rhs.httpResponse.statusCode
        }

        expectOperation(curry(makeSUT().get)(anyHTTPURL()),
                        toCompleteWith: expectedResult,
                        isEqual: resultIsEqual(compareGetResult),
                        file: file,
                        line: line)
    }

    private func nonHTTPURLResponse() -> URLResponse {
        return URLResponse(url: anyHTTPURL(), mimeType: nil, expectedContentLength: 0, textEncodingName: nil)
    }
}
