// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class RemoteFilterListLoaderTests: XCTestCase {

    let filterListDownloadMax = 4

    func test_load_customSourceMalformedPath_throwsBadURLError() {
        let (sut, _, _) = makeSUT()

        let invalidCustomSourcePath = " "

        expectOperation(curry(sut.loadFilterList)(.custom(invalidCustomSourcePath)),
                        toCompleteWith: .anyFailureWithCode(.filterListBadDownloadURL),
                        isEqual: resultIsEqual(==, errorCodeIsEqual))
    }

    func test_build_abpSource_createsCorrectURLBase() throws {
        let path = "https://easylist-downloads.adblockplus.org/fanboy-annoyance.txt"
        try expectToRequestLoadWithCorrectURL(forSource: .custom(path),
                                              expectedPath: path)

        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withAA(.easyList)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist_min+exceptionrules_content_blocker.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withAA(.easyListChina)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistchina-minified+exceptionrules-minimal.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withAA(.easyListDutch)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistdutch-minified+exceptionrules-minimal.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withAA(.easyListFrance)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+liste_fr-minified+exceptionrules-minimal.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withAA(.easyListGermany)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistgermany-minified+exceptionrules-minimal.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withAA(.easyListItaly)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistitaly-minified+exceptionrules-minimal.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withAA(.easyListRussiaUkraine)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+ruadlist-minified+exceptionrules-minimal.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withAA(.easyListSpain)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistspanish-minified+exceptionrules-minimal.json")

        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withoutAA(.easyList)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist_min_content_blocker.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withoutAA(.easyListChina)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistchina-minified.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withoutAA(.easyListDutch)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistdutch-minified.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withoutAA(.easyListFrance)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+liste_fr-minified.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withoutAA(.easyListGermany)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistgermany-minified.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withoutAA(.easyListItaly)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistitaly-minified.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withoutAA(.easyListRussiaUkraine)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+ruadlist-minified.json")
        try expectToRequestLoadWithCorrectURL(forSource: .abp(.withoutAA(.easyListSpain)), expectedPath: "https://easylist-downloads.adblockplus.org/easylist+easylistspanish-minified.json")
    }

    func test_build_addsURLWithClientMetaData() throws {
        let clientMeta = ClientMetaData(addon: .init(name: "safari", version: "1.0.0"),
                                        app: .init(name: "partnerApp", version: "2.1.2"),
                                        platform: .init(name: "webkit", version: "10.0.2"))

        let (sut, filterListStore, metaInfoLoader) = makeSUT(clientMetData: clientMeta)

        try expectToAddQueryItems([.init(name: "addonName", value: clientMeta.addon.name),
                                   .init(name: "application", value: clientMeta.app.name),
                                   .init(name: "application", value: clientMeta.app.name),
                                   .init(name: "applicationVersion", value: clientMeta.app.version),
                                   .init(name: "platform", value: clientMeta.platform.name),
                                   .init(name: "platformVersion", value: clientMeta.platform.version)],
                                  sut: sut,
                                  remoteStore: filterListStore,
                                  when: {
                                    metaInfoLoader.completeSingleSourceLoading(with: .anyFailure)
                                  })
    }

    func test_build_failedToLoadFilterListMeta_addsDefaultDownloadQueryValues() throws {
        let (sut, filterListStore, metaInfoLoader) = makeSUT()

        try expectToAddQueryItems([.init(name: "lastVersion", value: "0"),
                                   .init(name: "downloadCount", value: "0")],
                                  sut: sut,
                                  remoteStore: filterListStore,
                                  when: {
                                    metaInfoLoader.completeSingleSourceLoading(with: .anyFailure)
                                  })
    }

    func test_build_addsCorrectFilterListMetaInfoQueryItems() throws {
        let (sut, filterListStore, metaInfoLoader) = makeSUT()

        let filterListMetaInfo = DownloadedFilterListMetaInfo(source: anyFilterListRemoteSource(),
                                                              lastDownloadDate: Date(),
                                                              downloadCount: 3)

        let queryItems: [URLQueryItem] = [.init(name: "lastVersion",
                                                value: "\(filterListMetaInfo.lastDownloadDate.asTimestamp())"),
                                          .init(name: "downloadCount",
                                                value: "\(filterListMetaInfo.downloadCount)")]
        try expectToAddQueryItems(queryItems,
                                  sut: sut,
                                  remoteStore: filterListStore,
                                  when: {
                                    metaInfoLoader.completeSingleSourceLoading(with: .success(filterListMetaInfo))
                                  })
    }

    func test_build_filterListDownloadCountOverMax_addsCorrectQueryItem() throws {
        let (sut, filterListStore, metaInfoLoader) = makeSUT()

        let filterListMetaInfo = DownloadedFilterListMetaInfo(source: anyFilterListRemoteSource(),
                                                              lastDownloadDate: Date(),
                                                              downloadCount: filterListDownloadMax + 1)
        try expectToAddQueryItems([.init(name: "downloadCount", value: "\(filterListDownloadMax)+")],
                                  sut: sut,
                                  remoteStore: filterListStore,
                                  when: {
                                    metaInfoLoader.completeSingleSourceLoading(with: .success(filterListMetaInfo))
                                  })
    }

    func test_load_rulesStoreFailed_throwsError() {
        let (sut, filterListStore, metaInfoLoader) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==),
                        when: {
                            metaInfoLoader.completeSingleSourceLoading(with: .success(anyDownloadedFilterListMetaInfo()))
                            filterListStore.completeOperation(with: .anyFailure)
                        })
    }

    func test_load_rulesStoreSuccess_completesWithRules() {
        let (sut, filterListStore, metaInfoLoader) = makeSUT()

        let filterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            metaInfoLoader.completeSingleSourceLoading(with: .success(anyDownloadedFilterListMetaInfo()))
                            filterListStore.completeOperation(with: .success(filterList))
                        })
    }

    // MARK: - Helpers

    private func makeSUT(clientMetData: ClientMetaData = anyClientMetaData(),
                         file: StaticString = #file,
                         line: UInt = #line) -> (RemoteFilterListLoader,
                                                 RemoteFilterListStoreMock,
                                                 DownloadedFilterListMetaInfoLoaderMock) {
        let rulesStore = RemoteFilterListStoreMock()
        let rulesListMetaInfoProvider = DownloadedFilterListMetaInfoLoaderMock()

        let sut = RemoteFilterListLoader(remoteFilterListStore: rulesStore,
                                         clientMetaData: clientMetData,
                                         filterListMetaInfoLoader: rulesListMetaInfoProvider)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, rulesStore, rulesListMetaInfoProvider)
    }

    private func expectToRequestLoadWithCorrectURL(forSource source: FilterListType,
                                                   expectedPath: String,
                                                   file: StaticString = #file,
                                                   line: UInt = #line) throws {
        let (sut, rulesStore, rulesMetaLoader) = makeSUT()

        sut.loadFilterList(forSource: source) { _ in }
        if case .abp = source {
            rulesMetaLoader.completeSingleSourceLoading(with: .success(anyDownloadedFilterListMetaInfo()))
        }

        let retrievedURL = try XCTUnwrap(rulesStore.requestedParams.first, file: file, line: line)
        let urlComponents = try XCTUnwrap(URLComponents(url: retrievedURL, resolvingAgainstBaseURL: true),
                                          file: file,
                                          line: line)
        let scheme = try XCTUnwrap(urlComponents.scheme, file: file, line: line)
        let host = try XCTUnwrap(urlComponents.host, file: file, line: line)

        let urlBase = "\(scheme)://\(host)\(urlComponents.path)"
        XCTAssertEqual(urlBase, expectedPath, file: file, line: line)
    }

    private func expectToAddQueryItems(_ items: [URLQueryItem],
                                       sut: RemoteFilterListLoader,
                                       remoteStore: RemoteFilterListStoreMock,
                                       when action: () -> Void,
                                       file: StaticString = #file,
                                       line: UInt = #line) throws {

        sut.loadFilterList(forSource: anyFilterListRemoteSource()) { _ in }
        action()

        let recievedURL = try XCTUnwrap(remoteStore.requestedParams.first, "Expected to build url successfully", file: file, line: line)
        let components = URLComponents(url: recievedURL, resolvingAgainstBaseURL: true)

        for item in items {
            let queryComponent = try XCTUnwrap(components?.queryItems?.first { $0.name == item.name },
                                               "Missing query item for name \(item.name)",
                                               file: file,
                                               line: line)
            XCTAssertEqual(queryComponent, item, file: file, line: line)
        }
    }
}

extension Date {
    func asTimestamp() -> String {
        let fmt = DateFormatter()
        fmt.locale = Locale(identifier: "en_US_POSIX")
        fmt.dateFormat = "yyyyMMddHHmm"
        fmt.timeZone = TimeZone(secondsFromGMT: 0)
        return fmt.string(from: self)
    }
}
