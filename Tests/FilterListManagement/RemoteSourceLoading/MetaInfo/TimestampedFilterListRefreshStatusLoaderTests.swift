// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class TimestampedFilterListRefreshStatusLoaderTests: XCTestCase {

    func test_loadRefreshStatus_loadsMetaInfoForCorrectSource() {
        let (sut, metaInfoLoader) = makeSUT()

        let source = anyFilterListRemoteSource()
        sut.loadRefreshStatus(forSource: source, completion: { _ in })

        XCTAssertEqual(metaInfoLoader.singleSourceRequests, [source])
    }

    func test_loadRefreshStatus_failedToLoadMetaInfo_completesWithTrue() {
        let (sut, metaInfoLoader) = makeSUT()

        expectOperation(curry(sut.loadRefreshStatus)(anyFilterListRemoteSource()),
                        toCompleteWith: .needsRefresh,
                        isEqual: ==,
                        when: {
                            metaInfoLoader.completeSingleSourceLoading(with: .anyFailure)
        })
    }

    func test_loadRefreshStatus_nonExpiredFilterList_noRefreshNeeded() {
        let currentDate = Date()
        let (sut, metaInfoLoader) = makeSUT(currentDate: { currentDate })
        let nonExpiredTimestamp = currentDate.minusFilterListExpirationTimetamp().adding(seconds: 1)
        let nonExpiredMetaInfo = makeMetaInfo(date: nonExpiredTimestamp)

        expectOperation(curry(sut.loadRefreshStatus)(anyFilterListRemoteSource()),
                        toCompleteWith: .noRefreshNeeded,
                        isEqual: ==,
                        when: {
                            metaInfoLoader.completeSingleSourceLoading(with: .success(nonExpiredMetaInfo))
        })
    }

    func test_loadRefreshStatus_expiredFilterList_needsRefresh() {
        let currentDate = Date()
        let (sut, metaInfoLoader) = makeSUT(currentDate: { currentDate })
        let expiredTimestamp = currentDate.minusFilterListExpirationTimetamp()
        let expiredMetaInfo = makeMetaInfo(date: expiredTimestamp)

        expectOperation(curry(sut.loadRefreshStatus)(anyFilterListRemoteSource()),
                        toCompleteWith: .needsRefresh,
                        isEqual: ==,
                        when: {
                            metaInfoLoader.completeSingleSourceLoading(with: .success(expiredMetaInfo))
        })
    }

    func test_loadAllExpiredSources_requestsToLoadAllSources() {
        let (sut, metaInfoLoader) = makeSUT()

        sut.loadExpiredSources { _ in }

        XCTAssertEqual(metaInfoLoader.allSourcesLoadCount, 1)
    }

    func test_loadAllExpiredSources_failedToLoadMeta_throwsError() {
        let (sut, metaInfoLoader) = makeSUT()

        expectOperation(sut.loadExpiredSources,
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                           metaInfoLoader.completeLoadAllMetaLoading(with: .anyFailure)
        })
    }

    func test_loadAllExpiredSources_returnsExpiredSourcesOnly() {
        let currentDate = Date()
        let (sut, metaInfoLoader) = makeSUT(currentDate: { currentDate })
        let expiredTimestamp = currentDate.minusFilterListExpirationTimetamp()

        let expiredMetaInfo = makeMetaInfo(date: expiredTimestamp,
                                           source: .abp(.withAA(.easyList)))

        let nonExpiredMetaInfo = makeMetaInfo(date: expiredTimestamp.adding(seconds: 1),
                                              source: .abp(.withAA(.easyListDutch)))

        expectOperation(sut.loadExpiredSources,
                        toCompleteWith: .success([expiredMetaInfo.source]),
                        isEqual: resultIsEqual(==),
                        when: {
                           metaInfoLoader.completeLoadAllMetaLoading(with: .success([expiredMetaInfo, nonExpiredMetaInfo]))
        })
    }

    // MARK: - Helpers

    private func makeSUT(currentDate: @escaping () -> Date = Date.init,
                         file: StaticString = #file,
                         line: UInt = #line) -> (TimestampedFilterListRefreshStatusLoader, DownloadedFilterListMetaInfoLoaderMock) {
        let metaInfoLoader = DownloadedFilterListMetaInfoLoaderMock()
        let sut = TimestampedFilterListRefreshStatusLoader(metaInfoLoader: metaInfoLoader, currentDate: currentDate)
        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, metaInfoLoader)
    }

    private func makeMetaInfo(date: Date,
                              source: FilterListType = anyFilterListRemoteSource()) -> DownloadedFilterListMetaInfo {
        DownloadedFilterListMetaInfo(source: source,
                                     lastDownloadDate: date,
                                     downloadCount: 1)
    }
}
