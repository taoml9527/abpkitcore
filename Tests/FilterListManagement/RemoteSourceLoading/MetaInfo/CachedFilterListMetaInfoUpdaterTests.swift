// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class CachedFilterListMetaInfoUpdaterTests: XCTestCase {

    func test_update_failedToLoadPreviousMeta_savesInitialMeta() {
        let date = Date()

        let (sut, metaInfoStore) = makeSUT(dateBuilder: { date })

        let source = anyFilterListRemoteSource()
        sut.updateMetaInfo(forSource: source, completion: { _ in })
        metaInfoStore.completeRetrieve(withResult: .anyFailure)

        let metaToBeSaved = makeMetaInfo(source: source, lastDownloadDate: date)
        XCTAssertEqual(metaInfoStore.insertRequest, [[metaToBeSaved]])
    }

    func test_update_noPreviousMetaForSource_savesInitialMeta() {
        let date = Date()

        let (sut, metaInfoStore) = makeSUT(dateBuilder: { date })

        let source = anyFilterListRemoteSource()
        sut.updateMetaInfo(forSource: source, completion: { _ in })

        let storedMeta = makeMetaInfo(source: .abp(.withoutAA(.easyListChina)))
        metaInfoStore.completeRetrieve(withResult: .success([storedMeta]))

        let metaToBeSaved = makeMetaInfo(source: source, lastDownloadDate: date)
        XCTAssertEqual(metaInfoStore.insertRequest, [[storedMeta, metaToBeSaved]])
    }

    func test_update_updatesTheCorrectMeta() {
        let freshDate = Date()

        let (sut, metaInfoStore) = makeSUT(dateBuilder: { freshDate })

        let targetSource = anyFilterListRemoteSource()
        let storedMeta1 = makeMetaInfo(source: targetSource)
        let storedMeta2 = makeMetaInfo(source: .abp(.withoutAA(.easyListChina)))

        sut.updateMetaInfo(forSource: targetSource, completion: { _ in })
        metaInfoStore.completeRetrieve(withResult: .success([storedMeta1, storedMeta2]))

        let newMeta = makeMetaInfo(source: targetSource,
                                   lastDownloadDate: freshDate,
                                   downloadCount: storedMeta1.downloadCount + 1)
        XCTAssertEqual(metaInfoStore.insertRequest, [[storedMeta2, newMeta]])
    }

    func test_update_failedToStoreNewMeta_throwsError() {
        let (sut, metaInfoStore) = makeSUT()

        expectOperation(curry(sut.updateMetaInfo)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            metaInfoStore.completeRetrieve(withResult: .success([]))
                            metaInfoStore.completeInsert(withResult: .anyFailure)
        })
    }

    func test_update_successStoringNewMeta_completesWithSuccess() {
        let (sut, metaInfoStore) = makeSUT()

        expectOperation(curry(sut.updateMetaInfo)(anyFilterListRemoteSource()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            metaInfoStore.completeRetrieve(withResult: .success([]))
                            metaInfoStore.completeInsert(withResult: .success(()))
        })
    }

    // MARK: - Helpers

    private func makeSUT(dateBuilder: @escaping CachedFilterListMetaInfoUpdater.DateBuilder = Date.init,
                         file: StaticString = #file,
                         line: UInt = #line) -> (CachedFilterListMetaInfoUpdater, FilterListMetaInfoStoreMock) {
        let store = FilterListMetaInfoStoreMock()
        let loader = CachedFilterListMetaInfoUpdater(metaInfoStore: store,
                                              dateBuilder: dateBuilder)

        trackForMemoryLeaks(loader, file: file, line: line)

        return (loader, store)
    }

    private func makeMetaInfo(source: FilterListType = .abp(.withAA(.easyList)),
                              lastDownloadDate: Date = Date(),
                              downloadCount: Int = 1) -> DownloadedFilterListMetaInfo {
        DownloadedFilterListMetaInfo(source: source,
                                     lastDownloadDate: lastDownloadDate,
                                     downloadCount: downloadCount)
    }
}
