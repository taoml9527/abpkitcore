// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class CachedFilterListMetaInfoLoaderTests: XCTestCase {

    func test_load_failedToLoadMetaFromStore_throwsError() {
        let (sut, metaInfoStore) = makeSUT()

        expectOperation(curry(sut.loadMetaInfo)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            metaInfoStore.completeRetrieve(withResult: .anyFailure)
        })
    }

    func test_load_noMetaForSource_throwsError() {
        let (sut, metaInfoStore) = makeSUT()

        let storedMeta = makeMetaInfo(source: anyFilterListRemoteSource())

        expectOperation(curry(sut.loadMetaInfo)(.abp(.withAA(.easyListChina))),
                        toCompleteWith: .failure(CachedFilterListMetaInfoLoader.MissingMetaInfoError()),
                        isEqual: resultIsEqual(==,
                                               castedErrorIsEqual(cast: CachedFilterListMetaInfoLoader.MissingMetaInfoError.self)),
                        when: {
                            metaInfoStore.completeRetrieve(withResult: .success([storedMeta]))
        })
    }

    func test_load_metaForSourceExists_returnsMetaInfo() {
        let (sut, metaInfoStore) = makeSUT()

        let storedMeta = makeMetaInfo()

        expectOperation(curry(sut.loadMetaInfo)(storedMeta.source),
                        toCompleteWith: .success(storedMeta),
                        isEqual: resultIsEqual(==),
                        when: {
                            metaInfoStore.completeRetrieve(withResult: .success([storedMeta]))
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (CachedFilterListMetaInfoLoader, FilterListMetaInfoStoreMock) {
        let store = FilterListMetaInfoStoreMock()
        let loader = CachedFilterListMetaInfoLoader(metaInfoStore: store)

        trackForMemoryLeaks(loader, file: file, line: line)

        return (loader, store)
    }

    private func makeMetaInfo(source: FilterListType = .abp(.withAA(.easyList)),
                              lastDownloadDate: Date = Date(),
                              downloadCount: Int = 1) -> DownloadedFilterListMetaInfo {
        DownloadedFilterListMetaInfo(source: source,
                                     lastDownloadDate: lastDownloadDate,
                                     downloadCount: downloadCount)
    }
}
