// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class CachedFilterListLoaderTests: XCTestCase {
    func test_load_requestsLoadFromStoreWithCorrectName() {
        // AA sources
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withAA(.easyList)),
                                               expectedName: "easyList+exceptionRules.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withAA(.easyListChina)),
                                               expectedName: "easyListChina+exceptionRules.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withAA(.easyListDutch)),
                                               expectedName: "easyListDutch+exceptionRules.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withAA(.easyListItaly)),
                                               expectedName: "easyListItaly+exceptionRules.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withAA(.easyListSpain)),
                                               expectedName: "easyListSpain+exceptionRules.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withAA(.easyListFrance)),
                                               expectedName: "easyListFrance+exceptionRules.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withAA(.easyListGermany)),
                                               expectedName: "easyListGermany+exceptionRules.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withAA(.easyListRussiaUkraine)),
                                               expectedName: "easyListRussiaUkraine+exceptionRules.json")

        // Non AA sources
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withoutAA(.easyList)),
                                               expectedName: "easyList.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withoutAA(.easyListChina)),
                                               expectedName: "easyListChina.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withoutAA(.easyListDutch)),
                                               expectedName: "easyListDutch.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withoutAA(.easyListItaly)),
                                               expectedName: "easyListItaly.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withoutAA(.easyListSpain)),
                                               expectedName: "easyListSpain.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withoutAA(.easyListFrance)),
                                               expectedName: "easyListFrance.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withoutAA(.easyListGermany)),
                                               expectedName: "easyListGermany.json")
        expectToRequestRetrieveWithCorrectName(forSource: .abp(.withoutAA(.easyListRussiaUkraine)),
                                               expectedName: "easyListRussiaUkraine.json")

        // Custom source
        let path = "https://easylist-downloads.adblockplus.org/fanboy-annoyance.json"
        expectToRequestRetrieveWithCorrectName(forSource: .custom(path),
                                               expectedName: "fanboy-annoyance.json")
    }

    func test_load_retrieveFailed_throwsFailedToLoadFilterList() {
        let (sut, filterListStore) = makeSUT()

        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            filterListStore.completeRetrieval(with: .anyFailure)
        })
    }

    func test_load_retriveSucceeded_returnsFilteList() {
        let (sut, filterListStore) = makeSUT()

        let filterList = anyFilterList()
        expectOperation(curry(sut.loadFilterList)(anyFilterListRemoteSource()),
                        toCompleteWith: .success(filterList),
                        isEqual: resultIsEqual(==),
                        when: {
                            filterListStore.completeRetrieval(with: .success(filterList.content))
        })
    }

    func test_cache_requestsCacheWithCorrectRulesAndName() {
        // AA sources
        expectToRequestCacheWithCorrectName(forSource: .abp(.withAA(.easyList)),
                                            expectedName: "easyList+exceptionRules.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withAA(.easyListChina)),
                                            expectedName: "easyListChina+exceptionRules.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withAA(.easyListDutch)),
                                            expectedName: "easyListDutch+exceptionRules.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withAA(.easyListItaly)),
                                            expectedName: "easyListItaly+exceptionRules.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withAA(.easyListSpain)),
                                            expectedName: "easyListSpain+exceptionRules.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withAA(.easyListFrance)),
                                            expectedName: "easyListFrance+exceptionRules.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withAA(.easyListGermany)),
                                            expectedName: "easyListGermany+exceptionRules.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withAA(.easyListRussiaUkraine)),
                                            expectedName: "easyListRussiaUkraine+exceptionRules.json")

        // Non AA sources
        expectToRequestCacheWithCorrectName(forSource: .abp(.withoutAA(.easyList)),
                                            expectedName: "easyList.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withoutAA(.easyListChina)),
                                            expectedName: "easyListChina.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withoutAA(.easyListDutch)),
                                            expectedName: "easyListDutch.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withoutAA(.easyListItaly)),
                                            expectedName: "easyListItaly.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withoutAA(.easyListSpain)),
                                            expectedName: "easyListSpain.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withoutAA(.easyListFrance)),
                                            expectedName: "easyListFrance.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withoutAA(.easyListGermany)),
                                            expectedName: "easyListGermany.json")
        expectToRequestCacheWithCorrectName(forSource: .abp(.withoutAA(.easyListRussiaUkraine)),
                                            expectedName: "easyListRussiaUkraine.json")

        // Custom source
        let path = "https://easylist-downloads.adblockplus.org/fanboy-annoyance.json"
        expectToRequestCacheWithCorrectName(forSource: .custom(path),
                                            expectedName: "fanboy-annoyance.json")
    }

    func test_cache_requestsInsertForFilterList() {
        let (sut, filterListStore) = makeSUT()

        let filterList = anyFilterList()
        sut.cacheFilterList(filterList, forSource: anyFilterListRemoteSource(), completion: { _ in })

        let insertedFilterLists = filterListStore.requestedInserts.map { $0.filterList }
        XCTAssertEqual(insertedFilterLists,
                       [filterList.content],
                       "Expected to request insert for \(filterList), requested instead for \(insertedFilterLists)")
    }

    func test_cache_insertFailed_completesWithFailure() {
        let (sut, filterListStore) = makeSUT()

        expectOperation(curry(sut.cacheFilterList)(anyFilterList(), anyFilterListRemoteSource()),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            filterListStore.completeInsert(with: .anyFailure)
        })
    }

    func test_cache_insertSucceeded_completesWithSuccess() {
        let (sut, filterListStore) = makeSUT()

        expectOperation(curry(sut.cacheFilterList)(anyFilterList(), anyFilterListRemoteSource()),
                        toCompleteWith: .success,
                        isEqual: resultIsEqual(==),
                        when: {
                            filterListStore.completeInsert(with: .success(()))
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (CachedFilterListLoader, FilterListCacheStoreMock)  {
        let rulesListStore = FilterListCacheStoreMock()
        let sut = CachedFilterListLoader(filterListStore: rulesListStore)

        trackForMemoryLeaks(sut, file: file, line: line)

        return (sut, rulesListStore)
    }

    private func expectToRequestRetrieveWithCorrectName(forSource source: FilterListType,
                                                        expectedName: String,
                                                        file: StaticString = #file,
                                                        line: UInt = #line) {
        let (sut, rulesStore) = makeSUT()

        sut.loadFilterList(forSource: source) { _ in }
        XCTAssertEqual(rulesStore.requestedRetrieves,
                       [expectedName],
                       "Expected to request retrive for filter list with \(name), requested instead for \(rulesStore.requestedRetrieves)",
                       file: file,
                       line: line)
    }

    private func expectToRequestCacheWithCorrectName(forSource source: FilterListType,
                                                     expectedName: String,
                                                     file: StaticString = #file,
                                                     line: UInt = #line) {
        let (sut, rulesStore) = makeSUT()

        sut.cacheFilterList(anyFilterList(), forSource: source, completion: { _ in })
        XCTAssertEqual(rulesStore.requestedInserts.map { $0.filterListName },
                       [expectedName],
                       "Expected to request cache for filter list with \(name), requested instead for \(rulesStore.requestedRetrieves)",
                       file: file,
                       line: line)

    }
}
