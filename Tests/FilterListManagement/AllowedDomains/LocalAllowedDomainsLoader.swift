// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

@testable import ABPKitCore
import XCTest

class LocalAllowedDomainsLoaderTests: XCTestCase {
    func test_loadDomains_retrievesFromStore() {
        let (sut, store) = makeSUT()

        sut.loadDomains { _ in }
        XCTAssertEqual(store.commands, [.loadDomains])
    }

    func test_loadDomains_failedToRetrieveFromStore_throwsError() {
        let (sut, store) = makeSUT()

        expectOperation(sut.loadDomains,
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            store.completeRetrieve(with: .anyFailure)
        })
    }

    func test_loadDomains_loadedFromStore_returnsDomains() {
        let (sut, store) = makeSUT()

        let domains = [AllowedDomain("some.com"),
                       AllowedDomain("other.com")]

        expectOperation(sut.loadDomains,
                        toCompleteWith: .success(domains),
                        isEqual: resultIsEqual(==),
                        when: {
                            store.completeRetrieve(with: .success(domains))
        })
    }

    func test_addDomains_failedToLoadCurrentList_createsNewList() {
        let (sut, store) = makeSUT()

        let domains = [AllowedDomain("some.com"),
                       AllowedDomain("other.com")]
        sut.addDomains(domains, completion: { _ in })
        store.completeRetrieve(with: .anyFailure)

        XCTAssertEqual(store.commands, [.loadDomains, .insertDomains(domains)])
    }

    func test_addDomain_appendsToTheCurrentList() {
        let (sut, store) = makeSUT()

        let newDomains = [AllowedDomain("some.com"),
                          AllowedDomain("other.com")]
        let oldAllowedDomains = [AllowedDomain("old.com")]
        sut.addDomains(newDomains, completion: { _ in })
        store.completeRetrieve(with: .success(oldAllowedDomains))

        let expectedSavedDomains = oldAllowedDomains + newDomains
        XCTAssertEqual(store.commands,
                       [.loadDomains, .insertDomains(expectedSavedDomains)])
    }

    func test_addDomain_failedToInsert_throwsError() {
        let (sut, store) = makeSUT()

        expectOperation(curry(sut.addDomains)([anyAllowedDomain()]),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            store.completeRetrieve(with: .anyFailure)
                            store.completeInsert(with: .anyFailure)
        })
    }

    func test_addDomain_insertsSuccesfully_returnsNewList() {
        let (sut, store) = makeSUT()
        let newAllowedDomain = AllowedDomain("some.com")
        let oldAllowedDomain = AllowedDomain("old.com")

        expectOperation(curry(sut.addDomains)([newAllowedDomain]),
                        toCompleteWith: .success([oldAllowedDomain, newAllowedDomain]),
                        isEqual: resultIsEqual(==),
                        when: {
                            store.completeRetrieve(with: .success([oldAllowedDomain]))
                            store.completeInsert(with: .success)
        })
    }

    func test_addDomains_doesNotAddDuplicatedDomains() {
        let (sut, store) = makeSUT()

        let newDomains = [AllowedDomain("old.com"),
                          AllowedDomain("some.com")]
        let oldAllowedDomains = [AllowedDomain("old.com")]
        sut.addDomains(newDomains, completion: { _ in })
        store.completeRetrieve(with: .success(oldAllowedDomains))

        XCTAssertEqual(store.commands,
                       [.loadDomains, .insertDomains(newDomains)])

    }

    func test_removeDomain_failedToLoadCurrentList_throwsError() {
        let (sut, store) = makeSUT()

        expectOperation(curry(sut.removeDomains)([anyAllowedDomain()]),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            store.completeRetrieve(with: .anyFailure)
        })
    }

    func test_removeDomain_insertsListWithoutTheRemovedDomain() {
        let (sut, store) = makeSUT()

        let allowedDomain = AllowedDomain("some.com")
        let otherAllowedDomain = AllowedDomain("other.com")
        sut.removeDomains([allowedDomain], completion: { _ in })
        store.completeRetrieve(with: .success([allowedDomain, otherAllowedDomain]))

        XCTAssertEqual(store.commands,
                       [.loadDomains, .insertDomains([otherAllowedDomain])])
    }

    func test_removeDomain_failedToInsert_throwsError() {
        let (sut, store) = makeSUT()
        let allowedDomain = anyAllowedDomain()

        expectOperation(curry(sut.removeDomains)([allowedDomain]),
                        toCompleteWith: .anyFailure,
                        isEqual: resultIsEqual(==, nsErrorIsEqual),
                        when: {
                            store.completeRetrieve(with: .success([allowedDomain]))
                            store.completeInsert(with: .anyFailure)
        })
    }

    func test_removeDomain_insertsSuccesfully_returnsNewList() {
        let (sut, store) = makeSUT()
        let allowedDomain = anyAllowedDomain()

        expectOperation(curry(sut.removeDomains)([allowedDomain]),
                        toCompleteWith: .success([]),
                        isEqual: resultIsEqual(==),
                        when: {
                            store.completeRetrieve(with: .success([allowedDomain]))
                            store.completeInsert(with: .success)
        })
    }

    // MARK: - Helpers

    private func makeSUT(file: StaticString = #file,
                         line: UInt = #line) -> (LocalAllowedDomainsLoader, AllowedDomainsStoreMock) {
        let store = AllowedDomainsStoreMock()
        let sut = LocalAllowedDomainsLoader(allowedDomainsStore: store)
        trackForMemoryLeaks(sut, file: file, line: line)
        return (sut, store)
    }

    private class AllowedDomainsStoreMock: AllowedDomainsStore {

        enum Command: Equatable {
            case loadDomains
            case insertDomains([String])
        }

        private(set) var commands = [Command]()
        private var retrieveCompletions = [RetrieveCompletion]()
        private var insertCompletions = [InsertCompletion]()

        func retrieveDomains(completion: @escaping RetrieveCompletion) {
            commands.append(.loadDomains)
            retrieveCompletions.append(completion)
        }

        func insertDomains(_ domains: [String], completion: @escaping InsertCompletion) {
            commands.append(.insertDomains(domains))
            insertCompletions.append(completion)
        }

        func completeRetrieve(with result: RetrieveResult) {
            retrieveCompletions[0](result)
        }

        func completeInsert(with result: InsertResult) {
            insertCompletions[0](result)
        }
    }
}
