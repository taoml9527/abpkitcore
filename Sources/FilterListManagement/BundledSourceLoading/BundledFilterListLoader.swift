// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Loads a filter list from the bundle
final class BundledFilterListLoader: FilterListTypeLoader {

    private let filterListBundle: Bundle

    /// Creates a new instance
    /// - Parameter filterListBundle: The bundle from where to load the filter list
    init(filterListBundle: Bundle) {
        self.filterListBundle = filterListBundle
    }

    func loadFilterList(forSource source: FilterListType,
                        completion: @escaping LoadCompletion) {
        completion(Result {
            guard let bundledFilterListPath = filterListBundle.path(forResource: source.filterListPath,
                                                                    ofType: "") else {
                throw ABPKitCoreError.missingFilterListInBundle
            }

            let data = try Data(contentsOf: URL(fileURLWithPath: bundledFilterListPath))
            if let rawRules = String(data: data, encoding: .ascii) {
                return FilterList(content: rawRules)
            } else {
                throw ABPKitCoreError.malformedFilterListContent
            }
        })
    }
}

private extension FilterListType {
    var filterListPath: String {
        switch self {
        case let .abp(source):
            return source.filterListPath
        case let .custom(path):
            return path
        }
    }
}

private extension FilterListAAVariant {
    var filterListPath: String {
        switch self {
        case let .withAA(source):
            switch source {
            case .easyList:
                return "easylist+exceptionrules_content_blocker.json"
            case .easyListChina:
                return "easylistchina+exceptionrules_content_blocker.json"
            case .easyListDutch:
                return "easylistdutch+exceptionrules_content_blocker.json"
            case .easyListGermany:
                return "easylistgermany+exceptionrules_content_blocker.json"
            case .easyListItaly:
                return "easylistitaly+exceptionrules_content_blocker.json"
            case .easyListSpain:
                return "easylistspain+exceptionrules_content_blocker.json"
            case .easyListFrance:
                return "easylistfrance+exceptionrules_content_blocker.json"
            case .easyListRussiaUkraine:
                return "easylistrussiaukraine+exceptionrules_content_blocker.json"
            }
        case let .withoutAA(source):
            switch source {
            case .easyList:
                return "easylist_content_blocker.json"
            case .easyListChina:
                return "easylistchina_content_blocker.json"
            case .easyListDutch:
                return "easylistdutch_content_blocker.json"
            case .easyListGermany:
                return "easylistgermany_content_blocker.json"
            case .easyListItaly:
                return "easylistitaly_content_blocker.json"
            case .easyListSpain:
                return "easylistspain_content_blocker.json"
            case .easyListFrance:
                return "easylistfrance_content_blocker.json"
            case .easyListRussiaUkraine:
                return "easylistrussiaukraine_content_blocker.json"
            }
        }
    }
}
