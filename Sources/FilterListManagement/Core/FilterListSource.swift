// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Load path for a given filter list
public typealias FilterListPath = String

/// Name of a given filter list that uniquely identifies it
public typealias FilterListName = String

/// A source that describes a particular filter list
public enum FilterListSource: Equatable {
    /// Bundled filter list source
    case bundled(FilterListType)
    /// Remote filter list source
    case remote(FilterListType)
}

/// Filfter list types
public enum FilterListType: Equatable {
    /// ABP filter lists
    case abp(FilterListAAVariant)
    /// Custom source specified by path, be it bundle file name or remote url
    case custom(FilterListPath)
}

/// Available AA variants filter lists
public enum FilterListAAVariant: Equatable {
    /// Source for filter list that contain acceptable ads
    case withAA(FilterListABPSource)
    /// Source for filter list that doesn't contain acceptable ads
    case withoutAA(FilterListABPSource)
}

// ABP specific filter list sources
public enum FilterListABPSource: Equatable, Hashable {
    /// Source for english filter list
    case easyList
    /// Source for China filter list
    case easyListChina
    /// Source for Dutch filter list
    case easyListDutch
    /// Source for Germany filter list
    case easyListGermany
    /// Source for Italy filter list
    case easyListItaly
    /// Source for Spain filter list
    case easyListSpain
    /// Source for France filter list
    case easyListFrance
    /// Source for Russia and Ukraine filter list
    case easyListRussiaUkraine
}
