// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Represents the filter list refresh status
enum FilterListRefreshStatus {
    case needsRefresh
    case noRefreshNeeded
}

/// Provides the ability to load the filter list refresh status for a given filter list source
protocol FilterListRefreshStatusLoader {
    typealias LoadResult = FilterListRefreshStatus
    typealias LoadCompletion = (LoadResult) -> Void

    /// Loads the filter list refresh status for a given filter list source
    /// - Parameters:
    ///   - source: The source for which to load the refresh status
    ///   - completion: The refresh status load completion
    func loadRefreshStatus(forSource source: FilterListType,
                           completion: @escaping LoadCompletion)
}
