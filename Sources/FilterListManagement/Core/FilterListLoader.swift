// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Contains the particular filter list
public struct FilterList: Equatable {

    /// The actual filter list in JSON format
    public let content: String

    /// Creates a new instance
    /// - Parameter content: The actual filter list in JSON format
    public init(content: String) {
        self.content = content
    }
}

/// Handles the loading of a filter list based on the filter list source
public protocol FilterListLoader {
    /// The result of loading a filter list, containing either the loaded filter list or an error
    typealias LoadResult = Result<FilterList, Error>
    /// A closure to be used to handle the LoadResult
    typealias LoadCompletion = (LoadResult) -> Void

    /// Loads the list for a given source
    /// - Parameters:
    ///   - source: The source to load the filter list for
    ///   - completion: Closure to handle the load filter list result
    func loadFilterList(forSource source: FilterListSource,
                        completion: @escaping LoadCompletion)
}
