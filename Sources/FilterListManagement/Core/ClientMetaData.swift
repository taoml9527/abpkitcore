// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Client meta info to be used to accordingly populate the filter list download requests
public struct ClientMetaData {

    let addon: Addon
    let app: App
    let platform: Platform

    /// Creates a new instance
    /// - Parameters:
    ///   - addon: Contains the info about the addon that uses the framework
    ///   - app: Contains the info about the app that uses the framework
    ///   - platform: Contains the info about the platform on which the framework is used
    public init(addon: Addon, app: App, platform: Platform) {
        self.addon = addon
        self.app = app
        self.platform = platform
    }
}

/// Contains the info about the app that uses the framework
public struct App {

    let name: String
    let version: String

    /// Creates a new instance
    /// - Parameters:
    ///   - name: The name of the app
    ///   - version: The current version of the app
    public init(name: String, version: String) {
        self.name = name
        self.version = version
    }
}

/// Contains the info about the addon that uses the framework
public struct Addon {

    let name: String
    let version: String

    /// Creates a new instance
    /// - Parameters:
    ///   - name: The name of the addon
    ///   - version: The current version of the addon
    public init(name: String, version: String) {
        self.name = name
        self.version = version
    }
}

/// Contains the info about the platform on which the framework is used
public struct Platform {

    let name: String
    let version: String

    /// Creates a new instance
    /// - Parameters:
    ///   - name: The name of the platform
    ///   - version: The current version of the platform
    public init(name: String, version: String) {
        self.name = name
        self.version = version
    }
}
