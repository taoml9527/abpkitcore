// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

protocol FilterListWithAllowedDomainsLoaderOutput {
    func didAppendAllowedDomainsToFilterList(for source: FilterListSource,
                                             allowedDomains: AllowedDomains,
                                             allowedDomainRule: FilterRule)
}

/// Component responsible for loading filter list from source and append allowed domains to the loaded list
final class FilterListWithAllowedDomainsLoader: FilterListLoader {
    private let decoratee: FilterListLoader
    private let allowedDomainsLoader: AllowedDomainsLoader

    var output: FilterListWithAllowedDomainsLoaderOutput?

    /// Creates an instance
    /// - Parameters:
    ///   - decoratee: To be used to load the filter list
    ///   - allowedDomainsLoader: To be used to load allowed domains which will be added to the filter list
    init(decoratee: FilterListLoader, allowedDomainsLoader: AllowedDomainsLoader) {
        self.decoratee = decoratee
        self.allowedDomainsLoader = allowedDomainsLoader
    }

    func loadFilterList(forSource source: FilterListSource, completion: @escaping LoadCompletion) {
        func completeWithError(_ error: Error) {
            completion(.failure(error))
        }

        func completeWithFilterList(_ list: FilterList) {
            completion(.success(list))
        }

        func appendDomainsToFilterList() -> (FilterList) -> Void {
            return { [weak self] list in
                func appendFilterListWithDomains(_ domains: AllowedDomains) {
                    if domains.isEmpty {
                        completeWithFilterList(list)
                    } else {
                        let allowedDomainsFilter = makeAllowedDomainsFilter(from: domains)
                        self?.output?.didAppendAllowedDomainsToFilterList(for: source, allowedDomains: domains, allowedDomainRule: allowedDomainsFilter)
                        completeWithFilterList(makeAllowedDomainsAddedFilterList(allowedDomainsFilter, list))
                    }
                }

                func ignoreErrorAndReturnLoadedList(error: Error) {
                    completeWithFilterList(list)
                }

                self?.allowedDomainsLoader.loadDomains(completion: handleResult(appendFilterListWithDomains, ignoreErrorAndReturnLoadedList))
            }
        }

        decoratee.loadFilterList(forSource: source,
                                 completion: handleResult(appendDomainsToFilterList(), completeWithError))
    }
}

private func makeAllowedDomainsAddedFilterList(_ allowedDomainsFilter: FilterRule,
                                               _ filterList: FilterList) -> FilterList {
    let listForAppend: String = {
        guard filterList.content.last == "]" else {
            return filterList.content
        }
        var list = filterList.content
        _ = list.popLast()
        return list
    }()

    let listWithAllowedDomains = listForAppend.appending(",").appending(allowedDomainsFilter.rule).appending("]")
    return FilterList(content: listWithAllowedDomains)
}

struct FilterRule {
    let rule: String
}

private func makeAllowedDomainsFilter(from domains: AllowedDomains) -> FilterRule {
    let wrappedDomains = domains.map(wrappedDomain).joined(separator: ",")
    // For now the rule is expressed in raw form, as here it is the only place were it is used
    let topURL = "\"if-top-url\":[\(wrappedDomains)]"
    let loadedType = "\"load-type\":[\"first-party\",\"third-party\"]"
    let urlFilter = "\"url-filter\":\".*\""
    let isCaseSensitive = "\"url-filter-is-case-sensitive\":false"
    let ruleFormat = "{\"trigger\":{%@},\"action\":{%@}}"
    let actionType = "\"type\":\"ignore-previous-rules\""

    let trigger = [topURL, loadedType, urlFilter, isCaseSensitive].joined(separator: ",")
    return FilterRule(rule: String(format: ruleFormat, trigger, actionType))
}

private func wrappedDomain(_ allowedDomain: AllowedDomain) -> String {
    let wrappedDomainFormat = "\"^[^:]+:(\\/\\/)?([^\\/]+\\\\.)?%@[^\\\\.][\\/:]?\""
    return String(format: wrappedDomainFormat, allowedDomain)
}
