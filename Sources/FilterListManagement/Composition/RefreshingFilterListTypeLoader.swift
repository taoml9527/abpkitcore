// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// A strategy deciding from which loader to load based on filter list refresh status
final class RefreshingFilterListTypeLoader: FilterListTypeLoader {
    private let refreshStatusLoader: FilterListRefreshStatusLoader
    private let oldFilterListLoader: FilterListTypeLoader
    private let freshFilterListLoader: FilterListTypeLoader

    /// Creates a new instance
    /// - Parameters:
    ///   - refreshStatusLoader: To be used to determine if filter list for a given source needs refreshing
    ///   - oldFilterListLoader: The loader to be used to load the filter list when no refresh is needed
    ///   - freshFilterListLoader: The loader to be used to load the filter list when refresh is needed
    init(refreshStatusLoader: FilterListRefreshStatusLoader,
         oldFilterListLoader: FilterListTypeLoader,
         freshFilterListLoader: FilterListTypeLoader) {
        self.refreshStatusLoader = refreshStatusLoader
        self.oldFilterListLoader = oldFilterListLoader
        self.freshFilterListLoader = freshFilterListLoader
    }

    func loadFilterList(forSource source: FilterListType, completion: @escaping LoadCompletion) {
        func handlerRefreshState() -> (FilterListRefreshStatus) -> Void {
            return { [weak self] refreshStatus in
                switch refreshStatus {
                case .needsRefresh:
                    self?.freshFilterListLoader.loadFilterList(forSource: source, completion: completion)
                case .noRefreshNeeded:
                    self?.oldFilterListLoader.loadFilterList(forSource: source, completion: completion)
                }
            }
        }

        refreshStatusLoader.loadRefreshStatus(forSource: source,
                                              completion: handlerRefreshState())
    }
}
