// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// A decoration for remote source filter list loader that adds the functionality of caching the loaded filter list
final class FilterListTypeLoaderWithCaching: FilterListTypeLoader {
    private let filterListCache: RemoteSourceFilterListCache
    private let decoratee: FilterListTypeLoader

    /// Creates an instances
    /// - Parameters:
    ///   - filterListCache: To be used to cache the loaded filter list
    ///   - decoratee: To be used to load the filter list
    init(decoratee: FilterListTypeLoader, filterListCache: RemoteSourceFilterListCache) {
        self.filterListCache = filterListCache
        self.decoratee = decoratee
    }

    func loadFilterList(forSource source: FilterListType, completion: @escaping LoadCompletion) {
        func completeWithFailure(error: Error) {
            completion(.failure(error))
        }

        func completeWithFilterList(_ list: FilterList) {
            completion(.success(list))
        }

        func cacheLoadedFilterListAndComplete() -> (FilterList) -> Void {
            return { [weak self] filterList in
                func ignoreCacheResult(_ result: RemoteSourceFilterListCache.CachingResult) {}

                self?.filterListCache.cacheFilterList(filterList,
                                                     forSource: source,
                                                     completion: ignoreCacheResult)
                completeWithFilterList(filterList)
            }
        }

        decoratee.loadFilterList(forSource: source,
                                 completion: handleResult(cacheLoadedFilterListAndComplete(),
                                                          completeWithFailure))
    }
}
