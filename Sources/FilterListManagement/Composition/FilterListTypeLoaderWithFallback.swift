// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// A composite that composes two remote source rules loader to provide the fallback mechanism
final class FilterListTypeLoaderWithFallback: FilterListTypeLoader {
    private let primarySourceLoader: FilterListTypeLoader
    private let secondarySourceLoader: FilterListTypeLoader

    /// Creates an instance
    /// - Parameters:
    ///   - primarySourceLoader: The loader to load from first
    ///   - secondarySourceLoader: The loader to be load from if the primary load failed
    init(primarySourceLoader: FilterListTypeLoader, secondarySourceLoader: FilterListTypeLoader) {
        self.primarySourceLoader = primarySourceLoader
        self.secondarySourceLoader = secondarySourceLoader
    }

    func loadFilterList(forSource source: FilterListType, completion: @escaping LoadCompletion) {

        func completeWithFilterList(_ list: FilterList) {
            completion(.success(list))
        }

        func ignoreErrorAndLoadFromFallback() -> (Error) -> Void {
            return { [weak self] _ in
                self?.secondarySourceLoader.loadFilterList(forSource: source,
                                                           completion: completion)
            }
        }

        primarySourceLoader.loadFilterList(forSource: source,
                                           completion: handleResult(completeWithFilterList,
                                                                    ignoreErrorAndLoadFromFallback()))
    }
}
