// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// A component that allow to load filter from bundle if primary source for remote filter list failed to load the list
final class RemoteSourceFilterListLoaderWithBundleFallback: FilterListTypeLoader {
    private let decoratee: FilterListTypeLoader
    private let bundleSourceLoader: FilterListTypeLoader

    /// Creates an instance
    /// - Parameters:
    ///   - decoratee: The primary loader to be used to load the filter list
    ///   - bundleLoader: The loader to be used to load the filter list from bundle if primary loader failed
    init(decoratee: FilterListTypeLoader, bundleSourceLoader: FilterListTypeLoader) {
        self.decoratee = decoratee
        self.bundleSourceLoader = bundleSourceLoader
    }

    func loadFilterList(forSource source: FilterListType, completion: @escaping LoadCompletion) {
        func completeWithRules(_ rules: FilterList) {
            completion(.success(rules))
        }

        func ignoreErrorAndLoadFromBundle() -> (Error) -> Void {
            return { [weak self] _ in
                self?.bundleSourceLoader.loadFilterList(forSource: source.fallbackBundledSource,
                                                       completion: completion)
            }
        }

        decoratee.loadFilterList(forSource: source,
                                 completion: handleResult(completeWithRules, ignoreErrorAndLoadFromBundle()))
    }
}

private extension FilterListType {
    var fallbackBundledSource: FilterListType {
        switch self {
        case .abp:
            return self
        case let .custom(path):
            return .custom(path.fileNameFromPath)
        }
    }
}

extension FilterListPath {
    var fileNameFromPath: String {
        let url = URL(string: self)
        let fileName = url?.lastPathComponent
        return fileName ?? " " // Forward invalid path
    }
}
