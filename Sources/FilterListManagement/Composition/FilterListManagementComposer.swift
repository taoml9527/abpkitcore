// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// A repository to be used to perform allowed domain operations such as Load, Add and Remove
public typealias AllowedDomainsRepository = AllowedDomainsAdder & AllowedDomainsLoader & AllowedDomainsRemover

/// Composition root for FilterListManagement features
public final class FilterListManagementComposer {

    // MARK: - Public API

    /// The filter list loader to be used to load a filter list for a particular filter list source.
    ///
    /// Functionalities:
    /// - Load a filter list for bundled source.
    /// - Load a filter list for remote source.
    /// - If load from remote server failed, will load the latest cached filter list.
    /// - If cached filter list is expired or corupted load the filter list from remote
    /// - I failed to load the filter for remote source, will try to load the bundled couterpart from bundle, if there is any.
    /// - Other remote source filter loading functionaliy is expresed by remoteFilterListLoader, see below.
    public private(set) lazy var filterListLoader = makeFilterListLoader()

    /// Domains repository to be used to load, add, remove allowed domains. All of the domains will be added as a rule into the filter list
    /// loaded by FilterListLoader
    public private(set) lazy var allowedDomainsRepository: AllowedDomainsRepository = localAllowedDomainsLoader

    /// To be used to determine for which filter list sources, the downloaded filter lists are expired.
    public private(set) lazy var filterListExpiredSourcesLoader = makeFilterListExpiredSourcesLoader()

    /// A remote filter list loader to be used to alway load filter lists from remote server.
    /// This loader will not fallback to cache or bundle.
    ///
    /// Functionalities:
    /// - Always load the filter list from remote. Will populate the filter list load url with appropriate with appropriate filter list meta data
    /// - Cache the downloaded filter list
    /// - Update the meta data for the downloaded filter list
    ///
    /// This loader is a subcomponent of filterListLoader, thus it has a subset of filterListLoader functionality.
    public private(set) lazy var remoteFilterListLoader = makeRemoteFilterListLoader()

    // MARK: - Private components

    // Below are the base components from which all of the high level components are build through composition.
    // Those are exposed here, internally for integration testing purpose:
    // If we want to test that FilterListLoader is composed correctly we can mock the base components to assert for
    // correct side effects - read/write from cache, load from remote, load from bundle

    /// Loads the filter lists from bundle
    lazy var bundleSourceFilterListLoader: FilterListTypeLoader = BundledFilterListLoader(filterListBundle: clientAppBundle).withBundleLoadLoggingAdded()
    /// A remote source filter list loader which contains all of the logic related to loading a filter list for a remote source
    lazy var remoteSourceFilterListLoader: FilterListTypeLoader = makeRefreshingFilterListRemoteSourceLoader()

    /// The actual storage of allowed domains
    lazy var allowedDomainsStore: AllowedDomainsStore = fileSystemStore.withLoggingAdded()
    /// Loads the allowed domains
    lazy var allowedDomainsLoader: AllowedDomainsLoader = localAllowedDomainsLoader
    /// Adds new allowed domains to persistence storage
    lazy var allowedDomainsAdder: AllowedDomainsAdder = localAllowedDomainsLoader
    /// Removes allowed domains from persistence storage
    lazy var allowedDomainsRemover: AllowedDomainsRemover = localAllowedDomainsLoader

    /// The actual storage of filter lists
    lazy var filterListCacheStore: FilterListCacheStore = fileSystemStore.withLoggingAdded()
    /// Loads and stores the filter lists to persitence storage
    lazy var cachedFilterListLoader = CachedFilterListLoader(filterListStore: filterListCacheStore)
    /// Loads the refresh status for a given remote filter list source
    lazy var filterListRefreshStatusLoader: FilterListRefreshStatusLoader = timestampedRefreshStatusFilterListLoader
    /// The actual storage of filter list meta info
    lazy var metaInfoStore: DonwloadedFilterListMetaInfoStore = fileSystemStore.withLoggingAdded()
    /// Loads the filter list meta info from persistence storage
    lazy var metaInfoLoader: DownloadedFilterListMetaInfoLoader = CachedFilterListMetaInfoLoader(metaInfoStore: metaInfoStore).withLoggingAdded()
    /// Updates the filter list meta info from persistence storage
    lazy var metaInfoUpdater: DownloadedFilterListMetaInfoUpdater = CachedFilterListMetaInfoUpdater(metaInfoStore: metaInfoStore,
                                                                                                    dateBuilder: systemTime)

    /// Client used to perform HTTP calls
    lazy var httpClient: HTTPClient = URLSessionHTTPClient()
    /// The remote store for filter lists
    lazy var remoteStore: RemoteFilterListStore = HTTPRemoteFilterListStore(httpClient: httpClient).withLoggingAdded()

    // Those are concrete components that implement multiple protocols

    private lazy var timestampedRefreshStatusFilterListLoader = makeFilterListRefreshStatusLoader()
    private lazy var localAllowedDomainsLoader = LocalAllowedDomainsLoader(allowedDomainsStore: allowedDomainsStore)
    private lazy var fileSystemStore: FileSystemStore = FileSystemStore(containerURL: storageContainerURL)

    // MARK: - Config properties

    private let clientMetaData: ClientMetaData
    private let clientAppBundle: Bundle
    private let storageContainerURL: URL
    private let systemTime: () -> Date

    /// Builds a new instance with configuration parameters.
    ///
    /// - Parameters:
    ///   - clientMetaData: To be used to accordingly populate the filter list download requests
    ///   - clientAppBundle: To be used to load all of the bundled filter lists
    ///   - storageContainerURL: A container URL where all of the necessary data is stored - filter lists, meta data, allowed domains.
    ///   - currentDate: Current system time. It will be used to timestamp the downloaded filter list, also to determine when a filter list is expired.
    ///                  You provide a custom builder if you have a custom definition of the current time.
    /// - Returns: An instance of filter list loader
    public init(clientMetaData: ClientMetaData,
                clientAppBundle: Bundle,
                storageContainerURL: URL,
                systemTime: @escaping () -> Date = Date.init) {
        self.clientMetaData = clientMetaData
        self.clientAppBundle = clientAppBundle
        self.storageContainerURL = storageContainerURL
        self.systemTime = systemTime
    }

    // MARK: - Public functions

    /// Creates a instance of FilterListAutoUpdater to be used to perform periodic auto updates
    /// - Parameters:
    ///   - updater: The updater to be used to perform filter lists update. FilterListAutoUpdater will trigger periodic update requests
    ///                      on the updater. Thus the actual update is done by the updater depending on the framework client needs.
    ///   - updatesCheckInterval: The interval at which to check if an update is available.
    /// - Returns: A instance of FilterListAutoUpdater
    public func makeAutoUpdater<UpdateEvent>(using updater: @escaping FilterListUpdater<UpdateEvent>) -> FilterListAutoUpdater<UpdateEvent> {
        FilterListAutoUpdater(timeTicker: BackgroundTimeTicker(),
                              filterListUpdater: updater)
    }

    // MARK: - Internal

    func makeFilterListLoader() -> FilterListLoader {
        return remoteSourceFilterListLoader.withFallback(toBundle: bundleSourceFilterListLoader)
                                 .composed(with: bundleSourceFilterListLoader)
                                 .withAllowedDomainsAdded(allowedDomainsLoader)
    }

    func makeFilterListRefreshStatusLoader() -> TimestampedFilterListRefreshStatusLoader {
        return TimestampedFilterListRefreshStatusLoader(metaInfoLoader: metaInfoLoader,
            currentDate: systemTime)
    }

    func makeFilterListExpiredSourcesLoader() -> FilterListExpiredSourcesLoader {
        return timestampedRefreshStatusFilterListLoader
    }

    func makeRemoteFilterListLoader() -> FilterListTypeLoader {
        let metaInfoUpdater = CachedFilterListMetaInfoUpdater(metaInfoStore: metaInfoStore,
                                                                        dateBuilder: systemTime)
        return RemoteFilterListLoader(remoteFilterListStore: remoteStore,
                                      clientMetaData: clientMetaData,
                                      filterListMetaInfoLoader: metaInfoLoader)
            .withMetaInfoUpdate(using: metaInfoUpdater)
            .withCaching(to: cachedFilterListLoader)
            .withRemoteLoadLoggingAdded()
    }

    func makeRefreshingFilterListRemoteSourceLoader() -> FilterListTypeLoader {
        return RefreshingFilterListTypeLoader(refreshStatusLoader: filterListRefreshStatusLoader.withLoggingAdded(),
                                                      oldFilterListLoader: cachedFilterListLoader.withFallback(to: remoteFilterListLoader),
                                                      freshFilterListLoader: remoteFilterListLoader.withFallback(to: cachedFilterListLoader))
    }
}

// MARK: - Composition DSLs

extension FilterListTypeLoader {
    func withFallback(toBundle bundledSourceLoader: FilterListTypeLoader) -> FilterListTypeLoader {
        RemoteSourceFilterListLoaderWithBundleFallback(decoratee: self, bundleSourceLoader: bundledSourceLoader)
    }

    func composed(with bundledSourceLoader: FilterListTypeLoader) -> FilterListLoader {
        FilterListLoaderStrategy(bundledSourceLoader: bundledSourceLoader, remoteSourceLoader: self)
    }

    func withFallback(to fallback: FilterListTypeLoader) -> FilterListTypeLoader {
        FilterListTypeLoaderWithFallback(primarySourceLoader: self, secondarySourceLoader: fallback)
    }

    func withCaching(to cache: RemoteSourceFilterListCache) -> FilterListTypeLoader {
        FilterListTypeLoaderWithCaching(decoratee: self, filterListCache: cache)
    }

    func withMetaInfoUpdate(using metaInfoUpdater: DownloadedFilterListMetaInfoUpdater) -> FilterListTypeLoader {
        FilterListTypeLoaderWithMetaInfoUpdate(decoratee: self,
                                                       metaInfoUpdater: metaInfoUpdater)
    }
}

extension FilterListLoader {
    func withAllowedDomainsAdded(_ allowedDomainsLoader: AllowedDomainsLoader) -> FilterListLoader {
        let loader = FilterListWithAllowedDomainsLoader(decoratee: self, allowedDomainsLoader: allowedDomainsLoader)
        loader.output = Logger()
        return loader
    }
}

extension FilterListTypeLoader {
    func withBundleLoadLoggingAdded() -> FilterListTypeLoader {
        BundledSourceFilterListLoaderWithLogging(decoratee: self)
    }

    func withRemoteLoadLoggingAdded() -> FilterListTypeLoader {
        RemoteSourceFilterListLoaderWithLogging(decoratee: self)
    }
}

extension RemoteFilterListStore {
    func withLoggingAdded() -> RemoteFilterListStore {
        RemoteFilterListStoreWithLogging(decoratee: self)
    }
}

extension FilterListCacheStore {
    func withLoggingAdded() -> FilterListCacheStore {
        FilterListCacheStoreWithLogging(decoratee: self)
    }
}

extension DownloadedFilterListMetaInfoLoader {
    func withLoggingAdded() -> DownloadedFilterListMetaInfoLoader {
        DownloadedFilterListMetaInfoLoaderWithLogging(decoratee: self)
    }
}

extension DonwloadedFilterListMetaInfoStore {
    func withLoggingAdded() -> DonwloadedFilterListMetaInfoStore {
        DonwloadedFilterListMetaInfoStoreWithLogging(decoratee: self)
    }
}

extension FilterListRefreshStatusLoader {
    func withLoggingAdded() -> FilterListRefreshStatusLoader {
        FilterListRefreshStatusLoaderWithLogging(decoratee: self)
    }
}

extension AllowedDomainsStore {
    func withLoggingAdded() -> AllowedDomainsStore {
        AllowedDomainsStoreWithLogging(decoratee: self)
    }
}
