// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// A decorator for RemoteSourceFilterListLoader that updates the meta info associated
/// with requested source
final class FilterListTypeLoaderWithMetaInfoUpdate: FilterListTypeLoader {
    private let decoratee: FilterListTypeLoader
    private let metaInfoUpdater: DownloadedFilterListMetaInfoUpdater

    /// Creates an instance
    /// - Parameters:
    ///   - decoratee: To be used to load the filter list for a given source
    ///   - metaInfoUpdater: To be used to update the meta info if filter list load succeeded
    init(decoratee: FilterListTypeLoader,
         metaInfoUpdater: DownloadedFilterListMetaInfoUpdater) {
        self.decoratee = decoratee
        self.metaInfoUpdater = metaInfoUpdater
    }

    func loadFilterList(forSource source: FilterListType, completion: @escaping LoadCompletion) {

        func completeWithFailure(_ error: Error) {
            completion(.failure(error))
        }

        func completeWithFilterList(_ list: FilterList) {
            completion(.success(list))
        }

        func updateMetaDataAndCompleteWithRules() -> (FilterList) -> Void {
            return { [weak self] filterList in

                func ignoreUpdateResult(_ result: DownloadedFilterListMetaInfoUpdater.UpdateResult) {}

                self?.metaInfoUpdater.updateMetaInfo(forSource: source, completion: ignoreUpdateResult)
                completeWithFilterList(filterList)
            }
        }

        decoratee.loadFilterList(forSource: source,
                                 completion: handleResult(updateMetaDataAndCompleteWithRules(), completeWithFailure))
    }
}
