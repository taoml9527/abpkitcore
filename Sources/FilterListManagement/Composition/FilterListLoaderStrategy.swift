// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// A strategy that decides wich loader to use at runtime based on a filter list source
final class FilterListLoaderStrategy: FilterListLoader {
    private let bundledSourceLoader: FilterListTypeLoader
    private let remoteSourceLoader: FilterListTypeLoader

    /// Creates an instance
    /// - Parameters:
    ///   - bundledLoader: The bundled loader to be used to load the filter list for bundled sources
    ///   - remoteLoader: The remote loader to be used to load the filter list for remote sources
    init(bundledSourceLoader: FilterListTypeLoader, remoteSourceLoader: FilterListTypeLoader) {
        self.bundledSourceLoader = bundledSourceLoader
        self.remoteSourceLoader = remoteSourceLoader
    }

    func loadFilterList(forSource source: FilterListSource, completion: @escaping LoadCompletion) {
        switch source {
        case let .bundled(bundledSource):
            bundledSourceLoader.loadFilterList(forSource: bundledSource, completion: completion)
        case let .remote(remoteSource):
            remoteSourceLoader.loadFilterList(forSource: remoteSource, completion: completion)
        }
    }
}
