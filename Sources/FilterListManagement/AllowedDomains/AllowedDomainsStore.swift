// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// A store that provides the AllowedDomains storing functionality
protocol AllowedDomainsStore {
    /// The result of retrievieng the raw allowed domains from the storage, containing either the domains or an error
    typealias RetrieveResult = Result<[String], Error>
    /// A closure to be used to handle RetrieveResult
    typealias RetrieveCompletion = (RetrieveResult) -> Void

    /// The result of inserting the allowed domain, either success or error
    typealias InsertResult = Result<Void, Error>
    /// A closure to be used to handle InsetResult
    typealias InsertCompletion = (InsertResult) -> Void

    /// Retrieves all of the stored domains
    /// - Parameter completion: Closure to handle the retrieved allowed domains
    func retrieveDomains(completion: @escaping RetrieveCompletion)

    /// Inserts the given domains to the storage
    /// - Parameters:
    ///   - domains: Domains to be inserted
    ///   - completion: Closure the handle the insert result
    func insertDomains(_ allowedDomains: [String], completion: @escaping InsertCompletion)
}
