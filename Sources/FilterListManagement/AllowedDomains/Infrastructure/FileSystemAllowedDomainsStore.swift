// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

extension FileSystemStore: AllowedDomainsStore {
    var allowedDomainsStore: String {
        "abpKitAllowedDomains.store"
    }

    func retrieveDomains(completion: @escaping AllowedDomainsStore.RetrieveCompletion) {
        func parseDomains(_ data: Data) -> AllowedDomainsStore.RetrieveResult {
            Result {
                try JSONDecoder().decode(AllowedDomains.self, from: data)
            }.mapError(ABPKitCoreError.failedToDecodeAllowedDomains)
        }

        read(fromFileNamed: allowedDomainsStore,
             completion: mapError(mapToClosure(completion, parseDomains),
                                  ABPKitCoreError.failedToLoadAlowedDomains))
    }

    func insertDomains(_ allowedDomains: [String], completion: @escaping AllowedDomainsStore.InsertCompletion) {
        let encoder = JSONEncoder()

        Result { try encoder.encode(allowedDomains) }
            .onFailure { completion(.failure(ABPKitCoreError.failedToEncodeAllowedDomains($0))) }
            .onSuccess { data in
                write(data: data,
                      toFileNamed: allowedDomainsStore,
                      completion: mapError(completion, ABPKitCoreError.failedToStoreAllowedDomains))
            }
    }
}
