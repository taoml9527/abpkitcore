// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Handles adding allowed domains
public protocol AllowedDomainsAdder {
    /// The result of adding allowed domains, containing the new state after the domains where added
    /// or an error
    typealias AddResult = Result<AllowedDomains, Error>
    /// A closure to be used to handle the AddResult
    typealias AddCompletion = (AddResult) -> Void

    /// Adds the provided domains
    /// - Parameters:
    ///   - domains: Domains to be added
    ///   - completion: Closure to handle the new state after the domains were added
    func addDomains(_ allowedDomains: AllowedDomains, completion: @escaping AddCompletion)
}
