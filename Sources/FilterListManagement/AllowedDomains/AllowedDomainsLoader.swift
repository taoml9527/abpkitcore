// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Allowed domain representation backed by a string
public typealias AllowedDomain = String

/// A list of allowed domains
public typealias AllowedDomains = [AllowedDomain]

/// Handles loading all of the saved allowed domains
public protocol AllowedDomainsLoader {
    /// The result of loading allowed domains, containing either AllowedDomains or an Error
    typealias LoadResult = Result<AllowedDomains, Error>
    /// A closure to be used to handle the LoadResult
    typealias LoadCompletion = (LoadResult) -> Void

    /// Loads the available allowed domains
    /// - Parameter completion: Closure to handle the loaded allowed domains
    func loadDomains(completion: @escaping LoadCompletion)
}
