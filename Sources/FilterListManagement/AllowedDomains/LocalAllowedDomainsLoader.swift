// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Handle the caching logic for allowed domains
final class LocalAllowedDomainsLoader {
    private let allowedDomainsStore: AllowedDomainsStore

    /// Creates a new instance
    /// - Parameter allowedDomainsStore: To be used to retrieve and insert the allowed domains
    init(allowedDomainsStore: AllowedDomainsStore) {
        self.allowedDomainsStore = allowedDomainsStore
    }
}

extension LocalAllowedDomainsLoader: AllowedDomainsLoader {
    func loadDomains(completion: @escaping LoadCompletion) {
        allowedDomainsStore.retrieveDomains(completion: completion)
    }
}

extension LocalAllowedDomainsLoader: AllowedDomainsAdder {
    func addDomains(_ allowedDomains: [AllowedDomain], completion: @escaping AddCompletion) {
        allowedDomainsStore.retrieveDomains { [weak self] result in
            guard let self = self else { return }

            let oldDomains = (try? result.get()) ?? []
            let addedDomains = allowedDomains.filter { !oldDomains.contains($0) }
            let newDomains = oldDomains + addedDomains

            func completeWithFailure(_ error: Error) {
                completion(.failure(error))
            }

            func completeWithDomains() {
                completion(.success(newDomains))
            }

            self.allowedDomainsStore.insertDomains(newDomains,
                                                   completion: handleResult(completeWithDomains, completeWithFailure))
        }
    }
}

extension LocalAllowedDomainsLoader: AllowedDomainsRemover {
    func removeDomains(_ allowedDomains: AllowedDomains, completion: @escaping RemoveCompletion) {
        func completeWithFailure(_ error: Error) {
            completion(.failure(error))
        }

        func removeDomain() -> ([String]) -> Void {
            return { [weak self] storedDomains in
                let newDomains = storedDomains.filter { !allowedDomains.contains($0) }

                func completeWithNewList() {
                    completion(.success(newDomains))
                }
                self?.allowedDomainsStore.insertDomains(newDomains,
                                                        completion: handleResult(completeWithNewList, completeWithFailure))
            }
        }

        allowedDomainsStore.retrieveDomains(completion: handleResult(removeDomain(), completeWithFailure))
    }
}
