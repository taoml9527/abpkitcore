// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

extension FileSystemStore: DonwloadedFilterListMetaInfoStore {
    private var metaInfoStore: String {
        "abpKitFilterListMetaInfo.store"
    }

    private struct CodableMetaInfo: Codable {
        let codableSource: CodableFilterListType
        let lastDownloadDate: Date
        let downloadCount: Int

        init(metaInfo: DownloadedFilterListMetaInfo) {
            codableSource = CodableFilterListType(source: metaInfo.source)
            lastDownloadDate = metaInfo.lastDownloadDate
            downloadCount = metaInfo.downloadCount
        }

        var metaInfo: DownloadedFilterListMetaInfo {
            .init(source: codableSource.source,
                  lastDownloadDate: lastDownloadDate,
                  downloadCount: downloadCount)
        }
    }

    func insertMetaInfoList(_ metaInfoList: [DownloadedFilterListMetaInfo],
                            completion: @escaping DonwloadedFilterListMetaInfoStore.InsertCompletion) {
        let encoder = JSONEncoder()
        let cache = metaInfoList.map(CodableMetaInfo.init)

        Result { try encoder.encode(cache) }
            .onFailure { completion(.failure($0)) }
            .onSuccess { data in
                write(data: data,
                      toFileNamed: metaInfoStore,
                      completion: mapError(completion, ABPKitCoreError.failedToInsertFilterListMetaInfo))
            }
    }

    func retrieveMetaInfoList(completion: @escaping DonwloadedFilterListMetaInfoStore.RetrieveCompletion) {
        func parseMetaInfo(_ data: Data) -> DonwloadedFilterListMetaInfoStore.RetrieveResult {
            Result {
                let decoder = JSONDecoder()
                let cache = try decoder.decode([CodableMetaInfo].self, from: data)
                return cache.map { $0.metaInfo }
            }.mapError(ABPKitCoreError.failedToDecodeFilterListMetaInfo)
        }

        read(fromFileNamed: metaInfoStore,
             completion: mapError(mapToClosure(completion, parseMetaInfo),
                                  ABPKitCoreError.failedToLoadFilterListMetaInfo))
    }
}
