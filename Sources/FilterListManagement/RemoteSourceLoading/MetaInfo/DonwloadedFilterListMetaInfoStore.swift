// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Interface describing an actual storage for rules meta info
protocol DonwloadedFilterListMetaInfoStore {
    typealias RetrieveResult = Result<[DownloadedFilterListMetaInfo], Error>
    typealias RetrieveCompletion = (RetrieveResult) -> Void

    typealias InsertResult = Result<Void, Error>
    typealias InsertCompletion = (InsertResult) -> Void

    /// Retrieves the meta info list from the persistence storage
    /// - Parameters:
    ///   - completion: A completion handler for retrieval operation
    func retrieveMetaInfoList(completion: @escaping RetrieveCompletion)

    /// Inserts the filter list meta info to the persistance storage
    ///
    /// - Parameters:
    ///   - metaInfoList: The list of meta info to be inserted
    ///   - completion: A completion handler for save operation
    func insertMetaInfoList(_ metaInfoList: [DownloadedFilterListMetaInfo],
                            completion: @escaping InsertCompletion)
}
