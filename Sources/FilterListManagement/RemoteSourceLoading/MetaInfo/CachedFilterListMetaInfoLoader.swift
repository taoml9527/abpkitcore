// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Handles the filter lists storing logic
final class CachedFilterListMetaInfoLoader {

    private let metaInfoStore: DonwloadedFilterListMetaInfoStore

    /// Creates an instance
    /// - Parameters:
    ///   - metaInfoStore: The store that stores the actual meta info
    init(metaInfoStore: DonwloadedFilterListMetaInfoStore) {
        self.metaInfoStore = metaInfoStore
    }
}

extension CachedFilterListMetaInfoLoader: DownloadedFilterListMetaInfoLoader {
    struct MissingMetaInfoError: Error, Equatable {}

    func loadMetaInfo(forSource source: FilterListType,
                      completion: @escaping LoadCompletion) {
        func completeWithFailure(_ error: Error) {
            completion(.failure(error))
        }

        func completeWithMetaInfo(_ metaInfo: DownloadedFilterListMetaInfo) {
            completion(.success(metaInfo))
        }

        func lookupForMetaInfo(from metaInfoList: [DownloadedFilterListMetaInfo]) {
            if let metaForSource = metaInfoList.first(where: { $0.source == source }) {
                completeWithMetaInfo(metaForSource)
            } else {
                completeWithFailure(MissingMetaInfoError())
            }
        }

        metaInfoStore.retrieveMetaInfoList(completion: handleResult(lookupForMetaInfo, completeWithFailure))
    }

    func loadAllMetaInfo(completion: @escaping LoadAllCompletion) {
        metaInfoStore.retrieveMetaInfoList(completion: completion)
    }
}

final class CachedFilterListMetaInfoUpdater {
    typealias DateBuilder = () -> Date

    private let metaInfoStore: DonwloadedFilterListMetaInfoStore
    private let dateBuilder: DateBuilder

    /// Creates an instance
    /// - Parameters:
    ///   - metaInfoStore: The store that stores the actual meta info
    ///   - dateBuilder: The builder that provides the data to be used for saving the meta info
    init(metaInfoStore: DonwloadedFilterListMetaInfoStore,
         dateBuilder: @escaping DateBuilder) {
        self.metaInfoStore = metaInfoStore
        self.dateBuilder = dateBuilder
    }
}

extension CachedFilterListMetaInfoUpdater: DownloadedFilterListMetaInfoUpdater {
    func updateMetaInfo(forSource source: FilterListType,
                        completion: @escaping UpdateCompletion) {
        let date = dateBuilder()

        func makeInitialMetaInfo() -> DownloadedFilterListMetaInfo {
            DownloadedFilterListMetaInfo(source: source,
                                         lastDownloadDate: date,
                                         downloadCount: 1)
        }

        func ignoreErrorCreateInitialMetaInfo(_ error: Error) -> [DownloadedFilterListMetaInfo] {
            [makeInitialMetaInfo()]
        }

        func appendNewMetaInfo(to loadedMetaInfoList: [DownloadedFilterListMetaInfo]) -> [DownloadedFilterListMetaInfo] {
            if let oldMeta = loadedMetaInfoList.first(where: { $0.source == source }) {
                let metaInfoListWithoutOldMeta = loadedMetaInfoList.filter { $0.source != source }
                let newMeta = DownloadedFilterListMetaInfo(source: source,
                                                           lastDownloadDate: date,
                                                           downloadCount: oldMeta.downloadCount + 1)
                return metaInfoListWithoutOldMeta + [newMeta]
            } else {
                return loadedMetaInfoList + [makeInitialMetaInfo()]
            }
        }

        func saveMetaInfoList() -> ([DownloadedFilterListMetaInfo]) -> Void {
            return { [weak self] list in
                self?.metaInfoStore.insertMetaInfoList(list, completion: completion)
            }
        }

        let saveWithInitalMetaInfo = ignoreErrorCreateInitialMetaInfo >>> saveMetaInfoList()
        let saveByAppendingMetaInfo = appendNewMetaInfo >>> saveMetaInfoList()

        metaInfoStore.retrieveMetaInfoList(completion: handleResult(saveByAppendingMetaInfo, saveWithInitalMetaInfo))
    }
}
