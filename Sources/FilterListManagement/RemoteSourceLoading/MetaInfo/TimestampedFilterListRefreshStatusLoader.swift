// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// A tiestamp rules refresh checker impplementation, will determine if rules need refreshing
/// based on last download date
final class TimestampedFilterListRefreshStatusLoader {
    private static let filterListExpiration: TimeInterval = 86400
    private let metaInfoLoader: DownloadedFilterListMetaInfoLoader
    private let currentDate: () -> Date

    /// Creates an instance
    /// - Parameter rulesMetaInfoLoader: To be used to load the meta info for a given source
    init(metaInfoLoader: DownloadedFilterListMetaInfoLoader, currentDate: @escaping () -> Date) {
        self.metaInfoLoader = metaInfoLoader
        self.currentDate = currentDate
    }

    func refreshStatus(forMetaInfo metaInfo: DownloadedFilterListMetaInfo) -> FilterListRefreshStatus {
        if FilterListExpirationPolicy.isExpired(metaInfo.lastDownloadDate, against: currentDate()) {
            return .needsRefresh
        } else {
            return .noRefreshNeeded
        }
    }
}

extension TimestampedFilterListRefreshStatusLoader: FilterListRefreshStatusLoader {
    func loadRefreshStatus(forSource source: FilterListType,
                           completion: @escaping FilterListRefreshStatusLoader.LoadCompletion) {
        func completeWithComputedStatus() -> (DownloadedFilterListMetaInfo) -> Void {
            return { [weak self] metaInfo in
                guard let self = self else {
                    return
                }
                completion(self.refreshStatus(forMetaInfo: metaInfo))
            }
        }

        func needsRefreshOnError(_ error: Error) {
            completion(.needsRefresh)
        }

        let onLoad = handleResult(completeWithComputedStatus(), needsRefreshOnError)
        metaInfoLoader.loadMetaInfo(forSource: source,
                                    completion: onLoad)
    }
}

extension TimestampedFilterListRefreshStatusLoader: FilterListExpiredSourcesLoader {
    func loadExpiredSources(completion: @escaping FilterListExpiredSourcesLoader.LoadCompletion) {
        func completeWithFailure(_ error: Error) {
            completion(.failure(error))
        }

        func completeWithExpiredSources() -> ([DownloadedFilterListMetaInfo]) -> Void {
            return { [weak self] metaInfoList in
                guard let self = self else { return }

                completion(Result {
                    metaInfoList.filter { self.refreshStatus(forMetaInfo: $0) == .needsRefresh }
                        .map { $0.source }
                })
            }
        }

        metaInfoLoader.loadAllMetaInfo(completion: handleResult(completeWithExpiredSources(),
                                                                completeWithFailure))
    }
}
