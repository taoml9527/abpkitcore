// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Provides the ability to load the downloaed filter list meta info for a given source
protocol DownloadedFilterListMetaInfoLoader {
    typealias LoadResult = Result<DownloadedFilterListMetaInfo, Error>
    typealias LoadCompletion = (LoadResult) -> Void

    typealias LoadAllResult = Result<[DownloadedFilterListMetaInfo], Error>
    typealias LoadAllCompletion = (LoadAllResult) -> Void

    /// Loads the meta info for a given source
    /// - Parameters:
    ///   - forSource: The source for which to load the meta info
    ///   - completion: A handler for load completion
    func loadMetaInfo(forSource source: FilterListType,
                      completion: @escaping LoadCompletion)

    /// Loads all of the avaialble meta info
    /// - Parameter completion: A handler for load all completion
    func loadAllMetaInfo(completion: @escaping LoadAllCompletion)
}
