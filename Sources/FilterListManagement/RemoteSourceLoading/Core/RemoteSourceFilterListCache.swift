// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Provides the ability to cache the filter list for a given source
protocol RemoteSourceFilterListCache {
    typealias CachingResult = Result<Void, Error>
    typealias CachingCompletion = (CachingResult) -> Void

    /// Cache the filter list for a given source
    ///
    /// - Parameters:
    ///   - filterList: The filter list to be cached
    ///   - source: To source for which to cache the filter list
    ///   - completion: A completion handler for cache operation
    func cacheFilterList(_ filterList: FilterList,
                         forSource source: FilterListType,
                         completion: @escaping CachingCompletion)
}
