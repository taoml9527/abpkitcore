// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Provides the ability to load the downloaed filter list meta info for a given source
protocol DownloadedFilterListMetaInfoUpdater {
    typealias UpdateResult = Result<Void, Error>
    typealias UpdateCompletion = (UpdateResult) -> Void

    /// Updates the meta info for a given source
    /// - Parameters:
    ///   - forSource: Source for which to update the meta info
    ///   - completion: A handler of save completion
    func updateMetaInfo(forSource source: FilterListType,
                        completion: @escaping UpdateCompletion)
}
