// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Handles the logic of rules loading and caching from a local storage
final class CachedFilterListLoader {
    private let filterListStore: FilterListCacheStore

    /// Creates a new instance
    /// - Parameter filterListStore: To be used to retrieve and insert the filter lists
    init(filterListStore: FilterListCacheStore) {
        self.filterListStore = filterListStore
    }
}

extension CachedFilterListLoader: FilterListTypeLoader {
    func loadFilterList(forSource source: FilterListType, completion: @escaping LoadCompletion) {
        func mapRawFilterList(_ rawFilterList: RawFilterList) {
            completion(.success(FilterList(content: rawFilterList)))
        }

        func completeWithFailure(_ error: Error) {
            completion(.failure(error))
        }
        filterListStore.retrieveFilterList(withName: source.filterListName,
                                           completion: handleResult(mapRawFilterList, completeWithFailure))
    }
}

extension CachedFilterListLoader: RemoteSourceFilterListCache {
    func cacheFilterList(_ filterList: FilterList,
                         forSource source: FilterListType,
                         completion: @escaping CachingCompletion) {
        filterListStore.insertFilterList(filterList.content,
                                         withName: source.filterListName,
                                         completion: completion)
    }
}

private extension FilterListType {
    var filterListName: String {
        switch self {
        case let .abp(source):
            return source.filterListName
        case let .custom(path):
            return path.fileNameFromPath
        }
    }
}

private extension FilterListAAVariant {
    var filterListName: String {
        switch self {
        case let .withAA(source):
            switch source {
            case .easyList:
                return "easyList+exceptionRules.json"
            case .easyListChina:
                return "easyListChina+exceptionRules.json"
            case .easyListDutch:
                return "easyListDutch+exceptionRules.json"
            case .easyListGermany:
                return "easyListGermany+exceptionRules.json"
            case .easyListItaly:
                return "easyListItaly+exceptionRules.json"
            case .easyListSpain:
                return "easyListSpain+exceptionRules.json"
            case .easyListFrance:
                return "easyListFrance+exceptionRules.json"
            case .easyListRussiaUkraine:
                return "easyListRussiaUkraine+exceptionRules.json"
            }
        case let .withoutAA(source):
            switch source {
            case .easyList:
                return "easyList.json"
            case .easyListChina:
                return "easyListChina.json"
            case .easyListDutch:
                return "easyListDutch.json"
            case .easyListGermany:
                return "easyListGermany.json"
            case .easyListItaly:
                return "easyListItaly.json"
            case .easyListSpain:
                return "easyListSpain.json"
            case .easyListFrance:
                return "easyListFrance.json"
            case .easyListRussiaUkraine:
                return "easyListRussiaUkraine.json"
            }
        }
    }
}
