// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

typealias RawFilterList = String

/// Provides the ability to perform operation on stored filter lists
protocol FilterListCacheStore {
    typealias RetrieveResult = Result<RawFilterList, Error>
    typealias RetrieveCompletion = (RetrieveResult) -> Void

    typealias InsertResult = Result<Void, Error>
    typealias InsertCompletion = (InsertResult) -> Void

    /// Retrieves the filter list from store for a given rules name
    /// - Parameters:
    ///   - name: The name of filter list to be retrieved
    ///   - completion: A completion handler for retrieve operation
    func retrieveFilterList(withName name: FilterListName,
                            completion: @escaping RetrieveCompletion)

    /// Inserts the filter list with a given name
    ///
    /// - Parameters:
    ///   - rules: The filter list to be inserted
    ///   - name: The name which identifies the list
    ///   - completion: A completion handler for insert operation
    func insertFilterList(_ filterList: RawFilterList,
                          withName name: FilterListName,
                          completion: @escaping InsertCompletion)
}
