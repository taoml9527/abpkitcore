// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Codable version of `FilterListSource`, useful if it is needed to store a `FilterListSource`
public enum CodableFilterListSource: Equatable, Codable {
    case bundled(CodableFilterListType)
    case remote(CodableFilterListType)

    /// Converts to domain source type
    public var source: FilterListSource {
        switch self {
        case let .bundled(bundledSource):
            return .bundled(bundledSource.source)
        case let .remote(remoteSource):
            return .remote(remoteSource.source)
        }
    }

    /// Creates a new instance from the given domain source
    /// - Parameter source: A source to be converted to Codable version
    public init(source: FilterListSource) {
        switch source {
        case let .bundled(bundledSource):
            self = .bundled(.init(source: bundledSource))
        case let .remote(remoteSource):
            self = .remote(.init(source: remoteSource))
        }
    }

    enum CodingKeys: CodingKey {
        case remote, bundled
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let key = container.allKeys.first

        switch key {
        case .remote:
            let remoteSource = try container.decode(CodableFilterListType.self, forKey: .remote)
            self = .remote(remoteSource)
        case .bundled:
            let bundledSource = try container.decode(CodableFilterListType.self, forKey: .bundled)
            self = .bundled(bundledSource)
        default:
            throw ABPKitCoreError.failedToDecodeFilterListSource
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        switch self {
        case let .remote(remoteSource):
            try container.encode(remoteSource, forKey: .remote)
        case let .bundled(bundledSource):
            try container.encode(bundledSource, forKey: .bundled)
        }
    }
}

/// Codable version of `FilterListBundledSource`, useful if it is needed to store a `FilterListBundledSource`
public enum CodableFilterListType: Equatable, Codable {
    case abp(CodableFilterListAAVariant)
    case custom(FilterListPath)

    /// Converts to domain source type
    public var source: FilterListType {
        switch self {
        case let .abp(bundledSource):
            return .abp(bundledSource.source)
        case let .custom(path):
            return .custom(path)
        }
    }

    /// Creates a new instance from the given domain source
    /// - Parameter source: A source to be converted to Codable version
    public init(source: FilterListType) {
        switch source {
        case let .abp(abpSource):
            self = .abp(.init(source: abpSource))
        case let .custom(path):
            self = .custom(path)
        }
    }

    enum CodingKeys: CodingKey {
        case abp, custom
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let key = container.allKeys.first

        switch key {
        case .abp:
            let abpSource = try container.decode(CodableFilterListAAVariant.self, forKey: .abp)
            self = .abp(abpSource)
        case .custom:
            let customSource = try container.decode(FilterListPath.self, forKey: .custom)
            self = .custom(customSource)
        default:
            throw ABPKitCoreError.failedToDecodeFilterListSource
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        switch self {
        case let .abp(abpSource):
            try container.encode(abpSource, forKey: .abp)
        case let .custom(customSource):
            try container.encode(customSource, forKey: .custom)
        }
    }
}

/// Codable version of `FilterListBundledABPSource`, useful if it is needed to store a `FilterListBundledABPSource`
public enum CodableFilterListAAVariant: Equatable, Codable {
    case withAA(CodableFilterListABPSource)
    case withoutAA(CodableFilterListABPSource)

    /// Converts to domain source type
    public var source: FilterListAAVariant {
        switch self {
        case let .withAA(aaSource):
            return .withAA(aaSource.source)
        case let .withoutAA(nonAASource):
            return .withoutAA(nonAASource.source)
        }
    }

    enum CodingKeys: CodingKey {
        case withAA, withoutAA
    }

    /// Creates a new instance from the given domain source
    /// - Parameter source: A source to be converted to Codable version
    public init(source: FilterListAAVariant) {
        switch source {
        case let .withAA(aaSource):
            self = .withAA(.init(source: aaSource))
        case let .withoutAA(nonAASource):
            self = .withoutAA(.init(source: nonAASource))
        }
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let key = container.allKeys.first

        switch key {
        case .withAA:
            let withAASource = try container.decode(CodableFilterListABPSource.self, forKey: .withAA)
            self = .withAA(withAASource)
        case .withoutAA:
            let withoutAASource = try container.decode(CodableFilterListABPSource.self, forKey: .withoutAA)
            self = .withoutAA(withoutAASource)
        default:
            throw ABPKitCoreError.failedToDecodeFilterListSource
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        switch self {
        case let .withAA(withAASource):
            try container.encode(withAASource, forKey: .withAA)
        case let .withoutAA(withoutAASource):
            try container.encode(withoutAASource, forKey: .withoutAA)
        }
    }
}

/// Codable version of `FilterListBundledWithAASource`, useful if it is needed to store a `FilterListBundledWithAASource`
public enum CodableFilterListABPSource: String, Equatable, Codable {
    case easyList = "easylist"
    case easyListChina = "easyListChina"
    case easyListDutch = "easyListDutch"
    case easyListGermany = "easyListGermany"
    case easyListItaly = "easyListItaly"
    case easyListSpain = "easyListSpain"
    case easyListFrance = "easyListFrance"
    case easyListRussiaUkraine = "easyListRussiaUkraine"

    /// Converts to domain source type
    public var source: FilterListABPSource {
        switch self {
        case .easyList:
            return .easyList
        case .easyListChina:
            return .easyListChina
        case .easyListDutch:
            return .easyListDutch
        case .easyListGermany:
            return .easyListGermany
        case .easyListItaly:
            return .easyListItaly
        case .easyListSpain:
            return .easyListSpain
        case .easyListFrance:
            return .easyListFrance
        case .easyListRussiaUkraine:
            return .easyListRussiaUkraine
        }
    }

    /// Creates a new instance from the given domain source
    /// - Parameter source: A source to be converted to Codable version
    public init(source: FilterListABPSource) {
        switch source {
        case .easyList:
            self = .easyList
        case .easyListChina:
            self = .easyListChina
        case .easyListDutch:
            self = .easyListDutch
        case .easyListGermany:
            self = .easyListGermany
        case .easyListItaly:
            self = .easyListItaly
        case .easyListSpain:
            self = .easyListSpain
        case .easyListFrance:
            self = .easyListFrance
        case .easyListRussiaUkraine:
            self = .easyListRussiaUkraine
        }
    }
}
