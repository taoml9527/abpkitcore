// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

extension FileSystemStore: FilterListCacheStore {
    func retrieveFilterList(withName name: FilterListName, completion: @escaping FilterListCacheStore.RetrieveCompletion) {
        func parseFilterList(_ data: Data) -> FilterListCacheStore.RetrieveResult {
            Result {
                guard let filterList = String(data: data, encoding: .utf8) else {
                    throw ABPKitCoreError.malformedFilterListContent
                }
                return filterList
            }
        }

        read(fromFileNamed: name,
             completion: mapError(mapToClosure(completion, parseFilterList),
                                  ABPKitCoreError.failedToLoadCachedFilterList) )
    }

    func insertFilterList(_ filterList: RawFilterList,
                          withName name: FilterListName,
                          completion: @escaping InsertCompletion) {
        guard let data = filterList.data(using: .utf8) else {
            return completion(.failure(ABPKitCoreError.malformedFilterListContent))
        }
        write(data: data,
              toFileNamed: name,
              completion: mapError(completion, ABPKitCoreError.failedToCacheFilterList))
    }
}
