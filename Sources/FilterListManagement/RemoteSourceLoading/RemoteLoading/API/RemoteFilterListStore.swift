// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Provides the ability of retrieving rules from a remote source
protocol RemoteFilterListStore {
    typealias DownloadResult = Result<FilterList, Error>
    typealias DownloadCompletion = (DownloadResult) -> Void

    /// Retrievies the filter list from a given remote url
    /// - Parameters:
    ///   - url: The remote url to load the rules from
    ///   - completion: A completion of retrieval operation
    func download(from url: URL, completion: @escaping DownloadCompletion)
}
