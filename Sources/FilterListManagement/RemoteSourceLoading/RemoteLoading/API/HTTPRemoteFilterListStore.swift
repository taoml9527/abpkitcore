// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Remote filter list store that downloads filter list using the given http client
/// and maps the response accordingly
final class HTTPRemoteFilterListStore: RemoteFilterListStore {

    private let httpClient: HTTPClient

    /// Creates an instance
    /// - Parameter httpClient: To be used to load filter list for a given url
    init(httpClient: HTTPClient) {
        self.httpClient = httpClient
    }

    func download(from url: URL, completion: @escaping DownloadCompletion) {
        func completeWithFailure(error: Swift.Error) {
            completion(.failure(error))
        }

        func onSuccess(result: FilterListGetResult) {
            completion(DownloadedRulesMapper.mapResult(result))
        }

        httpClient.get(from: url,
                       completion: handleResult(onSuccess, completeWithFailure))
    }
}

private final class DownloadedRulesMapper {
    static let validStatusCodes = 200...300

    static func mapResult(_ result: FilterListGetResult) -> Result<FilterList, Error> {
        return validateResult(result).flatMap(extractFilterList)
    }

    private static func validateResult(_ result: FilterListGetResult) -> Result<Data, Error> {
        Result {
            guard validStatusCodes ~= result.httpResponse.statusCode  else {
                throw ABPKitCoreError.filterListBadDownloadErrorCode(result.httpResponse.statusCode)
            }

            return result.data
        }
    }

    private static func extractFilterList(_ data: Data) -> Result<FilterList, Error> {
        Result {
            guard let listContent = String(data: data, encoding: .utf8) else {
                throw ABPKitCoreError.malformedFilterListContent
            }

            return FilterList(content: listContent)
        }
    }
}
