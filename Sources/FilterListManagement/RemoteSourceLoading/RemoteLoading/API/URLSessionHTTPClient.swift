// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// URL session http download client
final class URLSessionHTTPClient: HTTPClient {

    private let session: URLSession

    /// Creates a new instance
    /// - Parameter session: To be used to perform download tasks
    init(session: URLSession = .shared) {
        self.session = session
    }

    func get(from url: URL, completion: @escaping GetCompletion) {
        func handle(_ data: Data?, _ response: URLResponse?, _ error: Error?) {
            completion(Result {
                if let error = error {
                    throw error
                } else if let data = data, let response = response as? HTTPURLResponse {
                    return .init(data: data, httpResponse: response)
                }

                throw ABPKitCoreError.filterListBadDownloadResponseFormat
            })
        }
        session.dataTask(with: url, completionHandler: handle).resume()
    }
}
