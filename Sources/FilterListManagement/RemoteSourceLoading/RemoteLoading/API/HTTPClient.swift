// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Contains the filter list remote download result
struct FilterListGetResult: Equatable {
    let data: Data
    let httpResponse: HTTPURLResponse
}

/// HTTP client that downloads filter list from a given url
protocol HTTPClient {
    typealias GetResult = Result<FilterListGetResult, Error>
    typealias GetCompletion = (GetResult) -> Void

    /// Downloads filter list from a given url
    /// - Parameters:
    ///   - url: The url to load filter list from
    ///   - completion: A handler for download completion
    func get(from url: URL, completion: @escaping GetCompletion)
}
