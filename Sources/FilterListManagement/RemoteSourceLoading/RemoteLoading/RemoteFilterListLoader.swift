// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Responsible for handling remote loading logic
final class RemoteFilterListLoader: FilterListTypeLoader {

    private struct Constants {
        static let downloadCountMax = 4
    }

    private let remoteFilterListStore: RemoteFilterListStore
    private let clientMetaData: ClientMetaData
    private let filterListMetaInfoLoader: DownloadedFilterListMetaInfoLoader

    /// Creates an instance
    /// - Parameters:
    ///   - remoteRulesStore: The store to be used to load the filter list for a given source
    ///   - clientMetaData: To be used to add the necessary query parameters
    ///   - filterListMetaInfoLoader: To be used to provide filter list meta info which will be added to query parameters
    init(remoteFilterListStore: RemoteFilterListStore,
         clientMetaData: ClientMetaData,
         filterListMetaInfoLoader: DownloadedFilterListMetaInfoLoader) {
        self.remoteFilterListStore = remoteFilterListStore
        self.clientMetaData = clientMetaData
        self.filterListMetaInfoLoader = filterListMetaInfoLoader
    }

    func loadFilterList(forSource source: FilterListType,
                        completion: @escaping LoadCompletion) {
        func completeWithFailure(_ error: Error) {
            completion(.failure(error))
        }

        func loadFromURL() -> (URL) -> Void {
            return { [weak self] url in
                self?.remoteFilterListStore.download(from: url,
                                                     completion: completion)
            }
        }

        buildURL(for: source,
                 completion: handleResult(loadFromURL(), completeWithFailure))
    }

    // MARK: - Helpers

    private func buildURL(for source: FilterListType,
                          completion: @escaping (Result<URL, Error>) -> Void) {

        func makeQueryItemsFromFilterListMeta(_ metaInfoResult: DownloadedFilterListMetaInfoLoader.LoadResult) -> [URLQueryItem] {
            let lastVersion: String
            let downloadCount: String

            switch metaInfoResult {
            case .failure:
                lastVersion = "0"
                downloadCount = "0"
            case let .success(meta):
                lastVersion = meta.lastDownloadDate.asTimestamp()
                downloadCount = meta.downloadCount > Constants.downloadCountMax ?
                "\(Constants.downloadCountMax)+" : "\(meta.downloadCount)"
            }

            return [URLQueryItem(name: "lastVersion", value: lastVersion),
                    URLQueryItem(name: "downloadCount", value: downloadCount)]
        }

        func makeQueryItemsFromClientMeta(_ clientMeta: ClientMetaData) -> [URLQueryItem] {
            [
                URLQueryItem(name: "addonName", value: clientMeta.addon.name),
                URLQueryItem(name: "addonVersion", value: clientMeta.addon.version),
                URLQueryItem(name: "application", value: clientMeta.app.name),
                URLQueryItem(name: "applicationVersion", value: clientMeta.app.version),
                URLQueryItem(name: "platform", value: clientMeta.platform.name),
                URLQueryItem(name: "platformVersion", value: clientMeta.platform.version)
            ]
        }

        switch source {
        case let .custom(path):
            completion(Result {
                if let url = URL(string: path) {
                    return url
                }

                throw ABPKitCoreError.filterListBadDownloadURL
            })
        case let .abp(abpSource):
            guard var urlComponents = URLComponents(string: abpSource.downloadPath)  else {
                return completion(.failure(ABPKitCoreError.filterListBadDownloadURL))
            }

            let clientMetaData = self.clientMetaData
            filterListMetaInfoLoader.loadMetaInfo(forSource: source) { filterListMeta in
                urlComponents.queryItems = makeQueryItemsFromClientMeta(clientMetaData) + makeQueryItemsFromFilterListMeta(filterListMeta)

                completion(Result {
                    if let builtURL = urlComponents.url {
                        return builtURL
                    } else {
                        throw ABPKitCoreError.filterListBadDownloadURL
                    }
                })
            }
        }
    }
}

private extension Date {
    func asTimestamp() -> String {
        let fmt = DateFormatter()
        fmt.locale = Locale(identifier: "en_US_POSIX")
        fmt.dateFormat = "yyyyMMddHHmm"
        fmt.timeZone = TimeZone(secondsFromGMT: 0)
        return fmt.string(from: self)
    }
}

private extension FilterListAAVariant {
    var downloadPath: String {
        switch self {
        case let .withAA(source):
            switch source {
            case .easyList:
                return "https://easylist-downloads.adblockplus.org/easylist_min+exceptionrules_content_blocker.json"
            case .easyListChina:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistchina-minified+exceptionrules-minimal.json"
            case .easyListDutch:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistdutch-minified+exceptionrules-minimal.json"
            case .easyListFrance:
                return "https://easylist-downloads.adblockplus.org/easylist+liste_fr-minified+exceptionrules-minimal.json"
            case .easyListGermany:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistgermany-minified+exceptionrules-minimal.json"
            case .easyListItaly:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistitaly-minified+exceptionrules-minimal.json"
            case .easyListRussiaUkraine:
                return "https://easylist-downloads.adblockplus.org/easylist+ruadlist-minified+exceptionrules-minimal.json"
            case .easyListSpain:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistspanish-minified+exceptionrules-minimal.json"
            }
        case let .withoutAA(source):
            switch source {
            case .easyList:
                return "https://easylist-downloads.adblockplus.org/easylist_min_content_blocker.json"
            case .easyListChina:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistchina-minified.json"
            case .easyListDutch:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistdutch-minified.json"
            case .easyListFrance:
                return "https://easylist-downloads.adblockplus.org/easylist+liste_fr-minified.json"
            case .easyListGermany:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistgermany-minified.json"
            case .easyListItaly:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistitaly-minified.json"
            case .easyListRussiaUkraine:
                return "https://easylist-downloads.adblockplus.org/easylist+ruadlist-minified.json"
            case .easyListSpain:
                return "https://easylist-downloads.adblockplus.org/easylist+easylistspanish-minified.json"
            }
        }
    }
}
