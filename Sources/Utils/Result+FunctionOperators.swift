// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

/// Useful to unwrap a completion with result
///
/// Provides the ability to work in a more functional manner
///
/// - Parameters:
///   - onSuccess: Function to execute when result is success
///   - onError: Function to execute when result is failure
/// - Returns: A handler for a given result
func handleResult<T>(_ onSuccess: @escaping (T) -> Void,
                     _ onError: @escaping (Error) -> Void) -> (Result<T, Error>) -> Void {
    return {
        switch $0 {
        case let .success(data):
            onSuccess(data)
        case let .failure(error):
            onError(error)
        }
    }
}

/// Maps the given result to a new completion based on map function
/// - Parameters:
///   - closure: Closure to map to
///   - flatMap: Used to map the fiven result
/// - Returns: Mapping function
func mapToClosure<T, U>(_ closure: @escaping (Result<T, Error>) -> Void,
                        _ flatMap: @escaping (U) -> Result<T, Error>) -> (Result<U, Error>) -> Void {
    return { result in
        closure(result.flatMap(flatMap))
    }
}

func mapError<T>(_ closure: @escaping (Result<T, Error>) -> Void,
                 _ errorMap: @escaping (Error) -> Error) -> (Result<T, Error>) -> Void {
    return { result in
        closure(result.mapError(errorMap))
    }
}

extension Result {
    @discardableResult
    func onFailure(_ sideEffect: (Error) -> Void) -> Self {
        if case let .failure(error) = self {
            sideEffect(error)
        }

        return self
    }

    @discardableResult
    func onSuccess(_ sideEffect: (Success) -> Void) -> Self {
        if case let .success(data) = self {
            sideEffect(data)
        }

        return self
    }
}

extension Result where Success == Void {
    static var success: Result {
        return .success(())
    }
}
