// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

precedencegroup FunctionsCompositionPrecedence {
    associativity: left
}

infix operator >>>: FunctionsCompositionPrecedence

/// Composes two functions in one
func >>> <A, B, C>(lhs: @escaping (A) -> B, rhs: @escaping (B) -> C) -> (A) -> C {
    return { rhs(lhs($0)) }
}

// MARK: - Decomposition -

func curry<A, B, C>(_ f: @escaping (A, B) -> C) -> (A) -> (B) -> C {
    return { a in
        return { b in
            f(a, b)
        }
    }
}

func curry<A, B, C, D>(_ f: @escaping (A, B, C) -> D) -> (A, B) -> (C) -> D {
    return { a, b in
        return { c in
            f(a, b, c)
        }
    }
}
