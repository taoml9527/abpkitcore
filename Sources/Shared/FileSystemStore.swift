// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

final class FileSystemStore {
    private let queue = DispatchQueue(label: "\(FileSystemStore.self).queue",
                                      qos: .userInitiated,
                                      attributes: .concurrent)
    private let containerURL: URL

    init(containerURL: URL) {
        self.containerURL = containerURL
    }

    func write(data: Data, toFileNamed fileName: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let url = fileURL(fileName)
        queue.async(flags: .barrier) {
            completion(Result { try data.write(to: url, options: .atomicWrite) })
        }
    }

    func read(fromFileNamed fileName: String, completion: @escaping (Result<Data, Error>) -> Void) {
        let url = fileURL(fileName)
        queue.async {
            completion(Result { try Data(contentsOf: url) })
        }
    }

    private func fileURL(_ name: String) -> URL {
        containerURL.appendingPathComponent(name, isDirectory: false)
    }
}
