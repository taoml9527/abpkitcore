// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// Describes all of the errors that might occur while using ABPKitCore
public struct ABPKitCoreError: CustomNSError, Equatable {

    // MARK: - Error info

    /// The error domain, ABPKitCore specific
    public static var errorDomain: String {
        "ABPKitCoreErrorDomain"
    }

    /// The error code within the given domain. See ABPKitCoreErrorCode for all possible codes
    public let errorCode: Int

    /// The user-info dictionary containing any useful info
    public let errorUserInfo: [String: Any]

    // MARK: - Error Codes

    /// A set of all possible error codes
    public enum ABPKitCoreErrorCode: Int {
        /// Failed to decode the allowed domains while trying to load those
        /// from persistence storage. This indicates that the content of storage is malformed.
        case failedToDecodeAllowedDomains

        /// Failed to encode allowed domains while trying to add those to persistence storage.
        case failedToEncodeAllowedDomains

        /// ABPKitCore was unable to write the encoded domains to the persistence storage.
        case failedToStoreAllowedDomains

        /// ABPKitCore was unable to read allowed domains from persistence storage.
        case failedToLoadAllowedDomains

        /// The requested filter list file does not exist in the application bundle.
        /// It either was not added or the file name is incorrect.
        case filterListMissingInBundle

        /// Interim error, to be replaced by failedToDecodeFilterList when filter list validation is in place.
        case malformedFilterListContent

        /// ABPKitCore was unable to read the filter list from persistence storage.
        case failedToLoadCachedFilterList

        /// ABPKitCore was unable to write the filter list to persistence storage.
        case failedToCacheFilterList

        /// Loaded filter list is malformed, not following WebKit structure.
        case failedToDecodeFilterList

        /// ABPKitCore was unable to encode the filter list so that it can be written to persistence storage.
        case failedToEncodeFilterList

        /// Loaded filter list meta info is malformed
        case failedToDecodeFilterListMetaInfo

        /// ABPKitCore was unable to read the filter list meta info from persistence storage
        case failedToLoadFilterListMetaInfo

        /// ABPKitCore was unable to write the filter list meta info to persistence storage
        case failedToInsertFilterListMetaInfo

        /// Filter list download path couldn't be converted to a download URL
        case filterListBadDownloadURL

        /// Filter list download completed with invalid status code
        case filterListBadDownloadStatusCode

        /// Filter list download completed with unexpected response format
        case filterListBadDownloadResponseFormat

        /// ABPKitCore was unable to decode data to FilterListSource
        case failedToDecodeFilterListSource
    }

    // MARK: - Private

    private let abpKitCoreErrorCode: ABPKitCoreErrorCode
    private let description: String
    private let underlyingError: Error?

    // MARK: - Init

    init(errorCode: ABPKitCoreErrorCode,
         description: String,
         underlyingError: Error? = nil) {
        self.abpKitCoreErrorCode = errorCode
        self.description = description
        self.underlyingError = underlyingError

        self.errorCode = errorCode.rawValue
        self.errorUserInfo = {
            var errorDict: [String: Any] = [NSLocalizedDescriptionKey: description]
            if let underlyingError = underlyingError {
                errorDict[NSUnderlyingErrorKey] = underlyingError as NSError
            }
            return errorDict
        }()
    }

    // MARK: - Equatable

    public static func == (lhs: ABPKitCoreError, rhs: ABPKitCoreError) -> Bool {
        return (lhs as NSError) == (rhs as NSError)
    }

    // MARK: - Debug info

    var debugDescription: String {
        return "\(description) (\(String(describing: type(of: self))).\(String(describing: abpKitCoreErrorCode)))" +
            (underlyingError.map { "Underlying error: \(($0) as NSError).debugDescription)" } ?? "")
    }
}

// MARK: - Errors Factory

// MARK: - Allowed Domains

extension ABPKitCoreError {
    static func failedToDecodeAllowedDomains(_ underlyingError: Error) -> ABPKitCoreError {
        .init(errorCode: .failedToDecodeAllowedDomains,
              description: "Stored allowed domains are in wrong format",
              underlyingError: underlyingError)
    }

    static func failedToEncodeAllowedDomains(_ underlyingError: Error) -> ABPKitCoreError {
        .init(errorCode: .failedToEncodeAllowedDomains,
              description: "There was an error while trying to encode allowed domains",
              underlyingError: underlyingError)
    }

    static func failedToLoadAlowedDomains(_ underlyingError: Error) -> ABPKitCoreError {
        .init(errorCode: .failedToLoadAllowedDomains,
              description: "Failed to load allowed domains from persistence storage",
              underlyingError: underlyingError)
    }

    static func failedToStoreAllowedDomains(_ underlyingError: Error) -> ABPKitCoreError {
        .init(errorCode: .failedToStoreAllowedDomains,
              description: "There was an error when trying to store updated allowed domains",
              underlyingError: underlyingError)
    }
}

// MARK: - Filter list loading

extension ABPKitCoreError {
    static var missingFilterListInBundle: ABPKitCoreError {
        .init(errorCode: .filterListMissingInBundle,
              description: "Couldn't find the target filter list in application bundle")
    }

    static var malformedFilterListContent: ABPKitCoreError {
        .init(errorCode: .malformedFilterListContent,
              description: "Filter list content is malformed, not in WebKit format")
    }

    static var filterListBadDownloadURL: ABPKitCoreError {
        .init(errorCode: .filterListBadDownloadURL,
              description: "Couldn't build filter list download URL")
    }

    static var filterListBadDownloadResponseFormat: ABPKitCoreError {
        .init(errorCode: .filterListBadDownloadResponseFormat,
              description: "Filter list download completed with unexpected response format")
    }

    static func filterListBadDownloadErrorCode(_ code: Int) -> ABPKitCoreError {
        .init(errorCode: .filterListBadDownloadStatusCode,
              description: "Filter list download finishd with bad status code - \(code)")
    }

    static func failedToLoadCachedFilterList(_ underlyingError: Error) -> ABPKitCoreError {
        .init(errorCode: .failedToLoadCachedFilterList,
              description: "There was an error while trying to load the filter list from cache",
              underlyingError: underlyingError)
    }

    static func failedToCacheFilterList(_ underlyingError: Error) -> ABPKitCoreError {
        .init(errorCode: .failedToCacheFilterList,
              description: "There was an error while trying to cache the filter list",
              underlyingError: underlyingError)
    }
}

// MARK: - Meta info

extension ABPKitCoreError {
    static func failedToDecodeFilterListMetaInfo(_ underlyingError: Error) -> ABPKitCoreError {
        .init(errorCode: .failedToDecodeFilterListMetaInfo,
              description: "Cached filter list meta info is malformed",
              underlyingError: underlyingError)
    }

    static func failedToLoadFilterListMetaInfo(_ underlyingError: Error) -> ABPKitCoreError {
        .init(errorCode: .failedToLoadFilterListMetaInfo,
              description: "There was an error while trying to load cached filter list meta info",
              underlyingError: underlyingError)
    }

    static func failedToInsertFilterListMetaInfo(_ underlyingError: Error) -> ABPKitCoreError {
        .init(errorCode: .failedToInsertFilterListMetaInfo,
              description: "There was an error while trying to insert filter list meta info to cache",
              underlyingError: underlyingError)
    }
}

// MARK: - General

extension ABPKitCoreError {
    static var failedToDecodeFilterListSource: ABPKitCoreError {
        .init(errorCode: .failedToDecodeFilterListSource,
              description: "Failed to decode data to FilterListSource")
    }
}

extension Error {
    var debugDescription: String {
        (self as? ABPKitCoreError)?.debugDescription ?? localizedDescription
    }
}
