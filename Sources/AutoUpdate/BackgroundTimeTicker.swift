// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// A timer that runs on a background thread to not block the main run loop
final class BackgroundTimeTicker: TimeTicker {
    typealias TickObserver = () -> Void

    private enum State {
        case ticking
        case stopped
    }

    private var state = State.stopped
    private let timer: DispatchSourceTimer

    init() {
        self.timer = DispatchSource.makeTimerSource()
    }

    deinit {
        timer.setEventHandler {}
        timer.cancel()
        start(tickInterval: 0.0) { }
    }

    func start(tickInterval: TimeInterval, tickObserver: @escaping TickObserver) {
        if state == .ticking {
            return
        }

        state = .ticking
        timer.schedule(deadline: .now() + tickInterval, repeating: tickInterval)
        timer.setEventHandler(handler: tickObserver)
        timer.resume()
    }

    func stop() {
        if state == .stopped {
            return
        }
        state = .stopped
        timer.suspend()
    }
}
