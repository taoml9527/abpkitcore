// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

/// The interval at which downloaded filter lists are checked for expiration.
public let defaultUpdatesCheckInterval: TimeInterval = 20

/// This component is responsible for triggering periodic updates, though it does not perform
/// actual update, see `AutoUpdateComposer.makeAutoUpdater(...)` for more details.
public final class FilterListAutoUpdater<UpdateEvent> {

    /// Updating state of the auto updater, to be used to know at any given moment
    /// if the autoupdater is running.
    public private(set) var isRunning = false

    // Colaborators
    private let timeTicker: TimeTicker
    private let filterListUpdater: FilterListUpdater<UpdateEvent>

    // Internal State
    private var observers = [UpdateObserver<UpdateEvent>]()

    /// Creates a new instnace
    /// - Parameters:
    ///   - timeTicker: To be used to determine when it is needed to perform a filter list update
    ///   - filterListUpdater: To be used to perform the actual update
    init(timeTicker: TimeTicker, filterListUpdater: @escaping FilterListUpdater<UpdateEvent>) {
        self.timeTicker = timeTicker
        self.filterListUpdater = filterListUpdater
    }

    // MARK: - Public API

    /// Attach an observer to react to update events
    /// - Parameter observer: An observer to be used to observer the update events
    public func observeUpdates(_ observer: @escaping UpdateObserver<UpdateEvent>) {
        observers.append(observer)
    }

    /// Starts auto update cycles.
    /// If auto update is already started this function will return early
    /// without triggering new auto update cycles.
    public func startAutoUpdate(withCheckInterval interval: TimeInterval = defaultUpdatesCheckInterval) {
        guard !isRunning else { return }
        isRunning = true
        timeTicker.start(tickInterval: interval) { [weak self] in
            guard let self = self else { return }
            self.filterListUpdater(self.notifyObservers())
        }
    }

    /// Stops further auto update cycles. The updates occuring in current cycle will still occur.
    public func stopAutoUpdate() {
        isRunning = false
        timeTicker.stop()
    }

    // MARK: - Private methods

    private func notifyObservers() -> (UpdateEvent) -> Void {
        return { [weak self] event in
            self?.observers.forEach { $0(event) }
        }
    }
}
