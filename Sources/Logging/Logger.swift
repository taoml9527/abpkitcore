// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import os

let log = OSLog(subsystem: "com.ABPKitCore", category: "Framework")

func logInfo(msg: StaticString, args: CVarArg...) {
    os_log(msg, log: log, type: .default, args)
}

func logError(msg: StaticString, args: CVarArg...) {
    os_log(msg, log: log, type: .error, args)
}

class Logger {}

extension Logger: FilterListWithAllowedDomainsLoaderOutput {
    func didAppendAllowedDomainsToFilterList(for source: FilterListSource,
                                             allowedDomains: AllowedDomains,
                                             allowedDomainRule: FilterRule) {
        let message = String(format: "Did append allowed domains rule: %@, to filter list for source: %@, original domains: %@", allowedDomainRule.rule, source.logName, allowedDomains.joined(separator: ", "))
        logInfo(msg: "%{public}@", args: message)
    }
}

extension FilterListSource {
    public var logName: String {
        switch self {
        case let .bundled(source):
            return "Bundled source: \(source.logName)"
        case let .remote(source):
            return "Remote source: \(source.logName)"
        }
    }
}

extension FilterListType {
    public var logName: String {
        switch self {
        case let .abp(source):
            let name: String
            switch source {
            case let .withAA(aaSource):
                name = {
                    switch aaSource {
                    case .easyList:
                        return "easyList+exceptions"
                    case .easyListChina:
                        return "easyListChina+exceptions"
                    case .easyListDutch:
                        return "easyListDutch+exceptions"
                    case .easyListGermany:
                        return "easyListGermany+exceptions"
                    case .easyListItaly:
                        return "easyListItaly+exceptions"
                    case .easyListSpain:
                        return "easyListSpain+exceptions"
                    case .easyListFrance:
                        return "easyListFrannce+exceptions"
                    case .easyListRussiaUkraine:
                        return "easyListRussiaUkraine+exceptions"
                    }
                }()
            case let .withoutAA(nonAASource):
                name = {
                    switch nonAASource {
                    case .easyList:
                        return "easyList"
                    case .easyListChina:
                        return "easyListChina"
                    case .easyListDutch:
                        return "easyListDutch"
                    case .easyListGermany:
                        return "easyListGermany"
                    case .easyListItaly:
                        return "easyListItaly"
                    case .easyListSpain:
                        return "easyListSpain"
                    case .easyListFrance:
                        return "easyListFrannce"
                    case .easyListRussiaUkraine:
                        return "easyListRussiaUkraine"
                    }
                }()
            }
            return "ABP \(name) source"
        case let .custom(path):
            return "Custom source with path \(path)"
        }
    }
}
