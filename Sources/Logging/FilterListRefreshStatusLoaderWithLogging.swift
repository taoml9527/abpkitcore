// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

final class FilterListRefreshStatusLoaderWithLogging: FilterListRefreshStatusLoader {
    private let decoratee: FilterListRefreshStatusLoader

    init(decoratee: FilterListRefreshStatusLoader) {
        self.decoratee = decoratee
    }

    func loadRefreshStatus(forSource source: FilterListType, completion: @escaping LoadCompletion) {
        logInfo(msg: "Checking if refresh is needed for source: %{public}@", args: source.logName)
        decoratee.loadRefreshStatus(forSource: source) { status in
            switch status {
            case .needsRefresh:
                logInfo(msg: "Cached filter list is expired or missing, new download is required")
            case .noRefreshNeeded:
                logInfo(msg: "Cached filter list is fresh, no new download is required")
            }
            completion(status)
        }
    }
}
