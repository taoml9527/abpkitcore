// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

final class FilterListCacheStoreWithLogging: FilterListCacheStore {
    private let decoratee: FilterListCacheStore

    init(decoratee: FilterListCacheStore) {
        self.decoratee = decoratee
    }

    func retrieveFilterList(withName name: FilterListName, completion: @escaping RetrieveCompletion) {
        logInfo(msg: "Retrievieng from cache the filter list named: %{public}@", args: name)
        decoratee.retrieveFilterList(withName: name) { result in
            switch result {
            case .success:
                logInfo(msg: "Retrievied from cache the filter list named: %{public}@", args: name)
            case let .failure(error):
                logInfo(msg: "Failed to retrieve filter list from cache, reason: %{public}@", args: error.debugDescription)
            }
            completion(result)
        }
    }

    func insertFilterList(_ filterList: RawFilterList, withName name: FilterListName, completion: @escaping InsertCompletion) {
        logInfo(msg: "Inserting to cache the filter list named: %{public}@", args: name)
        decoratee.insertFilterList(filterList, withName: name) { result in
            switch result {
            case .success:
                logInfo(msg: "Inserted to cache the filter list named: %{public}@", args: name)
            case let .failure(error):
                logInfo(msg: "Failed to insert filter list to cache, reason: %{public}@", args: error.debugDescription)
            }
            completion(result)
        }
    }
}
