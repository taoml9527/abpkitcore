// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

final class DonwloadedFilterListMetaInfoStoreWithLogging: DonwloadedFilterListMetaInfoStore {
    private let decoratee: DonwloadedFilterListMetaInfoStore

    init(decoratee: DonwloadedFilterListMetaInfoStore) {
        self.decoratee = decoratee
    }

    func retrieveMetaInfoList(completion: @escaping RetrieveCompletion) {
        logInfo(msg: "Retrievieng meta info list from cache")
        decoratee.retrieveMetaInfoList { result in
            switch result {
            case let .success(metaInfo):
                logInfo(msg: "Loaded meta info list, content: %{public}@", args: metaInfo.logInfo)
            case let .failure(error):
                logError(msg: "Failed to load meta info list, reason: %{public}@", args: error.debugDescription)
            }
            completion(result)
        }
    }

    func insertMetaInfoList(_ metaInfoList: [DownloadedFilterListMetaInfo], completion: @escaping InsertCompletion) {
        logInfo(msg: "Inserting meta info list to cache, content: %{public}@", args: metaInfoList.logInfo)
        decoratee.insertMetaInfoList(metaInfoList) { result in
            switch result {
            case .success:
                logInfo(msg: "Successfuly inserted meta info")
            case let .failure(error):
                logError(msg: "Failed to insert meta info list, reason: %{public}@", args: error.debugDescription)
            }
            completion(result)
        }
    }
}

extension Array where Element == DownloadedFilterListMetaInfo {
    var logInfo: String {
        map { $0.logInfo }.joined(separator: ",")
    }
}
