// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

final class AllowedDomainsStoreWithLogging: AllowedDomainsStore {
    private let decoratee: AllowedDomainsStore

    init(decoratee: AllowedDomainsStore) {
        self.decoratee = decoratee
    }

    func retrieveDomains(completion: @escaping RetrieveCompletion) {
        logInfo(msg: "Started to load allowed domains from cache")

        decoratee.retrieveDomains { result in
            switch result {
            case let .success(domains):
                logInfo(msg: "Successfuly loaded domains from cache: %{public}@",
                        args: domains.joined(separator: ", "))
            case let .failure(error):
                logError(msg: "Failed to load domains from cache, reason: %{public}@", args: error.debugDescription)
            }
            completion(result)
        }
    }

    func insertDomains(_ allowedDomains: [String], completion: @escaping InsertCompletion) {
        logInfo(msg: "Inserting allowed domains to cache: %{public}@",
                args: allowedDomains.joined(separator: ", "))

        decoratee.insertDomains(allowedDomains) { result in
            switch result {
            case .success:
                logInfo(msg: "Successfuly inserted domains to cache")
            case let .failure(error):
                logError(msg: "Failed to insert domains to cache, reason: %{public}@", args: error.debugDescription)
            }
            completion(result)
        }
    }
}
