// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

final class DownloadedFilterListMetaInfoLoaderWithLogging: DownloadedFilterListMetaInfoLoader {
    private let decoratee: DownloadedFilterListMetaInfoLoader

    init(decoratee: DownloadedFilterListMetaInfoLoader) {
        self.decoratee = decoratee
    }

    func loadAllMetaInfo(completion: @escaping LoadAllCompletion) {
        decoratee.loadAllMetaInfo(completion: completion)
    }

    func loadMetaInfo(forSource source: FilterListType, completion: @escaping LoadCompletion) {
        logInfo(msg: "Started to load meta info for source: %{public}@", args: source.logName)
        decoratee.loadMetaInfo(forSource: source) { result in
            switch result {
            case let .success(metaInfo):
                logInfo(msg: "Loaded meta info: %@", args: metaInfo.logInfo)
            case let .failure(error):
                logError(msg: "Failed to load meta info reason: %{public}@", args: error.debugDescription)
            }
            completion(result)
        }
    }
}

extension DownloadedFilterListMetaInfo {
    var logInfo: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/mm/yy hh:mm:ss"
        formatter.locale = .current

        return "{source: \(source.logName), lastDownloadDate: \(formatter.string(from: lastDownloadDate)), downloadCount: \(downloadCount)}"
    }
}
