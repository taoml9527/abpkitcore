// This file is part of Adblock Plus <https://adblockplus.org/>,
// Copyright (C) 2006-present eyeo GmbH
//
// Adblock Plus is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// Adblock Plus is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import Foundation

final class RemoteFilterListStoreWithLogging: RemoteFilterListStore {
    private let decoratee: RemoteFilterListStore

    init(decoratee: RemoteFilterListStore) {
        self.decoratee = decoratee
    }

    func download(from url: URL, completion: @escaping DownloadCompletion) {
        logInfo(msg: "Started filter list download from: %{public}@", args: url.absoluteString)
        decoratee.download(from: url) { result in
            switch result {
            case .success:
                logInfo(msg: "Successfuly downloaded filter list from: %{public}@", args: url.absoluteString)
            case let .failure(error):
                logError(msg: "Failed to download filter list, reason: %{public}@", args: error.debugDescription)
            }

            completion(result)
        }
    }
}
